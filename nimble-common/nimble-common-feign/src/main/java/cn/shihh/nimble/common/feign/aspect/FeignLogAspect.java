package cn.shihh.nimble.common.feign.aspect;

import cn.shihh.nimble.common.util.StringEx;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * Feign 调用日志
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月16日18:20:57
 */
@Slf4j
@Aspect
public class FeignLogAspect {

    @Value("${spring.application.name:#{null}}")
    private String applicationName;

    /**
     * 拦截所有控制器的方法
     */
    @Pointcut("@within(org.springframework.cloud.openfeign.FeignClient)")
    public void pointcut() {

    }

    @Before("pointcut()")
    public void doBefore(JoinPoint joinPoint) {
        try {
            //方法参数
            StringBuilder params = new StringBuilder();
            Object[] args = joinPoint.getArgs();
            if (args != null && args.length > 0) {
                for (int i = 0; i < args.length; i++) {
                    String str = (args[i] == null) ? "" : args[i].toString();
                    if (i == 0) {
                        params.append(str);
                    } else {
                        params.append(", ").append(str);
                    }
                }
            }
            log.debug("Feign Request：{} 参数:{}", getFeignInfo(joinPoint), params);
        } catch (Exception ignored) {
        }
    }

    /**
     * 控制器执行完成后切面
     *
     * @param joinPoint 切入点
     * @param result
     */
    @AfterReturning(returning = "result", pointcut = "pointcut()")
    public void doAfterReturning(JoinPoint joinPoint, Object result) {
        log.debug("Feign Response:{} 参数:{}", getFeignInfo(joinPoint), result);
    }


    /**
     * 后置异常通知
     * 定义一个名字，该名字用于匹配通知实现方法的一个参数名，当目标方法抛出异常返回后，将把目标方法抛出的异常传给通知方法；
     * throwing:限定了只有目标方法抛出的异常与通知方法相应参数异常类型时才能执行后置异常通知，否则不执行，
     * 对于throwing对应的通知方法参数为Throwable类型将匹配任何异常。
     *
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(value = "pointcut()", throwing = "exception")
    public void doAfterThrowingAdvice(JoinPoint joinPoint, Throwable exception) {
        if (exception instanceof NullPointerException) {
            log.error("Feign异常：{}发生了空指针异常。", getFeignInfo(joinPoint), exception);
        } else {
            log.error("Feign异常：{}发生了系统异常：{}", getFeignInfo(joinPoint), exception.getMessage());
        }
    }

    /**
     * 获取Feign客户端信息
     * @param joinPoint
     * @return
     */
    private String getFeignInfo(JoinPoint joinPoint) {
        try {
            FeignClient feignClient;
            String className = "";
            String targetName = "";
            for (Class<?> pi : joinPoint.getTarget().getClass().getInterfaces()) {
                if ((feignClient = pi.getAnnotation(FeignClient.class)) != null) {
                    className = pi.getSimpleName();
                    targetName = feignClient.name();
                    break;
                }
            }
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            String methodName = methodSignature.getMethod().getName();
            return StringEx.placeholder("{0}#{1} {2}->{3}", className, methodName, applicationName, targetName);
        } catch (Exception e) {
            return "";
        }

    }

}
