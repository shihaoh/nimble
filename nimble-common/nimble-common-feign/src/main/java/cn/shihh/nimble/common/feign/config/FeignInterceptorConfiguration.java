package cn.shihh.nimble.common.feign.config;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.same.SaSameUtil;
import cn.dev33.satoken.stp.StpUtil;
import cn.shihh.nimble.common.constant.HeaderConst;
import feign.RequestInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.context.annotation.Bean;

/**
 * @author shihh
 * @since 2023/2/13
 */
@Slf4j
public class FeignInterceptorConfiguration {

    @Bean
    public RequestInterceptor requestInterceptor() {
        return (requestTemplate) -> {
            try {
                if (requestTemplate.headers().containsKey(StpUtil.getTokenName())) {
                    return;
                }
                String tokenName = StpUtil.getTokenName();
                String tokenValue = SaHolder.getRequest().getHeader(tokenName);
                requestTemplate.header(tokenName, tokenValue);
            } catch (Exception e) {
                log.info("内部调用无token");
            }
            requestTemplate.header(HeaderConst.LOG_TRACE_PARAM, MDC.get(HeaderConst.LOG_TRACE_PARAM));
            requestTemplate.header(SaSameUtil.SAME_TOKEN, SaSameUtil.getToken());
        };
    }

}
