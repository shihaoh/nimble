package cn.shihh.nimble.common.swagger.config;

import cn.shihh.nimble.common.swagger.annotation.EnableOpenApi;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;
import java.util.Objects;

/**
 * openapi 配置类
 *
 * @author lengleng
 * @date 2023/1/1
 */
public class OpenAPIDefinitionImportSelector implements ImportBeanDefinitionRegistrar {

	@Override
	public void registerBeanDefinitions(AnnotationMetadata metadata, BeanDefinitionRegistry registry) {

		Map<String, Object> annotationAttributes = metadata.getAnnotationAttributes(EnableOpenApi.class.getName(),
				true);
		Object value = annotationAttributes.get("value");
		if (Objects.isNull(value)) {
			return;
		}

		BeanDefinitionBuilder definition = BeanDefinitionBuilder.genericBeanDefinition(OpenAPIDefinition.class);

		registry.registerBeanDefinition("openAPIDefinition", definition.getBeanDefinition());

	}

}
