package cn.shihh.nimble.common.swagger.annotation;

import cn.shihh.nimble.common.factory.YamlPropertySourceFactory;
import cn.shihh.nimble.common.swagger.config.OpenAPIDefinitionImportSelector;
import cn.shihh.nimble.common.swagger.config.SwaggerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import java.lang.annotation.*;

/**
 * @author shihh
 * @since 2023/3/22
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@EnableConfigurationProperties(SwaggerProperties.class)
@Import({ OpenAPIDefinitionImportSelector.class })
@PropertySource(value = "classpath:openapi-config.yaml", factory = YamlPropertySourceFactory.class)
public @interface EnableOpenApi {

    /**
     * 网关路由前缀
     * @return String
     */
    String value() default "";

}
