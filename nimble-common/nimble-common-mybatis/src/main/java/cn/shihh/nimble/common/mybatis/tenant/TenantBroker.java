package cn.shihh.nimble.common.mybatis.tenant;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author shihh
 * @since 2023/3/21
 */
@Slf4j
@UtilityClass
public class TenantBroker {

    /**
     * 以某个租户的身份运行
     * @param tenant 租户ID
     * @param func
     */
    public void runAs(Long tenant, Consumer<Long> func) {
        final Long pre = TenantContextHolder.getTenantId();
        try {
            log.trace("TenantBroker 切换租户{} -> {}", pre, tenant);
            TenantContextHolder.setTenantId(tenant);
            func.accept(tenant);
        }
        catch (Exception e) {
            throw new TenantBrokerExceptionWrapper(e.getMessage(), e);
        }
        finally {
            log.trace("TenantBroker 还原租户{} <- {}", pre, tenant);
            TenantContextHolder.setTenantId(pre);
        }
    }

    /**
     * 以某个租户的身份运行
     * @param tenant 租户ID
     * @param func
     * @param <T> 返回数据类型
     * @return
     */
    public <R> R applyAs(Long tenant, Function<Long, R> func) {
        final Long pre = TenantContextHolder.getTenantId();
        try {
            log.trace("TenantBroker 切换租户{} -> {}", pre, tenant);
            TenantContextHolder.setTenantId(tenant);
            return func.apply(tenant);
        }
        catch (Exception e) {
            throw new TenantBrokerExceptionWrapper(e.getMessage(), e);
        }
        finally {
            log.trace("TenantBroker 还原租户{} <- {}", pre, tenant);
            TenantContextHolder.setTenantId(pre);
        }
    }

    /**
     * 以某个租户的身份运行
     * @param supplier
     * @param func
     */
    public void runAs(Supplier<Long> supplier, Consumer<Long> func) {
        runAs(supplier.get(), func);
    }

    /**
     * 以某个租户的身份运行
     * @param supplier
     * @param func
     * @param <T> 返回数据类型
     * @return
     */
    public <T> T applyAs(Supplier<Long> supplier, Function<Long, T> func) {
        return applyAs(supplier.get(), func);
    }

    public static class TenantBrokerExceptionWrapper extends RuntimeException {

        public TenantBrokerExceptionWrapper(String message, Throwable cause) {
            super(message, cause);
        }

        public TenantBrokerExceptionWrapper(Throwable cause) {
            super(cause);
        }

    }

}
