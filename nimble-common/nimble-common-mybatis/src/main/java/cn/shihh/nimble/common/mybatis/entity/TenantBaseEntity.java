package cn.shihh.nimble.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/6/2
 */
@Data
public class TenantBaseEntity extends BaseEntity {

    /**
     * 租户ID
     */
    @TableField("tenant_id")
    private Long tenantId;

}
