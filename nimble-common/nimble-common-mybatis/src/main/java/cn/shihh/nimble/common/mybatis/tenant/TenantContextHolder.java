package cn.shihh.nimble.common.mybatis.tenant;

import lombok.experimental.UtilityClass;

/**
 * @author shihh
 * @since 2023/3/20
 */
@UtilityClass
public class TenantContextHolder {

    private static final ThreadLocal<Long> TENANT_ID = new ThreadLocal<>();

    private static final ThreadLocal<Boolean> SKIP_FLAG = new ThreadLocal<>();

    public static void setTenantId(Long tenantId) {
        TENANT_ID.set(tenantId);
    }

    public static Long getTenantId() {
        return TENANT_ID.get();
    }

    public static void setSkipFlag() {
        SKIP_FLAG.set(Boolean.TRUE);
    }

    public static Boolean getSkipFlag() {
        return SKIP_FLAG.get() != null && SKIP_FLAG.get();
    }

    public static void clear() {
        TENANT_ID.remove();
        SKIP_FLAG.remove();
    }

}
