package cn.shihh.nimble.common.mybatis.enums;

import cn.hutool.core.util.ObjectUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author shihh
 * @date 2022/12/26
 * <p>
 * 数据权限类型
 */
@Getter
@AllArgsConstructor
public enum DataScopeTypeEnum {


	/**
	 * 全部数据权限
	 */
	ALL(0, "全部数据权限", "", ""),

	/**
	 * 自定数据权限
	 */
	CUSTOM(1, "自定义数据权限", " #{#deptName} IN ( #{@sdss.getRoleCustom( #user.roleId )} ) ", ""),

	/**
	 * 部门及以下数据权限
	 */
	DEPT_AND_CHILD(2, "部门及以下数据权限", " #{#deptName} IN ( #{@sdss.getDeptAndChild( #user.deptId )} )", ""),

	/**
	 * 部门数据权限
	 */
	DEPT(3, "部门数据权限", " #{#deptName} = #{#user.deptId} ", ""),

	/**
	 * 仅本人数据权限
	 */
	SELF(4, "仅本人数据权限"," #{#userName} = #{#user.userId} ", " 1 = 0 ");

	/**
	 * 类型
	 */
	private final Integer code;

	/**
	 * 描述
	 */
	private final String desc;

	/**
	 * 语法 采用 spel 模板表达式
	 */
	private final String sqlTemplate;

	/**
	 * 不满足 sqlTemplate 则填充
	 */
	private final String elseSql;


	public static DataScopeTypeEnum findCode(Integer code) {
		if (ObjectUtil.isNull(code)) {
			return null;
		}
		for (DataScopeTypeEnum type : values()) {
			if (type.getCode().equals(code)) {
				return type;
			}
		}
		return null;
	}
}
