package cn.shihh.nimble.common.mybatis.handler;

import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.body.LoginUser;
import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.satoken.utils.LoginHelper;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.ClassUtils;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * MP注入处理器
 *
 * @author Lion Li
 * @date 2021/4/25
 */
@Slf4j
public class CreateAndUpdateMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.debug("mybatis plus start insert fill ....");
        LocalDateTime now = LocalDateTime.now();

        // 审计字段自动填充
        fillValIfNullByName("createTime", now, metaObject, false);
        fillValIfNullByName("updateTime", now, metaObject, false);
        fillValIfNullByName("createBy", getUserName(), metaObject, false);
        fillValIfNullByName("updateBy", getUserName(), metaObject, false);
        fillValIfNullByName("createAt", getUserId(), metaObject, false);
        fillValIfNullByName("updateAt", getUserId(), metaObject, false);
        fillValIfNullByName("createUser", getUserId(), metaObject, false);
        fillValIfNullByName("createUserName", getUserName(), metaObject, false);
        fillValIfNullByName("gatherer", getUserId(), metaObject, false);
        fillValIfNullByName("gathererName", getUserName(), metaObject, false);

        // 删除标记自动填充
        fillValIfNullByName("delFlag", CommonConstants.STATUS_NORMAL, metaObject, false);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.debug("mybatis plus start update fill ....");
        fillValIfNullByName("updateTime", LocalDateTime.now(), metaObject, true);
        fillValIfNullByName("updateBy", getUserName(), metaObject, true);
        fillValIfNullByName("updateAt", getUserId(), metaObject, true);
        fillValIfNullByName("modifyUser", getUserId(), metaObject, true);
        fillValIfNullByName("modifyUserName", getUserName(), metaObject, true);
        fillValIfNullByName("modifyTime", LocalDateTime.now(), metaObject, true);
    }

    /**
     * 填充值，先判断是否有手动设置，优先手动设置的值，例如：job必须手动设置
     * @param fieldName 属性名
     * @param fieldVal 属性值
     * @param metaObject MetaObject
     * @param isCover 是否覆盖原有值,避免更新操作手动入参
     */
    private static void fillValIfNullByName(String fieldName, Object fieldVal, MetaObject metaObject, boolean isCover) {
        // 0. 如果填充值为空
        if (fieldVal == null) {
            return;
        }
        // 1. 没有 get 方法
        if (!metaObject.hasSetter(fieldName)) {
            return;
        }
        // 2. 如果用户有手动设置的值
        Object userSetValue = metaObject.getValue(fieldName);
        String setValueStr = StrUtil.str(userSetValue, Charset.defaultCharset());
        if (StrUtil.isNotBlank(setValueStr) && !isCover) {
            return;
        }
        // 3. field 类型相同时设置
        Class<?> getterType = metaObject.getGetterType(fieldName);
        if (ClassUtils.isAssignableValue(getterType, fieldVal)) {
            metaObject.setValue(fieldName, fieldVal);
        }
    }

    private String getUserName() {
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (Optional.ofNullable(loginUser).isPresent()) {
            return StrUtil.blankToDefault(loginUser.getName(), loginUser.getUsername());
        }
        return Strings.EMPTY;
    }

    private Long getUserId() {
        LoginUser loginUser = LoginHelper.getLoginUser();
        if (Optional.ofNullable(loginUser).isPresent()) {
            return loginUser.getUserId();
        }
        return -1L;
    }
}
