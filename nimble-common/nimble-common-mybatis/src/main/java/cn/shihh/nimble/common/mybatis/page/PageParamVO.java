package cn.shihh.nimble.common.mybatis.page;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PageParamVO<T> implements Serializable {

    private static final long serialVersionUID = 3553389101499138252L;

    /** 默认每页显示记录数 */
    private static final int DEFAULT_PAGE_SIZE = 10;
    /** 默认页数 */
    private static final int DEFAULT_PAGE_NUMBER = 1;

    @Schema(description = "当前页数", required = true)
    private Integer current;

    @Schema(description = "每页显示记录数", required = true)
    private Integer pageSize = DEFAULT_PAGE_SIZE;

    public Integer getCurrent() {
        if (ObjectUtil.isNull(current)) {
            return DEFAULT_PAGE_NUMBER;
        }
        return current;
    }

    public Integer getPageSize() {
        if (ObjectUtil.isNull(pageSize)) {
            return DEFAULT_PAGE_SIZE;
        }
        return pageSize;
    }


    /**
     * 分页传入默认值
     * @param pageNumber
     * @param pageSize
     */
    public void setMybatisPage(Integer pageNumber, Integer pageSize){
        if(ObjectUtils.isEmpty(pageNumber) || ObjectUtils.isEmpty(pageSize)){
            this.current = DEFAULT_PAGE_NUMBER;
            this.pageSize = DEFAULT_PAGE_SIZE;
        }
    }

    /**
     * 构造函数
     */
    public PageParamVO(int current, int pageSize) {
        this.current = current;
        this.pageSize = pageSize;
    }

    public IPage<T> toPage() {
        return new Page<>(getCurrent(), getPageSize());
    }

}
