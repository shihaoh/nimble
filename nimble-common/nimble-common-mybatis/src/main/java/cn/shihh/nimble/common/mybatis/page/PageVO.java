package cn.shihh.nimble.common.mybatis.page;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 分页器VO
 */
@Data
@AllArgsConstructor
public class PageVO<T> implements Serializable {

    private static final long serialVersionUID = -3045402529379956805L;

    @Schema(description = "总记录数")
    private Long total;

    @Schema(description = "当前页数")
    private Long current;

    @Schema(description = "每页显示记录数")
    private Long pageSize;

    @Schema(description = "总页数")
    private Long pages;

    @Schema(description = "当前页数据")
    private List<T> content;

    @Schema(description = "是否有下一页")
    private Boolean hasNext;

    @Schema(description = "自定义页面统计参数")
    private Map<String, Object> customizeTotal;

    public PageVO() {
    }

    public static <T> PageVO<T> of(Page<T> page) {
        return of(page, null);
    }

    public static <T> PageVO<T> empty(Class<T> cla) {
        return new PageVO(0L, 0L, 0L, 0L, null, false, null);
    }

    public <E> PageVO<E> to(Class<E> c) {
        PageVO<E> result = new PageVO<>();
        result.setTotal(this.total);
        result.setCurrent(this.current);
        result.setPageSize(this.pageSize);
        result.setPages(this.pages);
        result.setHasNext(this.hasNext);
        result.setContent(BeanUtil.copyToList(this.content, c));
        return result;
    }

    @SuppressWarnings("all")
    public static <T, E> PageVO<E> of(Page<T> page, Function<T, E> converter) {
        long totalPages = page.getPages();
        long current =  page.getCurrent()==0L?1:page.getCurrent();
        long pageSize = page.getSize();
        final List<E> content = converter != null ? page.getRecords().stream().map(converter).collect(Collectors.toList()) :
                (List<E>)page.getRecords();
        return new PageVO<E>(page.getTotal(),
                current,
                pageSize,
                totalPages,
                content,
                page.hasNext(),
                null);
    }
}
