package cn.shihh.nimble.common.mybatis.page;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.io.Serializable;
import java.util.List;

/**
 * @author longhao
 * @since 2021/10/29
 */
public class PageUtil implements Serializable {

    /**
     * 以paramvo生成mybatisplus的page对象
     * @param param
     * @param <T>
     * @return
     */
    public static <T> Page<T> paramToPage(PageParamVO<T> param) {
        if (ObjectUtil.isNull(param)) {
            return new Page<>(1, 10);
        }
        return new Page<>(param.getCurrent(), param.getPageSize());
    }

    /**
     * 以前端参数生成page对象
     * @param current
     * @param pageSize
     * @param cla
     * @param <T>
     * @return
     */
    public static <T> Page<T> paramToPage(int current, int pageSize, Class<T> cla) {
        return new Page<T>(current, pageSize);
    }

    /**
     * 将mybatisplus的page转换为供传递的pagevo
     * @param page
     * @param c
     * @param <T>
     * @return
     */
    public static <T, E> PageVO<E> pageToVO(IPage<T> page, Class<E> c) {
        PageVO<E> pageVO = new PageVO<>();
        pageVO.setTotal(page.getTotal());
        pageVO.setCurrent(page.getCurrent());
        pageVO.setPageSize(page.getSize());
        pageVO.setPages(page.getPages());
        pageVO.setContent(BeanUtil.copyToList(page.getRecords(), c));
        pageVO.setHasNext(page.getCurrent() < page.getPages());
        return pageVO;
    }

    /**
     * 将mybatisplus的page转换为供传递的pagevo
     * @param page
     * @param list
     * @param <T>
     * @return
     */
    public static <T, E> PageVO<E> pageToVO(IPage<T> page, List<E> list) {
        PageVO<E> pageVO = new PageVO<>();
        pageVO.setTotal(page.getTotal());
        pageVO.setCurrent(page.getCurrent());
        pageVO.setPageSize(page.getSize());
        pageVO.setPages(page.getPages());
        pageVO.setContent(list);
        pageVO.setHasNext(page.getCurrent() < page.getPages());
        return pageVO;
    }

    /**
     * 将mybatisplus的page转换为供传递的pagevo
     * @param page
     * @param c
     * @param <T>
     * @return
     */
    public static <T, E> PageVO<E> pageToVO(IPage<T> page, List<T> records, Class<E> c) {
        PageVO<E> pageVO = new PageVO<>();
        pageVO.setTotal(page.getTotal());
        pageVO.setCurrent(page.getCurrent());
        pageVO.setPageSize(page.getSize());
        pageVO.setPages(page.getPages());
        pageVO.setContent(BeanUtil.copyToList(records, c));
        pageVO.setHasNext(page.getCurrent() < page.getPages());
        return pageVO;
    }

}
