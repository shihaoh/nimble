package cn.shihh.nimble.common.mybatis.tenant;

import cn.shihh.nimble.common.constant.CommonConstants;
import feign.RequestInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;

/**
 * @author shihh
 * @since 2023/3/20
 */
@Slf4j
@Configuration
public class TenantConfiguration {

    @Bean
    public RequestInterceptor tenantInterceptor() {
        return (requestTemplate) -> {
            Long tenantId = TenantContextHolder.getTenantId();
            if (tenantId == null) {
                return;
            }
            log.debug("TTL 中的 租户ID为 {}", tenantId);
            requestTemplate.header(CommonConstants.TENANT_ID, tenantId.toString());
        };
    }

    @Bean
    public ClientHttpRequestInterceptor tenantRequestInterceptor() {
        return new TenantRequestInterceptor();
    }
}
