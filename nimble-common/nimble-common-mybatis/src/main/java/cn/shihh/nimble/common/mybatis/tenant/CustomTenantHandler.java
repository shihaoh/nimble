package cn.shihh.nimble.common.mybatis.tenant;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.schema.Column;

import java.util.List;

/**
 * @author shihh
 * @since 2023/3/20
 */
@Slf4j
@RequiredArgsConstructor
public class CustomTenantHandler implements TenantLineHandler {

    private final TenantConfigProperties tenantConfigProperties;

    @Override
    public Expression getTenantId() {
        Long tenantId = TenantContextHolder.getTenantId();
        log.debug("当前租户为 >> {}", tenantId);

        if (tenantId == null) {
            return new NullValue();
        }
        return new LongValue(tenantId);
    }

    @Override
    public String getTenantIdColumn() {
        return tenantConfigProperties.getColumn();
    }

    @Override
    public boolean ignoreTable(String tableName) {
        if (TenantContextHolder.getSkipFlag()) {
            return Boolean.TRUE;
        }

        return !CollUtil.contains(tenantConfigProperties.getTables(), tableName);
    }

    @Override
    public boolean ignoreInsert(List<Column> columns, String tenantIdColumn) {
        return TenantLineHandler.super.ignoreInsert(columns, tenantIdColumn);
    }
}
