package cn.shihh.nimble.common.mybatis.tenant;

import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.satoken.utils.LoginHelper;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

/**
 * @author lengleng
 * @date 2018/9/13
 */
@Slf4j
@Component
@Order
public class TenantContextHolderFilter extends GenericFilterBean {

	private final static String UNDEFINED_STR = "undefined";

	@Override
	@SneakyThrows
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		// 内部调用，通过header传递租户ID
		String headerTenantId = request.getHeader(CommonConstants.TENANT_ID);

		if (StrUtil.isNotBlank(headerTenantId) && !StrUtil.equals(UNDEFINED_STR, headerTenantId)) {
		log.debug("获取header中的租户ID为:{}", headerTenantId);
			TenantContextHolder.setTenantId(Long.parseLong(headerTenantId));
		}
		else {
			TenantContextHolder.setTenantId(LoginHelper.getTenantId());
		}

		filterChain.doFilter(request, response);
		TenantContextHolder.clear();
	}

}
