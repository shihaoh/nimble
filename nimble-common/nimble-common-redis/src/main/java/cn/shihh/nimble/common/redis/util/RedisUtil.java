package cn.shihh.nimble.common.redis.util;

import cn.hutool.extra.spring.SpringUtil;
import cn.shihh.nimble.common.util.StringEx;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.text.DecimalFormat;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * redis工具类
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月20日16:32:30
 */
public class RedisUtil {

    /**
     * 默认redis存储时间600秒
     */
    private static final long DEFAULT_TTL = 600;

    /**
     * 获取redisTemplate对象
     *
     * @return
     */
    public static RedisTemplate<String, Object> getRedisTemplate() {
        return SpringUtil.getBean("redisTemplate");
    }

    /**
     * 获取stringRedisTemplate对象
     *
     * @return
     */
    public static StringRedisTemplate getStringRedisTemplate() {
        return SpringUtil.getBean("stringRedisTemplate");
    }


    /**
     * 保存对象数据至redis
     *
     * @param key      键值
     * @param obj      数据
     * @param ttl      时间
     * @param timeUnit 时间单位
     */
    public static void putObject(String key, Object obj, long ttl, TimeUnit timeUnit) {
        getRedisTemplate().opsForValue().set(key, obj, ttl, timeUnit);
    }

    /**
     * 保存对象数据至redis
     *
     * @param key      键值
     * @param obj      数据
     * @param duration 时间单位
     */
    public static void putObject(String key, Object obj, Duration duration) {
        getRedisTemplate().opsForValue().set(key, obj, duration);
    }

    /**
     * 保存对象数据至redis
     *
     * @param key 键值
     * @param obj 数据
     * @param ttl 时间
     */
    public static void putObject(String key, Object obj, long ttl) {
        putObject(key, obj, ttl, TimeUnit.SECONDS);
    }

    /**
     * 保存对象数据至redis
     *
     * @param key 键值
     * @param obj 数据
     */
    public static void putObject(String key, Object obj) {
        putObject(key, obj, DEFAULT_TTL);
    }

    /**
     * 保存字符串数据至redis
     *
     * @param key      键值
     * @param obj      数据
     * @param ttl      时间
     * @param timeUnit 时间单位
     */
    public static void putString(String key, String obj, long ttl, TimeUnit timeUnit) {
        getStringRedisTemplate().opsForValue().set(key, obj, ttl, timeUnit);
    }

    /**
     * 保存字符串数据至redis
     *
     * @param key      键值
     * @param obj      数据
     * @param duration 时间
     */
    public static void putString(String key, String obj, Duration duration) {
        getStringRedisTemplate().opsForValue().set(key, obj, duration);
    }

    /**
     * 保存字符串数据至redis
     *
     * @param key 键值
     * @param obj 数据
     * @param ttl 时间
     */
    public static void putString(String key, String obj, long ttl) {
        putString(key, obj, ttl, TimeUnit.SECONDS);
    }

    /**
     * 保存字符串数据至redis
     *
     * @param key 键值
     * @param obj 数据
     */
    public static void putString(String key, String obj) {
        putString(key, obj, DEFAULT_TTL);
    }

    /**
     * 获取缓存对象
     *
     * @param key 键值
     * @return
     */
    public static String getString(String key) {
        return getStringRedisTemplate().opsForValue().get(key);
    }

    /**
     * 保存字符串数据至redis
     *
     * @param key      键值
     * @param obj      数据
     * @param ttl      时间
     * @param timeUnit 时间单位
     */
    public static void putLong(String key, Long obj, long ttl, TimeUnit timeUnit) {
        putString(key, String.valueOf(obj), ttl, timeUnit);
    }

    /**
     * 保存字符串数据至redis
     *
     * @param key 键值
     * @param obj 数据
     * @param ttl 时间
     */
    public static void putLong(String key, Long obj, long ttl) {
        putLong(key, obj, ttl, TimeUnit.SECONDS);
    }

    /**
     * 保存字符串数据至redis
     *
     * @param key 键值
     * @param obj 数据
     */
    public static void putLong(String key, Long obj) {
        putLong(key, obj, DEFAULT_TTL);
    }

    /**
     * redis流水号自增
     *
     * @param key      流水号键
     * @param timeout  超时
     * @return 流水号
     */
    public Long incr(String key, int timeout) {
        Long increment = getRedisTemplate().opsForValue().increment(key, 1);

        if (increment == 1L && timeout > 0) {
            Duration d = Duration.ofSeconds(timeout);
            expire(key, d);
        }
        return increment;
    }

    /**
     * redis流水号自增
     *
     * @param key       流水号键
     * @param formatStr 流水号格式   默认000000
     * @param timeout   超时
     * @return 流水号
     */
    public String incr(String key, String formatStr, int timeout) {
        Long increment = incr(key, timeout);
        //位数不够，前面补0
        DecimalFormat df = new DecimalFormat(formatStr);
        return df.format(increment);
    }

    public static Set<String> keys(String pattern) {
        return getRedisTemplate().keys(pattern);
    }

    /**
     * 获取缓存对象
     *
     * @param key 键值
     * @return
     */
    public static Long getLong(String key) {
        String str = getString(key);
        if (StringEx.isNotEmpty(str)) {
            return Long.parseLong(str);
        }
        return null;
    }

    /**
     * 获取缓存对象
     *
     * @param key 键值
     * @return
     */
    public static Integer getInt(String key) {
        String str = getString(key);
        if (StringEx.isNotEmpty(str)) {
            return Integer.parseInt(str);
        }
        return null;
    }


    /**
     * 获取缓存对象
     *
     * @param key 键值
     * @return
     */
    public static Object getObject(String key) {
        return getRedisTemplate().opsForValue().get(key);
    }

    /**
     * 获取指定对象
     *
     * @param key    键值
     * @param tClass 对象类型
     * @param <T>    对象
     * @return 对象数据
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public static <T> T getObject(String key, Class<T> tClass) {
        Object obj = getObject(key);
        if (Objects.nonNull(obj)) {
            T t = null;
            try {
                t = tClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
            BeanUtils.copyProperties(obj, t);
            return t;
        }
        return null;
    }

    /**
     * 获取指定列表对象
     *
     * @param key    键值
     * @param tClass 对象类型
     * @param <T>    对象
     * @return 对象数据
     */
    public static <T> List<T> getList(String key, Class<T> tClass) {
        Object obj = getObject(key);
        if (Objects.nonNull(obj)) {
            List<T> result = new ArrayList<T>();
            if (obj instanceof List<?>) {
                for (Object o : (List<?>) obj) {
                    result.add(tClass.cast(o));
                }
                return result;
            }
        }
        return null;
    }

    /**
     * Redis 是否存在键值
     *
     * @param key
     * @return
     */
    public static boolean hasKey(String key) {
        return Boolean.TRUE.equals(getRedisTemplate().hasKey(key));
    }

    /**
     * 获取redis键值的剩余时间
     *
     * @param key      键值
     * @param timeUnit 时间单位
     * @return
     */
    public static long getExpire(String key, TimeUnit timeUnit) {
        return getRedisTemplate().getExpire(key, timeUnit);
    }

    /**
     * 获取redis键值的剩余时间(秒)
     *
     * @param key 键值
     * @return
     */
    public static long getExpire(String key) {
        return getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 删除redis对象
     *
     * @param key 键值
     * @return
     */
    public static boolean delete(String key) {
        return Boolean.TRUE.equals(getRedisTemplate().delete(key));
    }

    /**
     * 删除redis对象
     *
     * @param key 键值
     * @return
     */
    public static boolean delete(String keyPlaceholder, String... replaceStrArray) {
        return Boolean.TRUE.equals(getRedisTemplate().delete(StringEx.placeholder(keyPlaceholder, replaceStrArray)));
    }

    /**
     * 批量删除redis对象
     *
     * @param keys 键值集合
     * @return
     */
    public static long delete(Collection<String> keys) {
        return getRedisTemplate().delete(keys);
    }

    public static void expire(String key, Duration duration) {
        getRedisTemplate().expire(key, duration);
    }

    public static void expire(String key, long timeout, TimeUnit unit) {
        getRedisTemplate().expire(key, timeout, unit);
    }
}
