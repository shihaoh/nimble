package cn.shihh.nimble.common.log.aop;

import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.constant.HeaderConst;
import cn.shihh.nimble.common.util.HttpEx;
import cn.shihh.nimble.common.util.StringEx;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.slf4j.MDC;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Objects;

/**
 * 控制器请求响应参数拦截器
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月14日11:02:34
 */
@Slf4j
@Aspect
@RequiredArgsConstructor
public class RequestLogAspect {

    private final ApplicationEventPublisher publisher;

    private static final ThreadLocal logThreadLocal = new ThreadLocal();

    /**
     * 记录请求的开始时间，放到request请求做为key
     */
    private static final String REQUEST_BEGIN_TIME = "REQUEST_BEGIN_TIME";

    /**
     * 拦截所有控制器的方法
     */
    @Pointcut(value = "@within(org.springframework.stereotype.Controller) || @within(org.springframework.web.bind.annotation.RestController)")
    public void pointcut() {
    }

    @Before(value = "pointcut()")
    public void doBefore(JoinPoint joinPoint) {
        // 内部调用不记录日志
        if (isInnerInvoke(joinPoint)) {
            return;
        }
        // 执行业务逻辑
        HttpEx.setAttribute(REQUEST_BEGIN_TIME, System.currentTimeMillis());
        String placeholder = "请求信息 URI:{0},方法:{1},控制器:{2},请求参数:{3}";
        String uri = HttpEx.requestURI();
        String method = HttpEx.method();
        String handler = handler(joinPoint);
        //方法参数
        StringBuilder params = new StringBuilder();
        Object[] args = joinPoint.getArgs();
        if (args != null && args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                String str = (args[i] == null) ? "" : args[i].toString();
                if (i == 0) {
                    params.append(str);
                } else {
                    params.append(", ").append(str);
                }
            }
        }

        log.debug(StringEx.placeholder(placeholder, uri, method, handler, params.toString()));

        String traceId = MDC.get(HeaderConst.LOG_TRACE_PARAM);
    }

    /**
     * 控制器执行完成后切面
     *
     * @param joinPoint 切入点
     * @param result
     */
    @AfterReturning(returning = "result", pointcut = "pointcut()")
    public void doAfterReturning(JoinPoint joinPoint, Object result) {
        if (isInnerInvoke(joinPoint)) {
            return;
        }
        String placeholder = "响应信息 控制器:{0},耗时:{1}毫秒,响应参数:{2}";
        Object requestBeginTime = HttpEx.getAttribute(REQUEST_BEGIN_TIME);
        long dealTime = System.currentTimeMillis() - Long.parseLong(requestBeginTime == null ? "0" : requestBeginTime.toString());
        log.debug(StringEx.placeholder(placeholder, handler(joinPoint), String.valueOf(dealTime), Objects.isNull(result) ? null: JSON.toJSONString(result)));
    }

    /**
     * 后置异常通知
     * 定义一个名字，该名字用于匹配通知实现方法的一个参数名，当目标方法抛出异常返回后，将把目标方法抛出的异常传给通知方法；
     * throwing:限定了只有目标方法抛出的异常与通知方法相应参数异常类型时才能执行后置异常通知，否则不执行，
     * 对于throwing对应的通知方法参数为Throwable类型将匹配任何异常。
     *
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(value = "pointcut()", throwing = "exception")
    public void doAfterThrowing(JoinPoint joinPoint, Exception exception) {
        if (isInnerInvoke(joinPoint)) {
            return;
        }
        String placeholder = "异常响应 控制器:{0},耗时:{1}毫秒,异常类型:{2},异常栈:{3}";
        Object requestBeginTime = HttpEx.getAttribute(REQUEST_BEGIN_TIME);
        long dealTime = System.currentTimeMillis() - Long.parseLong(requestBeginTime == null ? "0" : requestBeginTime.toString());
        log.warn(StringEx.placeholder(placeholder,
                handler(joinPoint),
                String.valueOf(dealTime),
                exception.getClass().getSimpleName(),
                StringEx.exceptionStack(exception)));
    }

    /**
     * 从切入点获取控制器名称
     *
     * @param joinPoint
     * @return
     */
    private String handler(JoinPoint joinPoint) {
        String placeholder = "{0}#{1}";
        String className = joinPoint.getTarget().getClass().getSimpleName();
        Signature signature = joinPoint.getSignature();
        String methodName = signature.getName();
        return StringEx.placeholder(placeholder, className, methodName);
    }

    /**
     * 判断是否是内部调用
     * @param joinPoint
     * @return
     */
    private boolean isInnerInvoke(JoinPoint joinPoint) {
        String className = joinPoint.getTarget().getClass().getSimpleName();
        // FeignClient的实现类为InnerController，在多服务合一的情况下之前的服务间调用不会发起http请求，而是直接调用方法
        // Feign请求会传递SA-SAME-TOKEN头作为标识（cn.shihh.nimble.common.feign.config.FeignInterceptorConfiguration.java:33），可作为是否请求判断依据
        return className.startsWith("Inner") && StrUtil.isBlank(HttpEx.getHeader("SA-SAME-TOKEN"));
    }

}
