package cn.shihh.nimble.common.body;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 用户信息
 *
 * @author ruoyi
 */
@Data
@NoArgsConstructor
public class LoginUser implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 租户ID
	 */
	private Long tenantId;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 微信用户openid
	 */
	private String openId;

	/**
	 * 菜单权限
	 */
	private Set<String> menuPermission = new HashSet<>();

	/**
	 * 角色权限
	 */
	private Set<String> rolePermission = new HashSet<>();

	/**
	 * 手机号
	 */
	private String phone;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 昵称
	 */
	private String nickname;

	/**
	 * 姓名
	 */
	private String name;

	/**
	 * 拓展字段:邮箱
	 */
	private String email;

	/**
	 * 部门ID
	 */
	private Long deptId;

	/**
	 * 用户名
	 */
	private String avatar;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 锁定标志
	 */
	private String lockFlag;

	/**
	 * 角色列表
	 */
	private List<RoleBody> roles = new ArrayList<>();

	/**
	 * 数据权限 当前角色ID
	 */
	private Long roleId;

	private LocalDateTime createTime;

	/**
	 * 获取登录id
	 */
	public String getLoginId() {
		String loginId;
		if (getUserId() != null) {
			loginId = String.valueOf(getUserId());
		} else {
			loginId = StrUtil.EMPTY;
		}
		if (getOpenId() != null) {
			if (StrUtil.isNotEmpty(loginId)) {
				loginId += ":";
			}
			loginId += getOpenId();
		}
		return loginId;
	}

}
