package cn.shihh.nimble.common.util;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Http扩展工具
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月14日09:37:03
 */
public class HttpEx {


    /**
     * 上下文的Request容器
     *
     * @return ServletRequestAttributes
     */
    public static ServletRequestAttributes requestAttributes() {
        return (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    }

    /**
     * 获取当前Request对象
     *
     * @return
     */
    public static HttpServletRequest request() {
        ServletRequestAttributes attributes = requestAttributes();
        return attributes.getRequest();
    }

    /**
     * 获取当前response对象
     *
     * @return
     */
    public static HttpServletResponse response() {
        ServletRequestAttributes attributes = requestAttributes();
        return attributes.getResponse();
    }

    /**
     * 获取请求头内容
     *
     * @param headerName
     * @return
     */
    public static String getHeader(String headerName) {
        return request().getHeader(headerName);
    }

    /**
     * 设置上下文容器数据
     *
     * @param name
     * @param o
     */
    public static void setAttribute(String name, Object o) {
        request().setAttribute(name, o);
    }

    /**
     * 获取上下文容器数据
     *
     * @param name
     * @return
     */
    public static Object getAttribute(String name) {
        return request().getAttribute(name);
    }

    /**
     * 获取上下文容器数据
     *
     * @param name
     * @return
     */
    public static Object getParameter(String name) {
        return request().getParameter(name);
    }


    /**
     * 获取请求地址URI
     *
     * @return
     */
    public static String requestURI() {
        return request().getRequestURI();
    }

    /**
     * 获取请求地址URL
     *
     * @return
     */
    public static String requestURL() {
        return request().getRequestURL().toString();
    }

    /**
     * 获取请求方法
     *
     * @return
     */
    public static String method() {
        return request().getMethod();
    }

}
