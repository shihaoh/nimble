package cn.shihh.nimble.common.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串扩展工具
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月10日14:58:54
 */
public class StringEx {


    private static final Pattern linePattern = Pattern.compile("_(\\w)");

    private static final Pattern humpPattern = Pattern.compile("[A-Z]");

    private static final String DYNAMIC_PARAM_PREFIX = "${";
    private static final String DYNAMIC_PARAM_SUFFIX = "}";

    /**
     * 判断传入字符串是否为空字符串
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || "".equals(str);
    }

    /**
     * 判断传入字符串是否为非空字符串
     *
     * @param str 需要判断的字符串
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 解析参数替换字符串，
     * @param content 待解析字符串
     * @param data    对象参数
     * @return 解析后字符串
     */
    public static String processDynamicParams(String content, Object data) {
        if (isEmpty(content)) {
            return StrUtil.EMPTY;
        }
        int begin = content.indexOf(DYNAMIC_PARAM_PREFIX);
        int end = content.indexOf(DYNAMIC_PARAM_SUFFIX);
        if (begin == -1 || end == -1 || begin > end) {
            return content;
        }
        String startContent = content.substring(0, begin);
        String substring = content.substring(begin, end + 1);
        if (StrUtil.isNotEmpty(substring)) {
            String key = substring.replace(DYNAMIC_PARAM_PREFIX, "")
                    .replace(DYNAMIC_PARAM_SUFFIX, "");
            JSONObject jsonObject = JsonEx.toJSONObject(data);
            Object value = jsonObject.get(key);
            if (value != null) {
                startContent = startContent + value;
            }
        }

        int length = content.length();
        if ( length == end + 1) {
            return startContent;
        } else {
            return startContent + processDynamicParams(content.substring(end + 1, length), data);
        }
    }
    /**
     * 占位符替换
     * eg： originStr -> 这是需要{0}的数据
     * replaceStrArray -> ["替换"]
     * return -> 这是需要替换的数据
     *
     * @param originStr       原始字符串
     * @param replaceStrArray 占位符替换数据数组
     * @return 替换后的字符串
     */
    public static String placeholder(String originStr, String... replaceStrArray) {
        final String placeholder = "\\{(\\d)}";
        if (isEmpty(originStr)) {
            return originStr;
        }
        Matcher m = Pattern.compile(placeholder).matcher(originStr);
        while (m.find()) {
            try {
                originStr = originStr.replace(m.group(), replaceStrArray[Integer.parseInt(m.group(1))]);
            } catch (Exception ignored) {
            }
        }
        return originStr;
    }

    /**
     * 占位符替换
     * eg： originStr -> 这是需要{0}的数据
     * replaceStrArray -> ["替换"]
     * return -> 这是需要替换的数据
     *
     * @param originStr       原始字符串
     * @param replaceStrArray 占位符替换数据数组
     * @return 替换后的字符串
     */
    public static String placeholder(String originStr, List<String> replaceStrArray) {
        if (CollUtil.isEmpty(replaceStrArray)) {
            return originStr;
        }
        final String placeholder = "\\{(\\d)}";
        if (isEmpty(originStr)) {
            return originStr;
        }
        Matcher m = Pattern.compile(placeholder).matcher(originStr);
        while (m.find()) {
            try {
                originStr = originStr.replace(m.group(), replaceStrArray.get(Integer.parseInt(m.group(1))));
            } catch (Exception ignored) {
            }
        }
        return originStr;
    }


    /**
     * 判断字符串是否为空，如果为空返回默认字符串
     *
     * @param conditionStr
     * @param defaultStr
     * @return
     */
    public static String ifNull(String conditionStr, String defaultStr) {
        return isNotEmpty(conditionStr) ? conditionStr : defaultStr;
    }

    /**
     * 异常站点信息
     *
     * @param exception
     * @return
     */
    public static String exceptionStack(Exception exception) {
        StackTraceElement stackTrace = exception.getStackTrace()[0];
        String className = stackTrace.getClassName();
        String[] classNameSplit = className.split("\\.");
        return StringEx.placeholder("{0}#{1}({2})", classNameSplit[classNameSplit.length - 1], stackTrace.getMethodName(), String.valueOf(stackTrace.getLineNumber()));
    }

    /**
     * 下划线转驼峰
     *
     * @param str
     * @return
     */
    public static String lineToHump(String str) {
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }


    /**
     * 驼峰转下划线
     *
     * @param str
     * @return
     */
    public static String humpToLine(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString().toUpperCase();
    }


    public static void main(String[] args) {
//        String originStr = "这是需要{0}的数据";
//        String[] replaceStrArray = new String[1];
//        replaceStrArray[0] = "替换";
//        System.out.println(placeholder(originStr, replaceStrArray));

        // System.out.println(DateStyle.getDateStyle("2020-12-20", DateType.DATE));
        // System.out.println(DateStyle.getDateStyle("2020-12",DateType.DATE));
        //
        // Pattern r = Pattern.compile("^(\\d{4})年((0([1-9]))月(1[0|1|2]))$");
        // Matcher m = r.matcher("2020年12月31");
        // System.out.println(m.find());

        System.out.println(humpToLine("aateStyle"));


    }
}
