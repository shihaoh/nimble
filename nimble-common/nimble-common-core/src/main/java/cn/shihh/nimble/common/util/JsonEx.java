package cn.shihh.nimble.common.util;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * JSON 工具扩展类
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月15日11:14:31
 */
@Slf4j
public class JsonEx {

    /**
     * 对象转换为JSON 字符串
     *
     * @param o
     * @return
     */
    public synchronized static String toJSONString(Object o) {
        if (Objects.isNull(o)) {
            return "{}";
        }
        ObjectMapper om = JacksonEx.getHttpObjectMapper();
        try {
            return om.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    public static JSONObject toJSONObject(Object o) {
        if (o instanceof JSONObject) {
            return (JSONObject) o;
        }
        return JSONObject.parseObject(toJSONString(o));
    }

    /**
     * JSON字符串转换为Java对象
     * @param jsonStr
     * @param clazz
     * @param <T>
     * @return
     * @throws JsonProcessingException
     */
    public synchronized static <T> T toObject(String jsonStr, Class<T> clazz) {
        if (StringEx.isEmpty(jsonStr)) {
            return null;
        }
        ObjectMapper om = JacksonEx.getHttpObjectMapper();
        try {
            return om.readValue(jsonStr, clazz);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    /**
     * JSON字符串转换为Java泛型对象
     * 例1：String jsonStr = "[{\"id\":\"1234\",\"account\":\"admin\"}]";
     * List<UserInfo> list = JsonUtil.json2GenericObject(jsonStr, new TypeReference<List<UserInfo>>() {});
     * 例2：String jsonStr = "[\"1111\",\"2222\",\"3333\"]";
     * List<String> list = JsonUtil.json2GenericObject(jsonStr, new TypeReference<List<String>>() {});
     * @param <T> 转换泛型
     * @param jsonString JSON字符串
     * @param tr 需要转换的对象类型
     * @return Java泛型对象
     */
    public synchronized static <T> T toObject(String jsonString, TypeReference <T> tr) {
        if (jsonString != null && !("".equals(jsonString))) {
            ObjectMapper om = JacksonEx.getHttpObjectMapper();
            try {
                return (T) (tr.getType().equals(String.class) ? jsonString : om.readValue(jsonString, tr));
            } catch (Exception e) {
                log.warn("json error:" + e.getMessage());
            }
        }
        return null;

    }
}
