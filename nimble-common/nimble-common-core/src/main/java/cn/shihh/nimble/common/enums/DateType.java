package cn.shihh.nimble.common.enums;

public enum DateType {
    DATE,
    DATE_TIME,
    TIME
}
