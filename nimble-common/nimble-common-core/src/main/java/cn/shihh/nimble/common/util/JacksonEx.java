package cn.shihh.nimble.common.util;

import cn.shihh.nimble.common.jackson.JavaTimeModule;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * JSON 工具扩展类
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月15日11:14:31
 */
@Slf4j
public class JacksonEx {

    private static ObjectMapper httpObjectMapper;

    /**
     * 处理序列化后的1.8的日期时间格式
     *
     * @return
     */
    public static synchronized ObjectMapper getHttpObjectMapper() {
        if (Objects.nonNull(httpObjectMapper)) {
            return httpObjectMapper;
        } else {
            synchronized (JacksonEx.class) {
                ObjectMapper objectMapper = new ObjectMapper();
                SimpleModule simpleModule = new SimpleModule();
                //转换长整型，全部以字符串返回，避免前端丢失精度
                simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
                simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
                objectMapper.registerModule(simpleModule);
                objectMapper.registerModule(new JavaTimeModule());
                objectMapper.registerModule(new ParameterNamesModule());
                objectMapper.registerModule(new Jdk8Module());
                //允许前端传入为定义的多余字段
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
                // 序列化LocalDate等日期格式时添加转换器
//                objectMapper.setSerializerFactory(
//                        objectMapper.getSerializerFactory()
//                                .withSerializerModifier(new DateBeanSerializerModifier()));
                httpObjectMapper = objectMapper;
                return objectMapper;
            }
        }
    }

    private static ObjectMapper redisObjectMapper;

    /**
     * 处理序列化后的1.8的日期时间格式
     *
     * @return
     */
    public static ObjectMapper getRedisObjectMapper() {
        if (Objects.nonNull(redisObjectMapper)) {
            return redisObjectMapper;
        } else {
            synchronized (JacksonEx.class) {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                objectMapper.registerModule(new JavaTimeModule());
                objectMapper.registerModule(new ParameterNamesModule());
                objectMapper.registerModule(new Jdk8Module());
                objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
                objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
                redisObjectMapper = objectMapper;
                return objectMapper;
            }
        }
    }

}
