package cn.shihh.nimble.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@AllArgsConstructor
public enum DateStyle {

    //日期格式
    YYYY_MM("yyyy-MM","^(\\d{4})-((0([1-9]))|(1[0|1|2]))$",DateType.DATE),
    YYYY_MM_DD("yyyy-MM-dd","^(\\d{4})-((0([1-9]))|(1[0|1|2]))-(([0-2]\\d)|(3[0|1]))$",DateType.DATE),
    YYYY_MM_DD_HH_MM("yyyy-MM-dd HH:mm","^(\\d{4})-((0([1-9]))|(1[0|1|2]))-(([0-2]\\d)|(3[0|1])) (([0-1]\\d)|(2[0-3])):([0-5]\\d)$",DateType.DATE_TIME),
    YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss","^(\\d{4})-((0([1-9]))|(1[0|1|2]))-(([0-2]\\d)|(3[0|1])) (([0-1]\\d)|(2[0-3])):([0-5]\\d):([0-5]\\d)$",DateType.DATE_TIME),

    HH_MM("HH:mm","^(([0-1]\\d)|(2[0-3])):([0-5]\\d)$",DateType.TIME),
    HH_MM_SS("HH:mm:ss","^(([0-1]\\d)|(2[0-3])):([0-5]\\d):([0-5]\\d)$",DateType.TIME),


    ;


    private String value;

    private String regex;

    private DateType type;

    /**
     * 获取日期字符串的日期风格。失敗返回null。
     *
     * @param date
     *            日期字符串
     * @return 日期风格
     */
    public static DateStyle getDateStyle(String date,DateType dateType) {
        for (DateStyle style : DateStyle.values()) {
            if (!Objects.equals(dateType,style.type)){
                continue;
            }
            Pattern r = Pattern.compile(style.regex);
            Matcher m = r.matcher(date);
            if (m.find()){
                return style;
            }
        }
        return null;
    }
}
