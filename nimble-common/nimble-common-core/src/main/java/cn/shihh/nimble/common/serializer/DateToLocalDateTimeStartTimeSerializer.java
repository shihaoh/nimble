package cn.shihh.nimble.common.serializer;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateToLocalDateTimeStartTimeSerializer extends StdDeserializer<LocalDateTime> {
    private static final long serialVersionUID = 1L;

    protected DateToLocalDateTimeStartTimeSerializer() {
        super(LocalDateTime.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateTimeStr = jsonParser.getValueAsString();
        if(StrUtil.isNotEmpty(dateTimeStr) && !dateTimeStr.contains(":")){
            dateTimeStr = String.format("%s 00:00:00", dateTimeStr);
        }
        return LocalDateTime.parse(dateTimeStr, formatter);
    }
}
