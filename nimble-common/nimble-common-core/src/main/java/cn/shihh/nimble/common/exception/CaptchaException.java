package cn.shihh.nimble.common.exception;

/**
 * 验证码异常
 *
 */
public class CaptchaException extends RuntimeException {

    private static final long serialVersionUID = -6550886498142636261L;

    public CaptchaException(String message) {
        super(message);
    }
}
