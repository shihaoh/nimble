package cn.shihh.nimble.common.body;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 角色
 *
 * @author Lion Li
 */

@Data
@NoArgsConstructor
public class RoleBody implements Serializable {

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色权限
     */
    private String roleCode;

    /**
     * 数据范围类型（0：所有数据权限；1：自定义数据权限；2：本部门数据权限；3：本部门及以下数据权限；4：仅本人数据权限）
     */
    private Integer dsType;

    /**
     * 数据范围
     */
    private String dsScope;

}
