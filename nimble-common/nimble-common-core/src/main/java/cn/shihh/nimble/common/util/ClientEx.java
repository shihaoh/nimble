package cn.shihh.nimble.common.util;


import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * 远程客户端扩展
 * @author 邓军
 * @date 2020年07月16日16:32:16
 * @version 1.0
 */
@Slf4j
public class ClientEx {

    /**
     * 封装远程调用, 只返回关心的内容
     *
     * @param supplier 远程调用真正逻辑, 返回内容为: {@link R<T>}
     * @param <T>      关心的内容类型
     * @return 关心的内容
     */
    public static <T> T execute(Supplier<R<T>> supplier) {
        R<T> r = supplier.get();
        if (Objects.equals(r.getCode(), CommonConstants.SUCCESS)) {
            return r.getData();
        } else {
            log.error("远程服务错误：{}", r.getMsg());
            throw new ServiceException(r.getMsg(), r.getCode());
        }
    }

}
