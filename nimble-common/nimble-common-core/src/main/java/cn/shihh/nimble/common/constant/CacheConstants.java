package cn.shihh.nimble.common.constant;

public interface CacheConstants {

	/**
	 * 全局缓存，在缓存名称上加上该前缀表示该缓存不区分租户，比如:
	 * <p/>
	 * {@code @Cacheable(value = CacheConstants.GLOBALLY+CacheConstants.MENU_DETAILS, key = "#roleId  + '_menu'", unless = "#result == null")}
	 */
	String GLOBALLY = "gl:";

	/**
	 * 验证码前缀
	 */
	String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY:";

	/**
	 * 菜单信息缓存
	 */
	String MENU_DETAILS = "menu_details";

	/**
	 * 应用菜单树缓存
	 */
	String MENU_APPLICATION_TREE = "menu_app_tree";

	/**
	 * 用户信息缓存
	 */
	String USER_DETAILS = "user_details";

	/**
	 * 移动端用户信息缓存
	 */
	String USER_DETAILS_MINI = "user_details_mini";

	/**
	 * 角色信息缓存
	 */
	String ROLE_DETAILS = "role_details";

	/**
	 * 字典信息缓存
	 */
	String DICT_DETAILS = "dict_details";
	/**
	 * 文件信息
	 */
	String FILE_ID = "resource:file:{0}";

	/**
	 * spring boot admin 事件key
	 */
	String EVENT_KEY = GLOBALLY + "event_key";

	/**
	 * 参数缓存
	 */
	String PARAMS_DETAILS = "params_details";

	/**
	 * 租户缓存 (不区分租户)
	 */
	String TENANT_DETAILS = GLOBALLY + "tenant_details";

	/**
	 * 客户端配置缓存
	 */
	String CLIENT_FLAG = "client_config_flag";

	/**
	 * 登录错误次数
	 */
	String LOGIN_ERROR_TIMES = "login_error_times";

	/**
	 * 验证码前缀
	 */
	String CAPTCHA_PREFIX = "nimble::captcha::";

	/**
	 * 基座应用
	 */
	String ENTRY_APPLICATION = "entry_application";

	/**
	 * 站内信缓存
	 */
	String INSIDE_NOTIFY = "inside_notify::%s::%s";

	/**
	 * 站内信数量缓存
	 */
	String INSIDE_NOTIFY_COUNT = "inside_notify_count::%s::%s";

	/**
	 * 内容类型缓存
	 */
	String CONTENT_TYPE = "content::content_type";

	/**
	 * 内容来源缓存
	 */
	String CONTENT_SOURCE = "content::content_source";

}
