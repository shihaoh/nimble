package cn.shihh.nimble.common.util;

import cn.hutool.core.date.DatePattern;
import cn.shihh.nimble.common.enums.DateType;
import cn.shihh.nimble.common.enums.DateStyle;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 日期时间扩展
 *
 * @author 邓军
 * @version 1.0
 * @date 2020年07月14日15:20:03
 */
public class DateEx {

    /**
     * {@link DateTimeFormatter}是线程安全的, 所以可以缓存起来
     */
    public final static ConcurrentMap<String, DateTimeFormatter> FORMATTER_MAP = new ConcurrentHashMap<>(8);

    public final static String DEFAULT_DATE_TIME_PATTERN = DatePattern.NORM_DATETIME_PATTERN;
    public final static String DEFAULT_DATE_PATTERN = DatePattern.NORM_DATE_PATTERN;
    public final static String DEFAULT_TIME_PATTERN = DatePattern.NORM_TIME_PATTERN;

    /**
     * timestamp(时间戳) 转换为 {@link LocalDateTime}
     *
     * @param timestamp 时间戳
     * @return
     */
    public static LocalDateTime toLocalDateTime(long timestamp) {
        return LocalDateTime.ofEpochSecond(timestamp / 1000, 0, ZoneOffset.ofHours(8));
    }

    /**
     * timestamp(时间戳) 转换为 {@link LocalDate}
     *
     * @param timestamp 时间戳
     * @return
     */
    public static LocalDate toLocalDate(long timestamp) {
        return toLocalDateTime(timestamp).toLocalDate();
    }


    /**
     * timestamp(时间戳) 转换为 {@link LocalTime}
     *
     * @param timestamp 时间戳
     * @return
     */
    public static LocalTime toLocalTime(long timestamp) {
        return toLocalDateTime(timestamp).toLocalTime();
    }

    /**
     * 根据 {@link LocalDate} 获取对应 零点 的秒数. 时区默认使用的是系统当前时区.
     *
     * @param localDate
     * @return
     */
    public static Long toSecond(LocalDate localDate) {
        return toSecond(localDate, ZoneId.systemDefault());
    }

    /**
     * 根据 {@link LocalTime} 获取对应 当天零点 的秒数. 时区默认使用的是系统当前时区.
     *
     * @param localTime
     * @return
     */
    public static Long toSecond(LocalTime localTime) {
        return toSecond(localTime, ZoneId.systemDefault());
    }

    /**
     * 根据 {@link LocalDateTime} 获取对应的秒数. 时区默认使用的是系统当前时区.
     *
     * @param localDateTime
     * @return
     */
    public static Long toSecond(LocalDateTime localDateTime) {
        return toSecond(localDateTime, ZoneId.systemDefault());
    }

    /**
     * 根据 {@link LocalDate} 获取对应 零点 的秒数. 时区默认使用的是系统当前时区.
     *
     * @param localDate
     * @param timeZone
     * @return
     */
    public static Long toSecond(LocalDate localDate, TimeZone timeZone) {
        if (timeZone == null) {
            return toSecond(localDate);
        }
        return toSecond(localDate, timeZone.toZoneId());
    }

    /**
     * 根据 {@link LocalTime} 获取对应 当天零点 的秒数. 时区默认使用的是系统当前时区.
     *
     * @param localTime
     * @param timeZone
     * @return
     */
    public static Long toSecond(LocalTime localTime, TimeZone timeZone) {
        if (timeZone == null) {
            return toSecond(localTime);
        }

        return toSecond(localTime, timeZone.toZoneId());
    }

    /**
     * 根据 {@link LocalDateTime} 获取指定时区对应的秒数.
     *
     * @param localDateTime
     * @param timeZone      指定时区
     * @return
     */
    public static Long toSecond(LocalDateTime localDateTime, TimeZone timeZone) {
        if (timeZone == null) {
            return toSecond(localDateTime);
        }

        return toSecond(localDateTime, timeZone.toZoneId());
    }

    /**
     * 根据 {@link LocalDate} 获取对应 零点 的秒数. 时区默认使用的是系统当前时区.
     *
     * @param localDate
     * @return
     */
    public static Long toMilli(LocalDate localDate) {
        return toMilli(localDate, ZoneId.systemDefault());
    }

    /**
     * 根据 {@link LocalTime} 获取对应 当天零点 的秒数. 时区默认使用的是系统当前时区.
     *
     * @param localTime
     * @return
     */
    public static Long toMilli(LocalTime localTime) {
        return toMilli(localTime, ZoneId.systemDefault());
    }


    /**
     * 根据 {@link LocalDateTime} 获取对应的毫秒数. 时区默认使用的是系统当前时区.
     *
     * @param localDateTime
     * @return
     */
    public static Long toMilli(LocalDateTime localDateTime) {
        return toMilli(localDateTime, ZoneId.systemDefault());
    }


    /**
     * 根据 {@link LocalTime} 获取对应 当天零点 的毫秒数. 时区默认使用的是系统当前时区.
     *
     * @param localTime
     * @param timeZone
     * @return
     */
    public static Long toMilli(LocalTime localTime, TimeZone timeZone) {
        if (timeZone == null) {
            return null;
        }

        return toMilli(localTime, timeZone.toZoneId());
    }


    /**
     * 根据 {@link LocalDate} 获取对应 零点 的毫秒数. 时区默认使用的是系统当前时区.
     *
     * @param localDate
     * @param timeZone
     * @return
     */
    public static Long toMilli(LocalDate localDate, TimeZone timeZone) {
        if (timeZone == null) {
            return null;
        }

        return toMilli(localDate, timeZone.toZoneId());
    }


    /**
     * 根据 {@link LocalDateTime} 获取指定时区对应的毫秒数.
     *
     * @param localDateTime
     * @param timeZone      指定时区
     * @return
     */
    public static Long toMilli(LocalDateTime localDateTime, TimeZone timeZone) {
        if (timeZone == null) {
            return null;
        }
        return toMilli(localDateTime, timeZone.toZoneId());
    }

    /**
     * 根据 {@link LocalDate} 和 {@link ZoneId} 时区, 获取指定时区 零点 对应的秒数.
     *
     * @param localDate
     * @param zoneId
     * @return
     */
    public static Long toSecond(LocalDate localDate, ZoneId zoneId) {
        if (localDate == null || zoneId == null) {
            return null;
        }
        return toSecond(LocalDateTime.of(localDate, LocalTime.MIN), zoneId);
    }

    /**
     * 根据 {@link LocalDate} 和 {@link ZoneId} 时区, 获取指定时区 零点 对应的毫秒数.
     *
     * @param localDate
     * @param zoneId
     * @return
     */
    public static Long toMilli(LocalDate localDate, ZoneId zoneId) {
        if (localDate == null || zoneId == null) {
            return null;
        }
        return toMilli(LocalDateTime.of(localDate, LocalTime.MIN), zoneId);
    }

    /**
     * 根据 {@link LocalTime} 和 {@link ZoneId} 时区, 获取指定时区 当天零点 对应的秒数.
     *
     * @param localTime
     * @param zoneId
     * @return
     */
    public static Long toSecond(LocalTime localTime, ZoneId zoneId) {
        if (localTime == null || zoneId == null) {
            return null;
        }
        return toSecond(LocalDateTime.of(LocalDate.now(), localTime), zoneId);
    }

    /**
     * 根据 {@link LocalTime} 和 {@link ZoneId} 时区, 获取指定时区 当天零点 对应的毫秒数.
     *
     * @param localTime
     * @param zoneId
     * @return
     */
    public static Long toMilli(LocalTime localTime, ZoneId zoneId) {
        if (localTime == null || zoneId == null) {
            return null;
        }
        return toMilli(LocalDateTime.of(LocalDate.now(), localTime), zoneId);
    }

    /**
     * 根据 {@link LocalDateTime} 和 {@link ZoneId} 时区, 获取指定时区对应的秒数.
     *
     * @param localDateTime
     * @param zoneId        时区
     * @return
     */
    public static Long toSecond(LocalDateTime localDateTime, ZoneId zoneId) {
        if (localDateTime == null || zoneId == null) {
            return null;
        }

        return localDateTime.atZone(zoneId).toInstant().getEpochSecond();
    }

    /**
     * 根据 {@link LocalDateTime} 和 {@link ZoneId} 时区, 获取指定时区对应的毫秒数.
     *
     * @param localDateTime
     * @param zoneId        时区
     * @return
     */
    public static Long toMilli(LocalDateTime localDateTime, ZoneId zoneId) {
        if (localDateTime == null || zoneId == null) {
            return null;
        }

        return localDateTime.atZone(zoneId).toInstant().toEpochMilli();
    }

    /**
     * 根据 {@link LocalDate} 和 {@link ZoneOffset} 时区偏移量, 获取指定时区 零点 对应的秒数.
     *
     * @param localDate
     * @param zoneOffset
     * @return
     */
    public static Long toSecond(LocalDate localDate, ZoneOffset zoneOffset) {
        if (localDate == null || zoneOffset == null) {
            return null;
        }
        return toSecond(LocalDateTime.of(localDate, LocalTime.MIN), zoneOffset);
    }

    /**
     * 根据 {@link LocalDate} 和 {@link ZoneOffset} 时区偏移量, 获取指定时区 零点 对应的毫秒数.
     *
     * @param localDate
     * @param zoneOffset
     * @return
     */
    public static Long toMilli(LocalDate localDate, ZoneOffset zoneOffset) {
        if (localDate == null || zoneOffset == null) {
            return null;
        }
        return toMilli(LocalDateTime.of(localDate, LocalTime.MIN), zoneOffset);
    }

    /**
     * 根据 {@link LocalTime} 和 {@link ZoneOffset} 时区偏移量, 获取指定时区 当天零点 对应的秒数.
     *
     * @param localTime
     * @param zoneOffset
     * @return
     */
    public static Long toSecond(LocalTime localTime, ZoneOffset zoneOffset) {
        if (localTime == null || zoneOffset == null) {
            return null;
        }
        return toSecond(LocalDateTime.of(LocalDate.now(), localTime), zoneOffset);
    }

    /**
     * 根据 {@link LocalTime} 和 {@link ZoneOffset} 时区偏移量, 获取指定时区 当天零点 对应的毫秒数.
     *
     * @param localTime
     * @param zoneOffset
     * @return
     */
    public static Long toMilli(LocalTime localTime, ZoneOffset zoneOffset) {
        if (localTime == null || zoneOffset == null) {
            return null;
        }
        return toMilli(LocalDateTime.of(LocalDate.now(), localTime), zoneOffset);
    }

    /**
     * 根据 {@link LocalDateTime} 和 {@link ZoneOffset} 时区偏移量, 获取指定时区对应的秒数.
     *
     * @param localDateTime
     * @param zoneOffset    时区偏移量
     * @return
     */
    public static Long toSecond(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        if (localDateTime == null || zoneOffset == null) {
            return null;
        }

        return localDateTime.toEpochSecond(zoneOffset);
    }

    /**
     * 根据 {@link LocalDateTime} 和 {@link ZoneOffset} 时区偏移量, 获取指定时区对应的毫秒数.
     *
     * @param localDateTime
     * @param zoneOffset    时区偏移量
     * @return
     */
    public static Long toMilli(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        if (localDateTime == null || zoneOffset == null) {
            return null;
        }

        return localDateTime.toInstant(zoneOffset).toEpochMilli();
    }

    /**
     * 解析时间，日期格式：yyyy-MM-dd HH:mm:ss
     *
     * @param datetime 时间
     * @return
     */
    public static LocalDateTime parseDateTime(String datetime) {
        DateStyle dateStyle = DateStyle.getDateStyle(datetime, DateType.DATE_TIME);
        if (Objects.nonNull(dateStyle)) {
            return parseDateTime(datetime, dateStyle.getValue());
        } else {
            return parseDateTime(datetime, DEFAULT_DATE_TIME_PATTERN);
        }
    }

    /**
     * 解析日期
     *
     * @param date    日期
     * @param pattern 格式
     * @return
     */
    public static LocalDateTime parseDateTime(String date, String pattern) {
        DateTimeFormatter formatter = getFormatter(pattern);
        return LocalDateTime.parse(date, formatter);
    }

    /**
     * 解析时间，日期格式：yyyy-MM-dd
     *
     * @param datetime 时间
     * @return
     */
    public static LocalDate parseDate(String datetime) {
        DateStyle dateStyle = DateStyle.getDateStyle(datetime,DateType.DATE);
        if (Objects.nonNull(dateStyle)) {
            return parseDate(datetime, dateStyle.getValue());
        } else {
            return parseDate(datetime, DEFAULT_DATE_PATTERN);
        }
    }

    /**
     * 解析日期
     *
     * @param date    日期
     * @param pattern 格式
     * @return
     */
    public static LocalDate parseDate(String date, String pattern) {
        DateTimeFormatter formatter = getFormatter(pattern);
        return LocalDate.parse(date, formatter);
    }

    /**
     * 解析时间，日期格式：HH:mm:ss
     *
     * @param datetime 时间
     * @return
     */
    public static LocalTime parseTime(String datetime) {
        DateStyle dateStyle = DateStyle.getDateStyle(datetime,DateType.TIME);
        if (Objects.nonNull(dateStyle)) {
            return parseTime(datetime, dateStyle.getValue());
        } else {
            return parseTime(datetime, DEFAULT_TIME_PATTERN);
        }
    }

    /**
     * 解析日期
     *
     * @param date    日期
     * @param pattern 格式
     * @return
     */
    public static LocalTime parseTime(String date, String pattern) {
        DateTimeFormatter formatter = getFormatter(pattern);
        return LocalTime.parse(date, formatter);
    }

    public static String format(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        return localDate.format(getFormatter(DEFAULT_DATE_PATTERN));
    }

    public static String format(LocalTime localTime) {
        if (localTime == null) {
            return null;
        }
        return localTime.format(getFormatter(DEFAULT_TIME_PATTERN));
    }

    public static String format(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }
        return localDateTime.format(getFormatter(DEFAULT_DATE_TIME_PATTERN));
    }

    /**
     * 从缓存中获取{@link DateTimeFormatter}
     *
     * @param pattern
     * @return
     */
    private static DateTimeFormatter getFormatter(String pattern) {
        DateTimeFormatter formatter = FORMATTER_MAP.get(pattern);
        if (formatter == null) {
            formatter = DateTimeFormatter.ofPattern(pattern);
            FORMATTER_MAP.putIfAbsent(pattern, formatter);
        }
        return formatter;
    }
}
