package cn.shihh.nimble.common.serializer;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateToLocalDateTimeEndTimeSerializer extends StdDeserializer<LocalDateTime> {
    private static final long serialVersionUID = 1L;

    protected DateToLocalDateTimeEndTimeSerializer() {
        super(LocalDateTime.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String dateTimeStr = jsonParser.getValueAsString();
        if(StrUtil.isNotEmpty(dateTimeStr) && !dateTimeStr.contains(":")){
            dateTimeStr = String.format("%s 23:59:59", dateTimeStr);
        }else if(StrUtil.isNotEmpty(dateTimeStr) && dateTimeStr.contains("00:00:00")){
            dateTimeStr = dateTimeStr.replace("00:00:00","23:59:59"); //骚操作，处理前端未安规定传参数的问题，做兼容
        }
        return LocalDateTime.parse(dateTimeStr, formatter);
    }
}
