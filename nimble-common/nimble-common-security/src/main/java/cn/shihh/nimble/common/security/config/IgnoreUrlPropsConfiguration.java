package cn.shihh.nimble.common.security.config;

import jakarta.annotation.PostConstruct;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 忽略URL属性配置
 *
 */
@Data
@ConfigurationProperties(prefix = "nimble.security.ignore")
@Component
public class IgnoreUrlPropsConfiguration {

    /**
     * 认证中心默认忽略验证地址
     */
    private static final String[] SECURITY_ENDPOINTS =
        {"/v3/api-docs", "/v3/api-docs/*", "/doc.html", "/webjars/**",
            "**/favicon.ico", "/swagger-resources/**", "/swagger-ui.html"};

    private List<String> urls = new ArrayList<>();

    private List<String> ignoreSecurity = new ArrayList<>();

    /**
     * 首次加载合并ENDPOINTS
     */
    @PostConstruct
    public void initIgnoreSecurity() {
        Collections.addAll(ignoreSecurity, SECURITY_ENDPOINTS);
    }

}
