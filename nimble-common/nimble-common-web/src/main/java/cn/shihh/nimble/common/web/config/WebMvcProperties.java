package cn.shihh.nimble.common.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

/**
 * Web配置
 * @author 邓军
 * @date 2020年07月14日09:55:47
 * @version 1.0
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ConfigurationProperties(prefix = "nimble.web")
@Configuration
@Data
public class WebMvcProperties {

    /**
     * 允许来源方法, 默认POST、GET
     */
    private String[] corsAllowedMethods = {"POST","GET","PUT","DELETE"};

    /**
     * 允许来源域名,默认全部
     */
    private String[] corsAllowedOrigins = {"*"};

    /**
     * 打印请求信息，默认打印
     */
    private boolean printRequest = true;

    /**
     * 打印响应信息，默认打印
     */
    private boolean printResponse = true;

}
