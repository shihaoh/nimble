package cn.shihh.nimble.common.web.config;

import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.constant.HeaderConst;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.UUID;

public class TraceInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String requestId = request.getHeader(HeaderConst.LOG_TRACE_PARAM);
        //请求Id放入MDC，可以在logback使用
        MDC.put(HeaderConst.LOG_TRACE_PARAM, StrUtil.blankToDefault(requestId, UUID.randomUUID().toString()));
        return true;
    }

}
