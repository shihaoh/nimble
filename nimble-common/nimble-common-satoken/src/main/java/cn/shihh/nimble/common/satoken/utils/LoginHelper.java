package cn.shihh.nimble.common.satoken.utils;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.exception.SaTokenException;
import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.body.LoginUser;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 登录鉴权助手
 *
 * user_type 为 用户类型 同一个用户表 可以有多种用户类型 例如 pc,app
 * deivce 为 设备类型 同一个用户类型 可以有 多种设备类型 例如 web,ios
 * 可以组成 用户类型与设备类型多对多的 权限灵活控制
 *
 * 多用户体系 针对 多种用户类型 但权限控制不一致
 * 可以组成 多用户类型表与多设备类型 分别控制权限
 *
 * @author Lion Li
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginHelper {

    public static final String JOIN_CODE = ":";
    public static final String LOGIN_USER_KEY = "loginUser";
    public static final String LOGIN_USER_CACHE_KEY = "user:login:";

    /**
     * 登录系统
     *
     * @param loginUser 登录用户信息
     */
    public static void login(LoginUser loginUser) {
        SaHolder.getStorage().set(LOGIN_USER_KEY, loginUser);
        StpUtil.login(loginUser.getLoginId());
        setLoginUser(loginUser);
    }

    /**
     * 设置用户数据(多级缓存)
     */
    public static void setLoginUser(LoginUser loginUser) {
        StpUtil.getTokenSession().set(LOGIN_USER_KEY, loginUser);
    }

    /**
     * 获取用户(多级缓存)
     */
    public static LoginUser getLoginUser() {
        LoginUser loginUser = (LoginUser) SaHolder.getStorage().get(LOGIN_USER_KEY);
        if (loginUser != null) {
            return loginUser;
        }
        SaSession tokenSession = StpUtil.getTokenSession();
        if (tokenSession == null) {
            return null;
        }
        loginUser = (LoginUser) tokenSession.get(LOGIN_USER_KEY);
        SaHolder.getStorage().set(LOGIN_USER_KEY, loginUser);
        return loginUser;
    }

    /**
     * 获取用户(多级缓存)
     */
    public static LoginUser getLoginUser(String tokenValue) {
        LoginUser loginUser = (LoginUser) StpUtil.getTokenSessionByToken(tokenValue).get(LOGIN_USER_KEY);
        if (loginUser == null) {
            log.info("token {} 无效，未获取到登录用户", tokenValue);
            throw new SaTokenException("token invalid");
        }
        return loginUser;
    }

    /**
     * 获取用户id
     */
    public static String getLoginId() {
        LoginUser loginUser = getLoginUser();
        if (ObjectUtil.isNull(loginUser)) {
            return StpUtil.getLoginIdAsString();
        }
        return loginUser.getLoginId();
    }

    /**
     * 获取用户id
     */
    public static Long getTenantId() {
        LoginUser loginUser = getLoginUser();
        if (ObjectUtil.isNull(loginUser)) {
            return null;
        }
        return loginUser.getTenantId();
    }

    /**
     * 获取游客OpenId
     */
    public static String getOpenId() {
        LoginUser loginUser = getLoginUser();
        if (ObjectUtil.isNull(loginUser)) {
            return null;
        }
        return loginUser.getOpenId();
    }

    /**
     * 获取用户id
     */
    public static Long getUserId() {
        LoginUser loginUser = getLoginUser();
        return loginUser.getUserId();
    }

    /**
     * 获取用户账户
     */
    public static String getUsername() {
        LoginUser loginUser = getLoginUser();
        if (ObjectUtil.isNotNull(loginUser)) {
            return StrUtil.blankToDefault(loginUser.getName(), loginUser.getUsername());
        }
        return "";
    }

    /**
     * 获取用户姓名
     */
    public static String getName() {
        return getLoginUser().getName();
    }

    /**
     * 获取用户账户
     */
    public static String getAvatar() {
        return getLoginUser().getAvatar();
    }

}
