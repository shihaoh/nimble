package cn.shihh.nimble.system.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author shihh
 * @since 2023/3/23
 */
@Configuration
@ComponentScan("cn.shihh.nimble.system")
public class SysDependScanConfig {
}
