package cn.shihh.nimble.system.annotation;

import cn.shihh.nimble.system.config.SysDependScanConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({SysDependScanConfig.class})
public @interface EnableSystemServer {
}
