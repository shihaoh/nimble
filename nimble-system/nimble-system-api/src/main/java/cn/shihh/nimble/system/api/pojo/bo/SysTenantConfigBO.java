package cn.shihh.nimble.system.api.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 机构配置表(SysTenantConfig)表实体类
 *
 * @author makejava
 * @since 2023-03-21 15:56:02
 */
@Data
@Schema(description = "机构配置表")
public class SysTenantConfigBO {
    
    @Schema(description = "主键")
    private Long id;
    
    @Schema(description = "配置分类")
    private String confType;
    
    @Schema(description = "配置键")
    private String confKey;
    
    @Schema(description = "配置值")
    private String confValue;
    
    @Schema(description = "配置默认值")
    private String confDefaultValue;
    
    @Schema(description = "配置描述")
    private String confDesc;

}

