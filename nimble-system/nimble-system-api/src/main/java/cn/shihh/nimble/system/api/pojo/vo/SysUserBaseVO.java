package cn.shihh.nimble.system.api.pojo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author shihh
 * @since 2023/3/30
 */
@Data
@Schema(description = "用户基础信息")
public class SysUserBaseVO {

    @Schema(description = "主键ID")
    private Long userId;

    @Schema(description = "状态 0在职 1离职 2锁定")
    private Integer status;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "用户编码")
    private String userCode;

    @Schema(description = "头像")
    private String avatar;

    @Schema(description = "形象照")
    private String portrait;

    @Schema(description = "微信二维码")
    private String wechatQrcode;

    @Schema(description = "拓展字段:昵称")
    private String nickname;

    @Schema(description = "拓展字段:姓名")
    private String name;

    @Schema(description = "手机号")
    private String phone;

    @Schema(description = "邮箱")
    private String email;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "创建人")
    private String createBy;

    @Schema(description = "部门id")
    private Long deptId;

    @Schema(description = "部门名称")
    private String deptName;
}
