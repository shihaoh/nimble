package cn.shihh.nimble.system.api.service;

import cn.shihh.nimble.system.api.client.RemoteDataScopeClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author shihh
 * @since 2023/5/22
 */
@Service("sdss")
@RequiredArgsConstructor
public class SysDataScopeService {

    private final RemoteDataScopeClient remoteDataScopeClient;

    public String getRoleCustom(Long roleId) {
        return remoteDataScopeClient.getRoleCustom(roleId);
    }

    public String getDeptAndChild(Long deptId) {
        return remoteDataScopeClient.getDeptAndChild(deptId);
    }
}
