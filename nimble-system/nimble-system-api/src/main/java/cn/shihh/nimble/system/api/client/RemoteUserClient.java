package cn.shihh.nimble.system.api.client;

import cn.shihh.nimble.common.body.LoginUser;
import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.constant.SystemApiConstant;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserFromCardVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

/**
 * @author shihh
 * @since 2023/3/22
 */
@FeignClient(name = "nimble-system", url = "${nimble.feign.url.system:nimble-system}", contextId = "remoteUserClient")
public interface RemoteUserClient {

    @GetMapping(SystemApiConstant.PROVIDER_USER)
    R<LoginUser> getUserByPhone(@RequestHeader(CommonConstants.TENANT_ID) Long tenantId, @PathVariable("phone") String phone);

    @PostMapping(SystemApiConstant.PROVIDER_USER_LIST_BY_ROLE_IDS)
    R<List<SysUserBaseVO>> listByRoleIds(@RequestParam("roleId") Collection<Long> roleIds);

    @GetMapping(SystemApiConstant.PROVIDER_USER_LIST_BY_KEYWORDS)
    R<List<SysUserBaseVO>> listByKeywords(@RequestParam("keywords") String keywords);

    @GetMapping(SystemApiConstant.PROVIDER_USER_LIST_BY_PERMISSIONS)
    R<List<SysUserBaseVO>> listByPermissions(@RequestParam(value = "deptId", required = false) Long deptId);

    @GetMapping(SystemApiConstant.PROVIDER_USER_FROM_CARD_BY_USER_ID)
    R<List<SysUserFromCardVO>> getUserFromCardByUserIds(@PathVariable("userId") List<Long> userIds);

    @GetMapping(SystemApiConstant.PROVIDER_USER_DEPT_INFO_BY_USER_ID)
    R<SysDeptVO> getUserDeptInfoByUserId(@PathVariable("userId") Long userId);
}
