package cn.shihh.nimble.system.api.pojo.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 部门管理(SysDept)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:12
 */
@Data
@Schema(description = "部门管理")
public class SysDeptVO {

    private Long deptId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long parentId;
    
    @Schema(description = "名称")
    private String name;
    
    @Schema(description = "排序")
    private Integer sortOrder;

    @Schema(description = "部门配置")
    private String deptConf;

}

