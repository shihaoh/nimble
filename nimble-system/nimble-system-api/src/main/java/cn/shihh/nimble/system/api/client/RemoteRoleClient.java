package cn.shihh.nimble.system.api.client;

import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.constant.SystemApiConstant;
import cn.shihh.nimble.system.api.pojo.vo.SysRoleListVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author shihh
 * @since 2023/6/12
 */
@FeignClient(name = "nimble-system", url = "${nimble.feign.url.system:nimble-system}", contextId = "remoteRoleClient")
public interface RemoteRoleClient {

    @GetMapping(SystemApiConstant.PROVIDER_ROLE_BY_ROLE_IDS)
    R<List<SysRoleListVO>> listRoleByIds(@RequestParam("roleId") List<Long> roleIds);

}
