package cn.shihh.nimble.system.api.pojo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/3/15
 */
@Data
@Schema(description = "岗位信息")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysPostVO {
    @Schema(description = "岗位ID")
    private Long postId;

    @Schema(description = "岗位类型 0律师1行政2财务3其他")
    private Integer postType;

    @Schema(description = "岗位编码")
    private String postCode;

    @Schema(description = "岗位名称")
    private String postName;

    @Schema(description = "岗位排序")
    private Integer postSort;

    @Schema(description = "岗位描述")
    private String remark;

}
