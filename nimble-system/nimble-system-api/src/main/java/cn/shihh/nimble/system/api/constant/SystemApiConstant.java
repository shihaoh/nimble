package cn.shihh.nimble.system.api.constant;

import cn.shihh.nimble.common.constant.CommonConstants;

/**
 * @author shihh
 * @since 2023/3/22
 */
public interface SystemApiConstant {

    /**
     * 根据手机号获取用户信息
     */
    String PROVIDER_USER = CommonConstants.PROVIDER + "/user/{phone}";

    /**
     * 根据角色id列表获取用户列表
     */
    String PROVIDER_USER_LIST_BY_ROLE_IDS = CommonConstants.PROVIDER + "/user/listByRoleIds";

    /**
     * 模糊查询用户列表
     */
    String PROVIDER_USER_LIST_BY_KEYWORDS = CommonConstants.PROVIDER + "/user/listByKeywords";

    /**
     * 根据权限获取用户列表
     */
    String PROVIDER_USER_LIST_BY_PERMISSIONS = CommonConstants.PROVIDER + "/user/listByPermissions";

    /**
     * 根据用户id获取名片用户信息
     */
    String PROVIDER_USER_FROM_CARD_BY_USER_ID = CommonConstants.PROVIDER + "/user/card/{userId}";

    /**
     * 根据用户id获取用户部门信息
     */
    String PROVIDER_USER_DEPT_INFO_BY_USER_ID = CommonConstants.PROVIDER + "/user/dept/{userId}";

    /**
     * 根据角色id列表获取角色列表
     */
    String PROVIDER_ROLE_BY_ROLE_IDS = CommonConstants.PROVIDER + "/role/listByRoleIds";

    /**
     * 机构配置
     */
    String PROVIDER_TENANT_CONFIG = CommonConstants.PROVIDER + "/tenant/config";
}
