package cn.shihh.nimble.system.api.client;

import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.constant.SystemApiConstant;
import cn.shihh.nimble.system.api.enums.TenantConfigEnum;
import cn.shihh.nimble.system.api.pojo.vo.ConfigVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author shihh
 * @since 2023/3/21
 */
@FeignClient(name = "nimble-system", url = "${nimble.feign.url.system:nimble-system}", contextId = "remoteConfigClient")
public interface RemoteConfigClient {

    @GetMapping(SystemApiConstant.PROVIDER_TENANT_CONFIG + "/{conf}")
    R<ConfigVO> tenantConfig(@PathVariable("conf") TenantConfigEnum conf);

    @GetMapping(SystemApiConstant.PROVIDER_TENANT_CONFIG)
    R<List<ConfigVO>> tenantConfig(@RequestParam("config") List<TenantConfigEnum> configs);

    @PutMapping(SystemApiConstant.PROVIDER_TENANT_CONFIG + "/{conf}")
    R<Boolean> updateTenantConfig(@PathVariable("conf") TenantConfigEnum conf, @RequestParam("value") String value);

}
