package cn.shihh.nimble.system.api.pojo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author linglong
 * @since 2022/1/13
 */
@Data
@Schema(description = "规则修改传参")
public class ConfigParam {

    @Schema(description = "配置建")
    private String key;

    @Schema(description = "配置值")
    private String configVal;

}
