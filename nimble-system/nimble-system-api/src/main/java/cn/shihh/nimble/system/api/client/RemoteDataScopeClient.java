package cn.shihh.nimble.system.api.client;

import cn.shihh.nimble.common.constant.CommonConstants;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author shihh
 * @since 2023/5/22
 */
@FeignClient(name = "nimble-system", url = "${nimble.feign.url.system:nimble-system}", contextId = "remoteDataScopeClient")
public interface RemoteDataScopeClient {

    /**
     * 获取角色自定义权限语句
     */
    @GetMapping(CommonConstants.PROVIDER + "/dataScope/roleCustom/{roleId}")
    String getRoleCustom(@PathVariable("roleId") Long roleId);

    /**
     * 获取部门和下级权限语句
     */
    @GetMapping(CommonConstants.PROVIDER + "/dataScope/deptAndChild/{deptId}")
    String getDeptAndChild(@PathVariable("deptId") Long deptId);
}
