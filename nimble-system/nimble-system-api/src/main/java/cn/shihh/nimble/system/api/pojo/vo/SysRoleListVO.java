package cn.shihh.nimble.system.api.pojo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author shihh
 * @since 2023/6/12
 */
@Data
public class SysRoleListVO {

    @Schema(description = "角色id")
    private Long roleId;

    @Schema(description = "名称")
    private String roleName;

    @Schema(description = "角色编码")
    private String roleCode;

    @Schema(description = "角色描述")
    private String roleDesc;

    @Schema(description = "数据权限类型")
    private Integer dsType;

    @Schema(description = "数据权限范围")
    private String dsScope;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "创建人")
    private String createBy;

}
