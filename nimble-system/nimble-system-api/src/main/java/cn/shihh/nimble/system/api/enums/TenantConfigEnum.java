package cn.shihh.nimble.system.api.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TenantConfigEnum {
    /**
     * 机构配置
     */
    TENANT_DEFAULT_ID("DEFAULT_CONFIG", "TENANT_DEFAULT_ID", "1", "默认机构ID", Long.class),
    TENANT_DEFAULT_DEPTNAME("DEFAULT_CONFIG", "TENANT_DEFAULT_DEPTNAME", "总部", "默认机构名称", String.class),
    TENANT_DEFAULT_ROLECODE("DEFAULT_CONFIG", "TENANT_DEFAULT_ROLECODE", "ADMIN_ROLE", "默认机构名称", String.class),
    TENANT_DEFAULT_ROLENAME("DEFAULT_CONFIG", "TENANT_DEFAULT_ROLENAME", "管理员", "默认机构名称", String.class),
    TENANT_DEFAULT_USERNAME("DEFAULT_CONFIG", "TENANT_DEFAULT_USERNAME", "admin", "默认用户名称", String.class),

    /**
     * 名片相关
     */
    TENANT_CARD_LANGUAGE("CARD_CONFIG", "TENANT_CARD_DEFAULT_LANGUAGE", "CN", "名片默认语言", String.class),
    TENANT_CARD_LIST_SORT_TYPE("CARD_CONFIG", "TENANT_CARD_LIST_SORT_TYPE", "1", "名片列表排序方式", Integer.class),
    ;
    /**
     * 配置类型，可为空
     */
    private final String confType;
    /**
     * 配置键，通过该值获取具体配置 confKey 不能重复
     */
    public final String confKey;
    /**
     * 配置默认值，在未配置具体配置时返回该值
     */
    public final String confDefaultValue;
    /**
     * 配置描述
     */
    public final String confDesc;
    /**
     * 类型说明 用于类型校验和类型转换
     */
    public final Class<?> cla;

    public static TenantConfigEnum getEnumByKey(String key) {
        for (TenantConfigEnum v : values()) {
            if (StrUtil.equals(v.getConfKey(), key)) {
                return v;
            }
        }
        return null;
    }
}
