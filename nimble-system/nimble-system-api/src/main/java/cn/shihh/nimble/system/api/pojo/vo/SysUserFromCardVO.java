package cn.shihh.nimble.system.api.pojo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author shihh
 * @since 2023/5/22
 */
@Data
@Schema(description = "名片-用户信息")
public class SysUserFromCardVO extends SysUserBaseVO {

    @Schema(description = "拥有的岗位列表")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<SysPostVO> postList;

}
