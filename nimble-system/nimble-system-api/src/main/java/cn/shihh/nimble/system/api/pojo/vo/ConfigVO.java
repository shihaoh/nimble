package cn.shihh.nimble.system.api.pojo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author linglong
 * @since 2022/1/13
 */
@Data
@Schema(description = "规则修改返参")
public class ConfigVO {

    @Schema(description = "配置建")
    private String key;

    @Schema(description = "配置值")
    private String confVal;

    @Schema(description = "说明")
    private String confDesc;
}
