package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.TenantBaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 部门管理(SysDept)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:47
 */
@Data
@TableName(value = "sys_dept", autoResultMap = true)
public class SysDeptEntity extends TenantBaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long deptId;

    /**
     * 
     */
    @TableField("name")
    private String name;

    /**
     * 排序
     */
    @TableField("sort_order")
    private Integer sortOrder;

    /**
     * 
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 部门配置
     */
    @TableField("dept_conf")
    private String deptConf;

}

