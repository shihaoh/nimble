package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.shihh.nimble.system.pojo.entity.SysUserPostEntity;
import cn.shihh.nimble.system.mapper.SysUserPostMapper;
import cn.shihh.nimble.system.service.SysUserPostService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户与岗位关联表(SysUserPost)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 16:26:29
 */
@Service
@RequiredArgsConstructor
public class SysUserPostServiceImpl extends ServiceImpl<SysUserPostMapper, SysUserPostEntity> implements SysUserPostService {
    @Override
    public Boolean removeByPostId(Long postId) {
        LambdaQueryWrapper<SysUserPostEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysUserPostEntity::getPostId, postId);
        return remove(qr);
    }

    @Override
    public Boolean removeByUserId(Long userId) {
        LambdaQueryWrapper<SysUserPostEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysUserPostEntity::getUserId, userId);
        return remove(qr);
    }

    @Override
    public List<Long> listPostByUserId(Long userId) {
        List<SysUserPostEntity> list =
                lambdaQuery().select(SysUserPostEntity::getPostId).eq(SysUserPostEntity::getUserId, userId).list();
        return list.stream().map(SysUserPostEntity::getPostId).collect(Collectors.toList());
    }

    @Override
    public Boolean saveUserPost(Long userId, List<Long> postIds) {
        removeByUserId(userId);
        if (CollUtil.isEmpty(postIds)) {
            return Boolean.TRUE;
        }
        List<SysUserPostEntity> list = new ArrayList<>();
        for (Long postId : postIds) {
            SysUserPostEntity entity = new SysUserPostEntity();
            entity.setPostId(postId);
            entity.setUserId(userId);
            list.add(entity);
        }
        return saveBatch(list);
    }
}

