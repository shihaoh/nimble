package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户表(SysUser)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:18
 */
@Data
@Schema(description = "用户表")
public class SysUserBO {
    
    @Schema(description = "主键ID")
    private Long userId;
    
    @Schema(description = "用户名")
    private String username;
    
    @Schema(description = "密码")
    private String password;
    
    @Schema(description = "手机号")
    private String phone;
    
    @Schema(description = "头像")
    private String avatar;
    
    @Schema(description = "拓展字段:昵称")
    private String nickname;
    
    @Schema(description = "拓展字段:姓名")
    private String name;
    
    @Schema(description = "拓展字段:邮箱")
    private String email;
    
    @Schema(description = "部门ID")
    private Long deptId;

}

