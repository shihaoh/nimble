package cn.shihh.nimble.system.pojo.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/6/15
 */
@Data
@Schema(description = "用户数量")
public class SysUserNumVO {

    @Schema(name = "total", description = "总数")
    private Integer total;

    @Schema(name = "active", description = "在职数")
    private Integer active;

    @Schema(name = "leaver", description = "离职数")
    private Integer leaver;

}
