package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shihh.nimble.system.pojo.entity.SysDeptEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 部门管理(SysDept)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:53
 */
public interface SysDeptMapper extends BaseMapper<SysDeptEntity> {
    List<SysDeptVO> getDescendantList(@Param("deptId") Long deptId);
}

