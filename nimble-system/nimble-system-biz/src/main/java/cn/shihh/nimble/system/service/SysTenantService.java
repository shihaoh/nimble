package cn.shihh.nimble.system.service;

import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.system.pojo.entity.SysTenantEntity;
import cn.shihh.nimble.system.pojo.param.AddTenantParam;
import cn.shihh.nimble.system.pojo.vo.SysTenantVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 租户表(SysTenant)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysTenantService extends IService<SysTenantEntity> {

    /**
     * 根据域名获取租户信息
     * @param domain
     * @return
     */
    SysTenantVO getTenantByDomain(String domain);

    /**
     * 判断租户域名是否存在
     * @param domain
     * @return
     */
    Boolean existsByDomain(String domain);

    /**
     * 判断租户编码是否存在
     * @param code
     * @return
     */
    Boolean existsByCode(String code);


    /**
     * 获取正常的租户
     * @return
     */
    List<SysTenantEntity> getNormalTenant();

    /**
     * 保存租户
     *
     * @param param
     * @return
     */
    Boolean saveTenant(AddTenantParam param);

    /**
     * 修改租户
     * @param tenantId
     * @param param
     * @return
     */
    Boolean updateTenant(Long tenantId, AddTenantParam param);

    /**
     * 分页查询租户信息
     *
     * @param objectPage
     * @param domain
     * @param tenantName
     * @param status
     * @return
     */
    PageVO<SysTenantVO> pageSearch(Page<SysTenantEntity> objectPage, String domain, String tenantName, Integer status);
}

