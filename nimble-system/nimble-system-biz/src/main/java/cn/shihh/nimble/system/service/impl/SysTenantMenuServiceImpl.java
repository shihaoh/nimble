package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.common.mybatis.page.PageUtil;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.system.pojo.param.AddTenantMenuParam;
import cn.shihh.nimble.system.service.SysTenantMenuService;
import cn.shihh.nimble.system.mapper.SysTenantMenuMapper;
import cn.shihh.nimble.system.pojo.entity.SysTenantMenuEntity;
import cn.shihh.nimble.system.pojo.vo.SysTenantMenuVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * (SysTenantMenu)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Service
@RequiredArgsConstructor
public class SysTenantMenuServiceImpl extends ServiceImpl<SysTenantMenuMapper, SysTenantMenuEntity> implements SysTenantMenuService {
    @Override
    public PageVO<SysTenantMenuVO> pageSearch(Page<SysTenantMenuEntity> objectPage, String key, Integer status) {
        Page<SysTenantMenuEntity> page = lambdaQuery()
                .like(StrUtil.isNotBlank(key), SysTenantMenuEntity::getName, key)
                .eq(Objects.nonNull(status), SysTenantMenuEntity::getStatus, status)
                .page(objectPage);
        return PageUtil.pageToVO(page, SysTenantMenuVO.class);
    }

    @Override
    public Boolean saveTenantMenu(AddTenantMenuParam param) {
        SysTenantMenuEntity entity = BeanUtil.copyProperties(param, SysTenantMenuEntity.class);
        return save(entity);
    }

    @Override
    public Boolean updateTenantMenu(Long id, AddTenantMenuParam param) {
        SysTenantMenuEntity entity = getById(id);
        if (Objects.isNull(entity)) {
            throw new ServiceException("租户菜单不存在");
        }
        BeanUtil.copyProperties(param, entity);
        return updateById(entity);
    }
}

