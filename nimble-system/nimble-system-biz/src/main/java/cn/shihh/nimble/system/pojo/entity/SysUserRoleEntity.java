package cn.shihh.nimble.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户角色表(SysUserRole)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_user_role", autoResultMap = true)
public class SysUserRoleEntity {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 角色ID
     */
    private Long roleId;

}

