package cn.shihh.nimble.system.service;

import cn.shihh.nimble.system.api.enums.TenantConfigEnum;
import cn.shihh.nimble.system.api.pojo.vo.ConfigVO;
import cn.shihh.nimble.system.pojo.entity.SysTenantConfigEntity;
import cn.shihh.nimble.system.pojo.param.TenantConfigSetParam;
import cn.shihh.nimble.system.api.pojo.param.ConfigParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 机构配置表(SysTenantConfig)表服务接口
 *
 * @author makejava
 * @since 2023-03-21 15:36:37
 */
public interface SysTenantConfigService extends IService<SysTenantConfigEntity> {

    /**
     * 根据键获取配置
     *
     * @param key
     * @return
     */
    SysTenantConfigEntity getConfByKey(String key);

    /**
     * 保存机构配置
     * @param param
     */
    void saveConf(ConfigParam param);

    /**
     * 插入或者修改机构配置
     *
     * @param paramVO
     */
    void saveConf(List<ConfigParam> paramVO);


    /**
     * 查询机构配置
     *
     * @param keys
     * @return
     */
    List<ConfigVO> getConfListByKeys(Collection<String> keys);


    /**
     * 查询机构配置
     *
     * @param keys
     * @return
     */
    List<ConfigVO> getConfListByEnums(Collection<TenantConfigEnum> keys);

    /**
     * 机构配置设置
     *
     * @param conf /
     * @param val /
     * @return /
     */
    Boolean configSetting(TenantConfigEnum conf, String val);

    /**
     * 机构配置获取
     *
     * @param conf /
     * @return /
     */
    ConfigVO configGet(TenantConfigEnum conf);

    /**
     * 机构配置获取
     *
     * @param key /
     * @return /
     */
    ConfigVO configGet(String key);

    /**
     * 机构配置获取
     *
     * @param type /
     * @return /
     */
    Map<String, String> configGetByType(String type);

    /**
     * 机构配置设置
     *
     * @param param /
     * @return /
     */
    Boolean configSet(TenantConfigSetParam param);
}

