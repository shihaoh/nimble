package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 岗位信息表(SysPost)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:16
 */
@Data
@Schema(description = "岗位信息表")
public class SysPostBO {
    
    @Schema(description = "岗位ID")
    private Long postId;
    
    @Schema(description = "岗位编码")
    private String postCode;
    
    @Schema(description = "岗位名称")
    private String postName;
    
    @Schema(description = "岗位排序")
    private Integer postSort;
    
    @Schema(description = "岗位描述")
    private String remark;
    
    @Schema(description = "是否删除  -1：已删除  0：正常")
    private String delFlag;
    
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "更新时间")
    private LocalDateTime updateTime;
    
    @Schema(description = "更新人")
    private String updateBy;
    
    @Schema(description = "租户ID")
    private Long tenantId;

}

