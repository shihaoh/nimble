package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.TenantBaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 菜单权限表(SysMenu)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_menu", autoResultMap = true)
public class SysMenuEntity extends TenantBaseEntity {
    /**
     * 菜单ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long menuId;

    @TableField("app_id")
    private Long appId;

    /**
     * 
     */
    @TableField("name")
    private String name;

    /**
     * 是否别名
     */
    @TableField("is_alias")
    private Boolean isAlias;

    /**
     * 
     */
    @TableField("permission")
    private String permission;

    /**
     * 
     */
    @TableField("path")
    private String path;

    /**
     * 父菜单ID
     */
    @TableField("parent_id")
    private Long parentId;

    /**
     * 
     */
    @TableField("icon")
    private String icon;

    /**
     * 是否显示
     */
    @TableField("visible")
    private String visible;

    /**
     * 排序值
     */
    @TableField("sort_order")
    private Integer sortOrder;

    /**
     * 
     */
    @TableField("keep_alive")
    private String keepAlive;

    /**
     * 
     */
    @TableField("menu_type")
    private String menuType;

    /**
     * 原始菜单id
     */
    @TableField("origin_menu_id")
    private Long originMenuId;

}

