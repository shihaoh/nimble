package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysTenantConfigEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 机构配置表(SysTenantConfig)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-21 15:36:37
 */
public interface SysTenantConfigMapper extends BaseMapper<SysTenantConfigEntity> {
}

