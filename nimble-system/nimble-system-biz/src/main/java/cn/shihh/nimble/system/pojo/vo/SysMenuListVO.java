package cn.shihh.nimble.system.pojo.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * (SysTenantMenu)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:18
 */
@Data
@Schema(description = "租户菜单列表")
public class SysMenuListVO {
    
    @Schema(description = "原始菜单id")
    private Long id;
    
    @Schema(description = "租户菜单名称")
    private String name;

    @Schema(description = "是否别名")
    private Boolean isAlias;

}

