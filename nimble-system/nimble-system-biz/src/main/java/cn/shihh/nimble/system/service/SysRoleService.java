package cn.shihh.nimble.system.service;

import cn.shihh.nimble.system.pojo.entity.SysRoleEntity;
import cn.shihh.nimble.system.pojo.param.AddRoleParam;
import cn.shihh.nimble.system.pojo.param.UpdateRoleAndMenuParam;
import cn.shihh.nimble.system.pojo.param.UpdateRoleParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * 系统角色表(SysRole)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysRoleService extends IService<SysRoleEntity> {

    /**
     * 更新角色菜单
     * @param roleId
     * @param menuIds
     * @return
     */
    Boolean updateRoleMenus(Long roleId, List<Long> menuIds);

    /**
     * 通过角色ID，删除角色
     * @param id
     * @return
     */
    Boolean removeRoleById(Long id);

    /**
     * 新增角色
     * @param param
     * @return
     */
    Boolean saveRole(AddRoleParam param);

    /**
     * 更新角色
     * @param param
     * @return
     */
    Boolean updateRole(UpdateRoleParam param);

    /**
     * 更新角色
     * @param param
     * @return
     */
    Boolean updateRoleAndMenu(UpdateRoleAndMenuParam param);

    /**
     * 启用/停用角色
     * @param roleId
     * @return
     */
    Boolean enabled(Long roleId);

    /**
     * 获取租户默认角色
     * @param tenantId
     * @return
     */
    SysRoleEntity getDefaultRoleByTenantId(Long tenantId);

    /**
     * 角色编码是否存在
     * @param roleCode
     * @return
     */
    Boolean existsRoleCode(String roleCode);

    /**
     * 保存租户默认角色菜单
     * @param tenantId
     * @param menuIds
     * @return
     */
    Boolean saveDefaultRoleMenus(Long tenantId, Collection<Long> menuIds);
}

