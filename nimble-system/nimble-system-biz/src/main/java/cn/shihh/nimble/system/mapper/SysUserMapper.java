package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.vo.SysUserVO;
import cn.shihh.nimble.common.mybatis.annotation.DataColumn;
import cn.shihh.nimble.common.mybatis.annotation.DataPermission;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserFromCardVO;
import cn.shihh.nimble.system.pojo.entity.SysUserEntity;
import cn.shihh.nimble.system.pojo.vo.SysUserNumVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * 用户表(SysUser)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysUserMapper extends BaseMapper<SysUserEntity> {
    List<SysUserVO> pageSearch(Page<SysUserEntity> objectPage,
                               @Param("key") String key,
                               @Param("deptId") Long deptId,
                               @Param("roleId") Long roleId,
                               @Param("status") Integer status);

    List<SysUserBaseVO> listByRoleIds(@Param("roleIds") Collection<Long> roleIds);

    List<SysUserBaseVO> listByKeywords(String keywords);

    @DataPermission({
            @DataColumn(key = "deptName", value = "u.dept_id"),
            @DataColumn(key = "userName", value = "user_id")
    })
    List<SysUserBaseVO> listByPermissions(@Param("deptId") Long deptId);

    @DataPermission({
            @DataColumn(key = "deptName", value = "u.dept_id"),
            @DataColumn(key = "userName", value = "u.user_id")
    })
    List<SysUserFromCardVO> listFromCardByPermissions();

    List<SysUserFromCardVO> getUserFromCardByUserIds(@Param("userIds") List<Long> userIds);

    @DataPermission({
            @DataColumn,
            @DataColumn(key = "userName", value = "user_id")
    })
    List<SysUserVO> listPermissionSearch(@Param("status") Integer status,
                                         @Param("key") String key);

    SysDeptVO getUserDeptInfoByUserId(Long userId);

    SysUserNumVO countByStatus();
}

