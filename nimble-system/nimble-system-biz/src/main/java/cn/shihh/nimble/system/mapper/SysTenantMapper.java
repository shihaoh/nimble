package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysTenantEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 租户表(SysTenant)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysTenantMapper extends BaseMapper<SysTenantEntity> {
}

