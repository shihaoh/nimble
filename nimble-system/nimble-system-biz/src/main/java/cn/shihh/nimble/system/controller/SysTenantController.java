package cn.shihh.nimble.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.bean.BeanUtil;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.pojo.entity.SysTenantEntity;
import cn.shihh.nimble.system.pojo.param.AddTenantParam;
import cn.shihh.nimble.system.pojo.vo.SysTenantVO;
import cn.shihh.nimble.system.service.SysTenantService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author shihh
 * @since 2023/3/21
 */
@Tag(name = "租户管理模块")
@RestController
@RequestMapping("/sys/tenant")
@RequiredArgsConstructor
public class SysTenantController {

    private final SysTenantService sysTenantService;

    @Operation(summary = "根据域名获取租户信息")
    @GetMapping("/domain")
    public R<SysTenantVO> getByDomain(@RequestParam String domain) {
        return R.ok(sysTenantService.getTenantByDomain(domain));
    }

    @Operation(summary = "分页查询租户信息")
    @GetMapping("/page")
    @SaCheckPermission("admin_systenant_get")
    public R<PageVO<SysTenantVO>> page(@RequestParam(value = "current", defaultValue = "1") Integer current,
                                       @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                       @RequestParam(value = "domain", required = false) String domain,
                                       @RequestParam(value = "tenantName", required = false) String tenantName,
                                       @RequestParam(value = "status", required = false) Integer status) {
        PageVO<SysTenantVO> res = sysTenantService.pageSearch(new Page<>(current, pageSize), domain, tenantName, status);
        return R.ok(res);
    }

    @Operation(summary = "获取租户信息")
    @GetMapping("/{id}")
    @SaCheckPermission("admin_systenant_get")
    public R<SysTenantVO> getById(@PathVariable Long id) {
        SysTenantEntity entity = sysTenantService.getById(id);
        return R.ok(BeanUtil.toBean(entity, SysTenantVO.class));
    }

    @Operation(summary = "新增租户信息")
    @PostMapping
    @SaCheckPermission("admin_systenant_add")
    public R<Boolean> add(@RequestBody AddTenantParam param) {
        return R.ok(sysTenantService.saveTenant(param));
    }

    @Operation(summary = "修改租户信息")
    @PutMapping("/{tenantId}")
    @SaCheckPermission("admin_systenant_edit")
    public R<Boolean> update(@PathVariable Long tenantId, @RequestBody AddTenantParam param) {
        return R.ok(sysTenantService.updateTenant(tenantId, param));
    }

    @Operation(summary = "删除租户信息")
    @DeleteMapping("/{tenantId}")
    @SaCheckPermission("admin_systenant_del")
    public R<Boolean> delete(@PathVariable Long tenantId) {
        return R.ok(sysTenantService.removeById(tenantId));
    }

    @Operation(summary = "获取正常的租户")
    @GetMapping("/valid")
    public R<List<SysTenantVO>> getValidTenant() {
        List<SysTenantEntity> list = sysTenantService.getNormalTenant();
        return R.ok(BeanUtil.copyToList(list, SysTenantVO.class));
    }

}
