package cn.shihh.nimble.system.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.lang.tree.Tree;
import cn.shihh.nimble.common.mybatis.tenant.TenantContextHolder;
import cn.shihh.nimble.common.satoken.utils.LoginHelper;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.pojo.param.AddAppParam;
import cn.shihh.nimble.system.pojo.param.AddMenuParam;
import cn.shihh.nimble.system.pojo.vo.SysAppMenuTreeVO;
import cn.shihh.nimble.system.pojo.vo.SysApplicationVO;
import cn.shihh.nimble.system.service.SysApplicationService;
import cn.shihh.nimble.system.pojo.param.AddTenantApplicationParam;
import cn.shihh.nimble.system.pojo.vo.SysApplicationEntryVO;
import cn.shihh.nimble.system.pojo.vo.SysMenuListVO;
import cn.shihh.nimble.system.service.SysMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author shihh
 * @since 2023/5/15
 */
@Tag(name = "应用管理", description = "应用管理相关接口")
@Slf4j
@RestController
@RequestMapping("/sys/application")
@RequiredArgsConstructor
public class SysApplicationController {

    private final SysApplicationService sysApplicationService;
    private final SysMenuService sysMenuService;

    @Operation(summary = "addApp", description = "添加应用")
    @PostMapping
    public R<Boolean> add(@RequestBody AddAppParam param) {
        return R.ok(sysApplicationService.addApplication(param));
    }

    @Operation(summary = "delApp", description = "删除应用")
    @DeleteMapping("/{appId}")
    public R<Boolean> del(@PathVariable Long appId) {
        return R.ok(sysApplicationService.deleteApplication(appId));
    }

    @Operation(summary = "updateApp", description = "修改应用")
    @PutMapping("/{appId}")
    public R<Boolean> update(@PathVariable Long appId, @RequestBody AddAppParam param) {
        return R.ok(sysApplicationService.updateApplication(appId, param));
    }

    @Operation(summary = "getAppById", description = "查询应用")
    @GetMapping("/{appId}")
    public R<SysApplicationVO> get(@PathVariable Long appId) {
        return R.ok(sysApplicationService.getApplication(appId));
    }

    @Operation(summary = "getAppList", description = "查询应用列表")
    @GetMapping("/list")
    public R<List<SysApplicationVO>> list() {
        return R.ok(sysApplicationService.getTenantApplication(TenantContextHolder.getTenantId()));
    }

    @Operation(summary = "getEntryAppList", description = "查询基座应用列表")
    @GetMapping("/entryList")
    @SaIgnore
    public R<List<SysApplicationEntryVO>> entryList() {
        return R.ok(sysApplicationService.getEntryApplication());
    }

    @Operation(summary = "addAppMenu", description = "添加应用菜单")
    @PostMapping("/{appCode}/menu")
    public R<Boolean> addMenu(@PathVariable String appCode, @RequestBody AddMenuParam param) {
        Boolean res = sysApplicationService.saveApplicationMenu(appCode, param);
        sysApplicationService.deleteMenuApplicationCache(appCode);
        return R.ok(res);
    }

    @Operation(summary = "updateAppMenu", description = "修改应用菜单")
    @PutMapping("/menu/{menuId}")
    public R<Boolean> updateMenu(@PathVariable Long menuId, @RequestBody AddMenuParam param) {
        Long appId = sysMenuService.updateMenu(menuId, param);
        Optional.of(sysApplicationService.getById(appId)).ifPresent(app -> sysApplicationService.deleteMenuApplicationCache(app.getCode()));
        return R.ok();
    }

    @Operation(summary = "delAppMenu", description = "删除应用菜单")
    @DeleteMapping("/{appCode}/menu")
    public R<Boolean> delMenu(@PathVariable String appCode, @RequestBody List<Long> menuIds) {
        Boolean res = sysApplicationService.deleteApplicationMenu(menuIds);
        sysApplicationService.deleteMenuApplicationCache(appCode);
        return R.ok(res);
    }

    @Operation(summary = "getAppMenuTree", description = "获取应用下的菜单树")
    @GetMapping("/{appCode}/menu/tree")
    public R<List<Tree<Long>>> getMenuTree(@PathVariable String appCode) {
        return R.ok(sysApplicationService.getApplicationMenuTree(appCode));
    }

    @Operation(summary = "getTenantAppMenu", description = "添加租户应用菜单")
    @PostMapping("/tenant")
    public R<Boolean> addTenantApplication(@Validated @RequestBody AddTenantApplicationParam param) {
        return R.ok(sysApplicationService.addTenantApplication(param));
    }

    @Operation(summary = "getTenantAppMenuTree", description = "获取租户应用菜单树")
    @GetMapping("/tenant/menu/tree")
    public R<List<SysAppMenuTreeVO>> getTenantAppMenuTree() {
        return R.ok(sysApplicationService.getTenantApplicationMenuTree(TenantContextHolder.getTenantId()));
    }

    @Operation(summary = "getTenantAppMenuList", description = "获取租户菜单列表")
    @GetMapping("/tenant/menu/{tenantId}")
    public R<Map<Long, List<SysMenuListVO>>> getTenantAppMenuTree(@PathVariable Long tenantId) {
        return R.ok(sysApplicationService.getTenantMenuList(tenantId));
    }

    @Operation(summary = "getAppMenuByRole", description = "根据角色获取应用菜单")
    @GetMapping("/roleAppMenu/{roleId}")
    public R<Map<Long, Set<Long>>> roleAppMenu(@PathVariable Long roleId) {
        return R.ok(sysApplicationService.roleAppMenu(roleId));
    }

    @Operation(summary = "getAppByUser", description = "获取当前用户应用列表")
    @GetMapping("/user")
    public R<List<SysApplicationVO>> currentUserApp() {
        return R.ok(sysApplicationService.getUserApplication(LoginHelper.getUserId()));
    }

    @Operation(summary = "getAppMenuByUser", description = "获取当前用户应用菜单列表")
    @GetMapping("/user/{appId}")
    public R<List<Tree<Long>>> currentUserApp(@PathVariable Long appId) {
        return R.ok(sysApplicationService.getUserMenuApplication(LoginHelper.getUserId(), appId));
    }

    @Operation(summary = "getAppMenuByUserFromCode", description = "获取当前用户应用菜单列表")
    @GetMapping("/user/code/{code}")
    public R<List<Tree<Long>>> currentUserAppByCode(@PathVariable String code) {
        return R.ok(sysApplicationService.getUserMenuApplicationByCode(LoginHelper.getUserId(), code));
    }
}
