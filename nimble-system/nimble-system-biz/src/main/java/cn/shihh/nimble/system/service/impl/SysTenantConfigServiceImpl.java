package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.common.satoken.utils.LoginHelper;
import cn.shihh.nimble.system.api.enums.TenantConfigEnum;
import cn.shihh.nimble.system.api.pojo.param.ConfigParam;
import cn.shihh.nimble.system.api.pojo.vo.ConfigVO;
import cn.shihh.nimble.system.mapper.SysTenantConfigMapper;
import cn.shihh.nimble.system.pojo.entity.SysTenantConfigEntity;
import cn.shihh.nimble.system.pojo.param.TenantConfigSetParam;
import cn.shihh.nimble.system.service.SysTenantConfigService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 机构配置表(SysTenantConfig)表服务实现类
 *
 * @author makejava
 * @since 2023-03-21 15:36:37
 */
@Service
@RequiredArgsConstructor
public class SysTenantConfigServiceImpl extends ServiceImpl<SysTenantConfigMapper, SysTenantConfigEntity> implements SysTenantConfigService {
    @Override
    public SysTenantConfigEntity getConfByKey(String key) {
        LambdaQueryWrapper<SysTenantConfigEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysTenantConfigEntity::getConfKey, key);
        return baseMapper.selectOne(qr);
    }

    @Override
    public void saveConf(ConfigParam param) {
        TenantConfigEnum anEnum = TenantConfigEnum.getEnumByKey(param.getKey());
        if (ObjectUtil.isNull(anEnum)) {
            throw new ServiceException("根据传入key未找到配置数据，请传入正确的key");
        }
        if (anEnum.cla != String.class) {
            // 判断传值类型是否匹配
            Object vla = Convert.convert(anEnum.cla, param.getConfigVal());
            if (ObjectUtil.isEmpty(vla)) {
                throw new ServiceException("val传值类型和与设值类型不匹配");
            }
        }
        SysTenantConfigEntity addConf = BeanUtil.toBean(anEnum, SysTenantConfigEntity.class);
        SysTenantConfigEntity tenantConfig = getConfByKey(param.getKey());
        if (ObjectUtil.isNotEmpty(tenantConfig)) {
            addConf.setId(tenantConfig.getId());
        }
        addConf.setTenantId(LoginHelper.getTenantId());
        addConf.setConfValue(param.getConfigVal());
        saveOrUpdate(addConf);
    }

    @Override
    public void saveConf(List<ConfigParam> paramVO) {
        for (ConfigParam vo : paramVO) {
            saveConf(vo);
        }
    }

    private List<SysTenantConfigEntity> getTenantConfListByKey(Collection<String> keys) {
        return this.list(Wrappers.<SysTenantConfigEntity>lambdaQuery()
                .in(SysTenantConfigEntity::getConfKey, keys));
    }

    @Override
    public List<ConfigVO> getConfListByKeys(Collection<String> keys) {
        List<SysTenantConfigEntity> list = getTenantConfListByKey(keys);
        List<ConfigVO> voList = Lists.newArrayList();
        for (String str : keys) {
            TenantConfigEnum anEnum = TenantConfigEnum.getEnumByKey(str);
            if (ObjectUtil.isNull(anEnum)) {
                throw new ServiceException("参数错误");
            }
            ConfigVO vo = new ConfigVO();
            vo.setKey(anEnum.confKey);
            vo.setConfVal(anEnum.confDefaultValue);
            vo.setConfDesc(anEnum.confDesc);
            Optional<SysTenantConfigEntity> first = list.stream().filter(m -> m.getConfKey().equals(str)).findFirst();
            if (first.isPresent()) {
                SysTenantConfigEntity config = first.get();
                vo.setConfVal(config.getConfValue());
            }
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<ConfigVO> getConfListByEnums(Collection<TenantConfigEnum> keys) {
        if (CollUtil.isNotEmpty(keys)) {
            List<ConfigVO> confList = Lists.newArrayList();
            for (TenantConfigEnum key : keys) {
                confList.add(configGet(key));
            }
            return confList;
        }
        return Collections.emptyList();
    }

    @Override
    public Boolean configSetting(TenantConfigEnum conf, String val) {
        SysTenantConfigEntity config = getConfByKey(conf.confKey);
        if (ObjectUtil.isEmpty(config)) {
            SysTenantConfigEntity addConf = BeanUtil.toBean(conf, SysTenantConfigEntity.class);
            addConf.setConfValue(val);
            baseMapper.insert(addConf);
        } else {
            config.setConfValue(val);
            baseMapper.updateById(config);
        }
        return Boolean.TRUE;
    }

    @Override
    public ConfigVO configGet(TenantConfigEnum conf) {
        SysTenantConfigEntity entity = getConfByKey(conf.confKey);
        if (ObjectUtil.isEmpty(entity)) {
            ConfigVO resConf = new ConfigVO();
            resConf.setKey(conf.confKey);
            resConf.setConfVal(conf.confDefaultValue);
            resConf.setConfDesc(conf.confDesc);
            return resConf;
        }
        return entityToVO(entity);
    }

    private ConfigVO entityToVO(SysTenantConfigEntity entity) {
        ConfigVO res = new ConfigVO();
        res.setKey(entity.getConfKey());
        res.setConfVal(entity.getConfValue());
        res.setConfDesc(entity.getConfDesc());
        return res;
    }

    @Override
    public ConfigVO configGet(String key) {
        SysTenantConfigEntity entity = getConfByKey(key);
        if (ObjectUtil.isEmpty(entity)) {
            return null;
        }
        return entityToVO(entity);
    }

    @Override
    public Map<String, String> configGetByType(String type) {
        LambdaQueryWrapper<SysTenantConfigEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysTenantConfigEntity::getConfType, type);
        List<SysTenantConfigEntity> list = list(qr);
        if (CollUtil.isNotEmpty(list)) {
            Map<String, String> map = Maps.newHashMap();
            for (SysTenantConfigEntity entity : list) {
                map.put(entity.getConfKey(), entity.getConfValue());
            }
            return map;
        }
        return MapUtil.empty();
    }

    @Override
    public Boolean configSet(TenantConfigSetParam param) {
        SysTenantConfigEntity entity = getConfByKey(param.getKey());
        if (ObjectUtil.isEmpty(entity)) {
            SysTenantConfigEntity addConf = new SysTenantConfigEntity();
            addConf.setConfKey(param.getKey());
            addConf.setConfValue(param.getConfVal());
            addConf.setConfDesc(param.getConfDesc());
            return save(addConf);
        } else {
            entity.setConfType(param.getConfType());
            entity.setConfValue(param.getConfVal());
            entity.setConfDesc(param.getConfDesc());
            return updateById(entity);
        }
    }
}

