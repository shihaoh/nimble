package cn.shihh.nimble.system.pojo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.List;

/**
 * @author shihh
 * @since 2023/3/20
 */
@Data
@Schema(description = "新增角色参数")
public class AddRoleParam {

    @Schema(description = "角色名称")
    @NotBlank(message = "角色名称不能为空")
    private String roleName;

    @Schema(description = "角色编码")
    private String roleCode;

    @Schema(description = "角色描述")
    private String roleDesc;

    @Schema(description = "数据权限类型")
    @NotNull(message = "数据权限类型不能为空")
    private Integer dsType;

    @Schema(description = "数据权限范围")
    private String dsScope;

    @Schema(description = "菜单编号列表")
    private List<Long> menuIds;

    @Schema(description = "是否启用")
    private String enabled;

}
