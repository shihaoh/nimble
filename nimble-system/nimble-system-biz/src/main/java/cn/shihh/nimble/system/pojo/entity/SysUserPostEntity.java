package cn.shihh.nimble.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户与岗位关联表(SysUserPost)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 16:26:29
 */
@Data
@TableName(value = "sys_user_post", autoResultMap = true)
public class SysUserPostEntity {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 岗位ID
     */
    private Long postId;

}

