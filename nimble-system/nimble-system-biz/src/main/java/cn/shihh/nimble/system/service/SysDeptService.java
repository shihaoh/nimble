package cn.shihh.nimble.system.service;

import cn.hutool.core.lang.tree.Tree;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.pojo.entity.SysDeptEntity;
import cn.shihh.nimble.system.pojo.param.AddDeptParam;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 部门管理(SysDept)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:51
 */
public interface SysDeptService extends IService<SysDeptEntity> {

    /**
     * 查询部门树菜单
     * @return 树
     */
    List<Tree<Long>> selectTree();

    /**
     * 添加信息部门
     *
     * @param param
     * @return
     */
    Boolean saveDept(AddDeptParam param);

    /**
     * 删除部门
     * @param id 部门 ID
     * @return 成功、失败
     */
    Boolean removeDeptById(Long id);

    /**
     * 更新部门
     *
     * @param deptId
     * @param param  部门信息
     * @return 成功、失败
     */
    Boolean updateDeptById(Long deptId, AddDeptParam param);

    /**
     * 获取部门的所有子部门
     * @param deptId
     * @return
     */
    List<SysDeptVO> getDescendantList(Long deptId);

    /**
     * 根据','分割的部门id字符串获取部门列表
     * @param deptIdStr
     * @return
     */
    JSONArray getDeptArrayByDeptIdStrs(String deptIdStr);
}

