package cn.shihh.nimble.system.pojo.param;

import cn.shihh.nimble.system.pojo.vo.SysMenuListVO;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

/**
 * @author shihh
 * @since 2023/5/15
 */
@Data
@Schema(description = "租户添加应用参数")
public class AddTenantApplicationParam {

    @Schema(description = "租户ID")
    @NotNull(message = "租户ID不能为空")
    private Long tenantId;

    @Schema(description = "分配租户原始菜单列表")
    private List<SysMenuListVO> menuList;

}
