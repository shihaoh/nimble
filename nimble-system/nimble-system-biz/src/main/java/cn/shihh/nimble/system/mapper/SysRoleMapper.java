package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysRoleEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 系统角色表(SysRole)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

    /**
     * 通过用户ID，查询角色信息
     * @param userId
     * @return
     */
    List<SysRoleEntity> listRolesByUserId(Long userId);
}

