package cn.shihh.nimble.system.service.impl;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.body.LoginUser;
import cn.shihh.nimble.common.body.RoleBody;
import cn.shihh.nimble.common.constant.CacheConstants;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.common.mybatis.page.PageUtil;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.common.redis.util.RedisUtil;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserFromCardVO;
import cn.shihh.nimble.system.mapper.SysUserMapper;
import cn.shihh.nimble.system.pojo.entity.SysMenuEntity;
import cn.shihh.nimble.system.pojo.entity.SysUserEntity;
import cn.shihh.nimble.system.pojo.param.AddUserParam;
import cn.shihh.nimble.system.pojo.param.UpdateUserParam;
import cn.shihh.nimble.system.pojo.vo.SysUserNumVO;
import cn.shihh.nimble.system.pojo.vo.SysUserVO;
import cn.shihh.nimble.system.service.SysMenuService;
import cn.shihh.nimble.system.service.SysUserPostService;
import cn.shihh.nimble.system.service.SysUserRoleService;
import cn.shihh.nimble.system.service.SysUserService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 用户表(SysUser)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {

    private final SysUserPostService sysUserPostService;
    private final SysUserRoleService sysUserRoleService;
    private final SysMenuService sysMenuService;

    @Override
    public PageVO<SysUserVO> pageSearch(Page<SysUserEntity> objectPage, String key, Long deptId, Long roleId, Integer status) {
        List<SysUserVO> resList = baseMapper.pageSearch(objectPage, key, deptId, roleId, status);
        return PageUtil.pageToVO(objectPage, resList);
    }

    @Override
    public List<SysUserVO> listUsers(Integer status, String key) {
        return baseMapper.listPermissionSearch(status, key);
    }

    @Override
    public SysUserVO getUserById(Long userId) {
        SysUserEntity entity = getById(userId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("用户不存在");
        }
        SysUserVO vo = BeanUtil.copyProperties(entity, SysUserVO.class);
        vo.setPostList(sysUserPostService.listPostByUserId(userId));
        vo.setRoleList(sysUserRoleService.listRoleIdByUserId(userId));
        return vo;
    }

    @Override
    public Boolean existsByPhone(String phone, Long excludeUserId) {
        return lambdaQuery()
                .eq(SysUserEntity::getPhone, phone)
                .ne(ObjectUtil.isNotNull(excludeUserId), SysUserEntity::getUserId, excludeUserId)
                .exists();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveUser(AddUserParam param) {
        if (existsByPhone(param.getPhone(), null)) {
            throw new ServiceException("手机号已存在");
        }
        if (StrUtil.isNotBlank(param.getUserCode())) {
            if (lambdaQuery().eq(SysUserEntity::getUserCode, param.getUserCode()).exists()) {
                throw new ServiceException("用户编码已存在");
            }
        } else {
            String userCode;
            // 生成唯一的用户编码，6位随机字符串
            do {
                userCode = RandomUtil.randomString(6);
            } while (lambdaQuery().eq(SysUserEntity::getUserCode, userCode).exists());
            param.setUserCode(userCode);
        }
        SysUserEntity entity = BeanUtil.copyProperties(param, SysUserEntity.class);
        String password = param.getPassword();
        if (StrUtil.isBlank(password)) {
            password = StrUtil.subSufByLength(param.getPhone(), 6);
        }
        entity.setPassword(BCrypt.hashpw(password));
        save(entity);

        if (CollUtil.isNotEmpty(param.getPost())) {
            sysUserPostService.saveUserPost(entity.getUserId(), param.getPost());
        }
        if (CollUtil.isNotEmpty(param.getRole())) {
            sysUserRoleService.saveUserRole(entity.getUserId(), param.getRole());
        }
        return Boolean.TRUE;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = CacheConstants.USER_DETAILS, key = "#param.phone")
    public Boolean updateUser(UpdateUserParam param) {
        SysUserEntity entity = getById(param.getId());
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("用户不存在");
        }
        if (existsByPhone(param.getPhone(), param.getId())) {
            throw new ServiceException("手机号重复");
        }
        if (StrUtil.isNotBlank(param.getPhone()) && ObjectUtil.notEqual(param.getPhone(), entity.getPhone())) {
            RedisUtil.delete(CacheConstants.USER_DETAILS + "::" + entity.getPhone());
        }
        BeanUtil.copyProperties(param, entity);
        String password = param.getPassword();
        if (StrUtil.isNotBlank(password)) {
            entity.setPassword(BCrypt.hashpw(password));
        } else {
            entity.setPassword(null);
        }
        sysUserPostService.removeByUserId(param.getId());
        sysUserRoleService.removeByUserId(param.getId());
        sysUserPostService.saveUserPost(entity.getUserId(), param.getPost());
        sysUserRoleService.saveUserRole(entity.getUserId(), param.getRole());
        return updateById(entity);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeUser(Long userId) {
        SysUserEntity entity = getById(userId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("用户不存在");
        }
        RedisUtil.delete(CacheConstants.USER_DETAILS + "::" + entity.getPhone());
        sysUserPostService.removeByUserId(userId);
        sysUserRoleService.removeByUserId(userId);
        return removeById(userId);
    }

    @Override
    public LoginUser getLoginUserByPhone(String phone) {
        SysUserEntity entity = getUserByPhone(phone);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("用户不存在");
        }
        if (ObjectUtil.notEqual(entity.getStatus(), 0)) {
            throw new ServiceException("用户已离职或被锁定");
        }
        return userToLoginUser(entity);
    }

    private LoginUser userToLoginUser(SysUserEntity entity) {
        LoginUser loginUser = BeanUtil.copyProperties(entity, LoginUser.class);
        List<RoleBody> roleBodies = sysUserRoleService.listRoleByUserId(entity.getUserId());
        if (CollUtil.isEmpty(roleBodies)) {
            throw new ServiceException("登录失败，用户角色已失效，如需使用请联系管理员~");
        }
        loginUser.setRoles(roleBodies);

        // 角色权限设置
        List<Long> roleIds = new ArrayList<>();
        loginUser.setRolePermission(Sets.newHashSet());
        for (RoleBody r : roleBodies) {
            roleIds.add(r.getRoleId());
            loginUser.getRolePermission().add(r.getRoleCode());
        }

        // 菜单权限设置
        List<SysMenuEntity> menus = sysMenuService.findMenuByRoleIds(roleIds);
        loginUser.setMenuPermission(Sets.newHashSet());
        if (CollUtil.isNotEmpty(menus)) {
            for (SysMenuEntity m : menus) {
                if (StrUtil.isNotBlank(m.getPermission())) {
                    loginUser.getMenuPermission().add(m.getPermission());
                }
            }
        }

        return loginUser;
    }

    @Override
    public LoginUser getLoginUserById(Long userId) {
        SysUserEntity entity = getById(userId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("用户不存在");
        }
        return userToLoginUser(entity);
    }

    @Override
    public SysUserEntity getUserByPhone(String phone) {
        return lambdaQuery()
                .eq(SysUserEntity::getPhone, phone)
                .one();
    }

    @Override
    public List<SysUserBaseVO> listByRoleIds(Collection<Long> roleIds) {
        return baseMapper.listByRoleIds(roleIds);
    }

    @Override
    public List<SysUserBaseVO> listByPermissions(Long deptId) {
        return baseMapper.listByPermissions(deptId);
    }

    @Override
    public List<SysUserFromCardVO> listFromCardByPermissions() {
        return baseMapper.listFromCardByPermissions();
    }

    @Override
    public List<SysUserBaseVO> listByKeywords(String keywords) {
        return baseMapper.listByKeywords(keywords);
    }

    @Override
    public List<SysUserFromCardVO> getUserFromCardByUserIds(List<Long> userIds) {
        return baseMapper.getUserFromCardByUserIds(userIds);
    }

    @Override
    public SysDeptVO getUserDeptInfoByUserId(Long userId) {
        return baseMapper.getUserDeptInfoByUserId(userId);
    }

    @Override
    public SysUserNumVO countByStatus() {
        return baseMapper.countByStatus();
    }
}

