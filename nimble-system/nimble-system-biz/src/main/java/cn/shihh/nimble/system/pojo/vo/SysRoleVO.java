package cn.shihh.nimble.system.pojo.vo;


import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 系统角色表(SysRole)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:16
 */
@Data
@Schema(description = "系统角色表")
public class SysRoleVO {
    
    @Schema(description = "角色id")
    private Long roleId;
    
    @Schema(description = "名称")
    private String roleName;
    
    @Schema(description = "角色编码")
    private String roleCode;
    
    @Schema(description = "角色描述")
    private String roleDesc;
    
    @Schema(description = "数据权限类型")
    private Integer dsType;
    
    @Schema(description = "数据权限范围")
    private String dsScope;

    @Schema(description = "数据权限范围部门数组")
    private JSONArray dsScopeArray;

    @Schema(description = "启用")
    private Boolean enabled;

    @Schema(description = "用户数")
    private Integer userCount;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "创建人")
    private String createBy;

    @Schema(description = "菜单ids")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Long> menuIds;
}

