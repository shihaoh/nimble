package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysApplicationEntity;
import cn.shihh.nimble.system.pojo.entity.SysMenuEntity;
import cn.shihh.nimble.system.pojo.vo.SysApplicationVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 应用表(SysApplication)表数据库访问层
 *
 * @author makejava
 * @since 2023-05-15 14:47:28
 */
public interface SysApplicationMapper extends BaseMapper<SysApplicationEntity> {
    List<SysMenuEntity> getApplicationMenuByRoleId(@Param("roleId") Long roleId);

    List<SysApplicationVO> getApplicationByUserId(@Param("userId") Long userId);

    List<SysMenuEntity> getApplicationMenuByUserIdAndAppId(@Param("userId") Long userId,
                                                           @Param("appId") Long appId);

    List<SysMenuEntity> getApplicationMenuByUserIdAndAppCode(@Param("userId") Long userId,
                                                             @Param("code") String code);
}

