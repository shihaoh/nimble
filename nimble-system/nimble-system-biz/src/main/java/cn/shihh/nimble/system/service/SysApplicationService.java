package cn.shihh.nimble.system.service;

import cn.hutool.core.lang.tree.Tree;
import cn.shihh.nimble.system.pojo.entity.SysApplicationEntity;
import cn.shihh.nimble.system.pojo.vo.SysAppMenuTreeVO;
import cn.shihh.nimble.system.pojo.param.AddAppParam;
import cn.shihh.nimble.system.pojo.param.AddMenuParam;
import cn.shihh.nimble.system.pojo.param.AddTenantApplicationParam;
import cn.shihh.nimble.system.pojo.vo.SysApplicationEntryVO;
import cn.shihh.nimble.system.pojo.vo.SysApplicationVO;
import cn.shihh.nimble.system.pojo.vo.SysMenuListVO;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 应用表(SysApplication)表服务接口
 *
 * @author makejava
 * @since 2023-05-15 14:47:28
 */
public interface SysApplicationService extends IService<SysApplicationEntity> {

    /**
     * 通过code获取应用
     * @param code
     * @return
     */
    SysApplicationEntity getByCode(String code);

    /**
     * 添加应用
     * @param param
     * @return
     */
    Boolean addApplication(AddAppParam param);

    /**
     * 应用添加菜单
     * @param appCode
     * @param param
     * @return
     */
    Boolean saveApplicationMenu(String appCode, AddMenuParam param);

    /**
     * 应用删除菜单
     * @param menuIds
     * @return
     */
    Boolean deleteApplicationMenu(List<Long> menuIds);

    /**
     * 删除菜单对应应用缓存
     * @param appCode
     * @return
     */
    void deleteMenuApplicationCache(String appCode);

    /**
     * 获取应用菜单树
     * @param appCode
     * @return
     */
    List<Tree<Long>> getApplicationMenuTree(String appCode);

    /**
     * 更新应用
     * @param appId
     * @param param
     * @return
     */
    Boolean updateApplication(Long appId, AddAppParam param);

    /**
     * 删除应用
     * @param appId
     * @return
     */
    Boolean deleteApplication(Long appId);

    /**
     * 删除租户所有应用
     * @param tenantId
     * @return
     */
    Boolean deleteApplicationByTenantId(Long tenantId);

    /**
     * 获取应用
     * @param appId
     * @return
     */
    SysApplicationVO getApplication(Long appId);

    /**
     * 租户相关操作
     */
    /**
     * 添加租户应用
     * @param param
     * @return
     */
    Boolean addTenantApplication(AddTenantApplicationParam param);

    /**
     * 获取租户应用
     * @param tenantId
     * @return
     */
    List<SysApplicationVO> getTenantApplication(Long tenantId);

    /**
     * 获取基座应用
     * @return
     */
    List<SysApplicationEntryVO> getEntryApplication();

    /**
     * 获取租户应用菜单树
     * @param tenantId
     * @return
     */
    List<SysAppMenuTreeVO> getTenantApplicationMenuTree(Long tenantId);

    /**
     * 获取租户菜单列表
     *
     * @param tenantId
     * @return
     */
    Map<Long, List<SysMenuListVO>> getTenantMenuList(Long tenantId);

    /**
     * 根据原始应用id获取租户应用列表
     * @param originAppIds
     * @return
     */
    List<SysApplicationEntity> getTenantApplicationByOriginAppIds(Collection<Long> originAppIds);

    /**
     * 根据角色查询应用
     *
     * @param roleId
     * @return key为应用id，value为菜单id集合
     */
    Map<Long, Set<Long>> roleAppMenu(Long roleId);

    /**
     * 获取人员绑定应用
     * @param userId
     * @return
     */
    List<SysApplicationVO> getUserApplication(Long userId);

    /**
     * 获取人员绑定应用菜单
     * @param userId
     * @param appId
     * @return
     */
    List<Tree<Long>> getUserMenuApplication(Long userId, Long appId);

    /**
     * 获取用户绑定应用菜单，根据code
     * @param userId
     * @param code
     * @return
     */
    List<Tree<Long>> getUserMenuApplicationByCode(Long userId, String code);
}

