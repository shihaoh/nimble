package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 部门关系表(SysDeptRelation)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:15
 */
@Data
@Schema(description = "部门关系表")
public class SysDeptRelationBO {
    
    @Schema(description = "祖先节点")
    private Long ancestor;
    
    @Schema(description = "后代节点")
    private Long descendant;

}

