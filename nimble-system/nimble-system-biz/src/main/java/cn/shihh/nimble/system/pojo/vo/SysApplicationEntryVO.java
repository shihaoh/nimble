package cn.shihh.nimble.system.pojo.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 应用表(SysApplication)表实体类
 *
 * @author makejava
 * @since 2023-05-15 15:45:43
 */
@Data
@Schema(description = "基座应用")
public class SysApplicationEntryVO {
    
    @Schema(description = "应用入口")
    private String entry;
    
    @Schema(description = "应用图标")
    private String icon;
    
    @Schema(description = "应用编码")
    private String code;

}

