package cn.shihh.nimble.system.service;

import cn.hutool.core.lang.tree.Tree;
import cn.shihh.nimble.system.pojo.entity.SysMenuEntity;
import cn.shihh.nimble.system.pojo.param.AddMenuParam;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * 菜单权限表(SysMenu)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysMenuService extends IService<SysMenuEntity> {

    /**
     * 通过角色编号查询URL 权限
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    List<Long> findMenuIdByRoleId(Long roleId);

    /**
     * 通过角色编号查询URL 权限
     *
     * @param roleId 角色ID
     * @param appCode 应用编码
     * @return 菜单列表
     */
    List<Long> findMenuIdByRoleIdAndAppCode(Long roleId, String appCode);

    /**
     * 通过角色编号查询URL 权限
     *
     * @param roleIds 角色ID
     * @return 菜单列表
     */
    List<SysMenuEntity> findMenuByRoleIds(List<Long> roleIds);

    /**
     * 级联删除菜单列表
     * @param ids 菜单ID
     * @return 成功、失败
     */
    void removeMenuByIds(Collection<Long> ids);

    /**
     * 更新菜单信息
     * @param sysMenu 菜单信息
     * @return 成功、失败
     */
    Boolean updateMenuById(SysMenuEntity sysMenu);

    /**
     * 构建树
     * @param menus
     * @return
     */
    List<Tree<Long>> treeMenu(List<SysMenuEntity> menus);

    /**
     * 构建树
     * @param menus
     * @param parentId
     * @return
     */
    List<Tree<Long>> treeMenu(List<SysMenuEntity> menus, Long parentId);

    /**
     * 查询菜单
     * @param voSet
     * @param parentId
     * @return
     */
    List<Tree<Long>> filterMenu(Collection<SysMenuEntity> voSet, String type, Long parentId);

    /**
     * 更新菜单
     *
     * @param menuId
     * @param param
     * @return
     */
    Long updateMenu(Long menuId, AddMenuParam param);

    /**
     * 获取租户菜单, 通过原始菜单ID
     * @param originMenuIds
     * @return
     */
    List<SysMenuEntity> getTenantMenuByOriginMenuIds(Collection<Long> originMenuIds);

    /**
     * 根据应用编码获取菜单
     * @param appCode
     * @return
     */
    List<SysMenuEntity> getMenuByAppCode(String appCode);

    /**
     * 根据租户ID删除菜单
     * @param tenantId
     * @return
     */
    Boolean deleteMenuByTenantId(Long tenantId);
}

