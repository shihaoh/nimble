package cn.shihh.nimble.system.service;

import cn.shihh.nimble.common.body.LoginUser;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.pojo.param.AddUserParam;
import cn.shihh.nimble.system.pojo.param.UpdateUserParam;
import cn.shihh.nimble.system.pojo.vo.SysUserNumVO;
import cn.shihh.nimble.system.pojo.vo.SysUserVO;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserFromCardVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.shihh.nimble.system.pojo.entity.SysUserEntity;

import java.util.Collection;
import java.util.List;

/**
 * 用户表(SysUser)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysUserService extends IService<SysUserEntity> {

    /**
     * 分页查询用户信息
     *
     * @param objectPage
     * @param key
     * @param deptId
     * @param roleId
     * @param status
     * @return
     */
    PageVO<SysUserVO> pageSearch(Page<SysUserEntity> objectPage, String key, Long deptId, Long roleId, Integer status);

    /**
     * 查询用户列表
     *
     * @param status
     * @param key
     * @return
     */
    List<SysUserVO> listUsers(Integer status, String key);

    /**
     * 根据用户id查询用户
     * @param userId
     * @return
     */
    SysUserVO getUserById(Long userId);

    /**
     * 判断手机号是否存在
     * @param phone
     * @return
     */
    Boolean existsByPhone(String phone, Long excludeUserId);

    /**
     * 新增用户
     * @param param
     * @return
     */
    Boolean saveUser(AddUserParam param);

    /**
     * 修改用户
     * @param param
     * @return
     */
    Boolean updateUser(UpdateUserParam param);

    /**
     * 删除用户
     * @param userId
     * @return
     */
    Boolean removeUser(Long userId);

    /**
     * 根据手机号获取登录用户信息
     *
     * @param phone
     * @return
     */
    LoginUser getLoginUserByPhone(String phone);

    /**
     * 根据用户id获取登录用户信息
     *
     * @param userId
     * @return
     */
    LoginUser getLoginUserById(Long userId);

    /**
     * 根据手机号获取用户信息
     *
     * @param phone
     * @return
     */
    SysUserEntity getUserByPhone(String phone);

    /**
     * 根据角色id查询用户列表
     * @param roleIds
     * @return
     */
    List<SysUserBaseVO> listByRoleIds(Collection<Long> roleIds);

    /**
     * 根据权限查询用户列表
     * @return
     */
    List<SysUserBaseVO> listByPermissions(Long deptId);

    /**
     * 根据权限查询名片用户列表
     * @return
     */
    List<SysUserFromCardVO> listFromCardByPermissions();

    /**
     * 模糊查询用户列表
     * @param keywords
     * @return
     */
    List<SysUserBaseVO> listByKeywords(String keywords);

    /**
     * 根据用户id查询名片用户信息
     *
     * @param userIds
     * @return
     */
    List<SysUserFromCardVO> getUserFromCardByUserIds(List<Long> userIds);

    /**
     * 根据用户id查询部门信息
     * @param userId
     * @return
     */
    SysDeptVO getUserDeptInfoByUserId(Long userId);

    /**
     * 根据状态查询用户数量
     * @return
     */
    SysUserNumVO countByStatus();
}

