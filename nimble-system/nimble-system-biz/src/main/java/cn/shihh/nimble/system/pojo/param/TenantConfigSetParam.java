package cn.shihh.nimble.system.pojo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/5/30
 */
@Data
@Schema(description = "租户配置设置参数")
public class TenantConfigSetParam {

    @Schema(description = "配置项")
    private String key;

    @Schema(description = "配置类")
    private String confType;

    @Schema(description = "配置值")
    private String confVal;

    @Schema(description = "说明")
    private String confDesc;

}
