package cn.shihh.nimble.system.enums;

public enum SysOperateTypeEnum {
    ROLE(1, "角色"),
    USER(2, "成员"),
    ;
    public final int code;
    public final String name;

    SysOperateTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
