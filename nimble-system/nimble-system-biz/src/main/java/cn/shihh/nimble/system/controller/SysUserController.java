package cn.shihh.nimble.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.lang.tree.Tree;
import cn.shihh.nimble.common.body.RoleBody;
import cn.shihh.nimble.common.mybatis.page.PageUtil;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.common.satoken.utils.LoginHelper;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserFromCardVO;
import cn.shihh.nimble.system.pojo.entity.SysMenuEntity;
import cn.shihh.nimble.system.pojo.entity.SysUserEntity;
import cn.shihh.nimble.system.pojo.param.AddUserParam;
import cn.shihh.nimble.system.pojo.param.UpdateUserParam;
import cn.shihh.nimble.system.pojo.vo.SysUserNumVO;
import cn.shihh.nimble.system.pojo.vo.SysUserVO;
import cn.shihh.nimble.system.service.SysMenuService;
import cn.shihh.nimble.system.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author shihh
 * @since 2023/3/22
 */
@Tag(name = "用户管理", description = "用户管理")
@RestController
@RequestMapping("/sys/user")
@RequiredArgsConstructor
public class SysUserController {

    private final SysUserService sysUserService;
    private final SysMenuService sysMenuService;

    @Operation(summary = "分页查询用户信息")
    @GetMapping("/page")
    public R<PageVO<SysUserVO>> page(@RequestParam(value = "current", defaultValue = "1") Integer current,
                                     @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                     @RequestParam(value = "key", required = false) String key,
                                     @RequestParam(value = "deptId", required = false) Long deptId,
                                     @RequestParam(value = "roleId", required = false) Long roleId,
                                     @RequestParam(value = "status", required = false) Integer status) {
        PageVO<SysUserVO> res = sysUserService.pageSearch(PageUtil.paramToPage(current, pageSize, SysUserEntity.class),
                key, deptId, roleId, status);
        return R.ok(res);
    }

    @Operation(summary = "查询用户列表")
    @GetMapping("/list")
    public R<List<SysUserVO>> list(@RequestParam(value = "status", required = false) Integer status,
                                   @RequestParam(value = "key", required = false) String key) {
        List<SysUserVO> res = sysUserService.listUsers(status, key);
        return R.ok(res);
    }

    @Operation(summary = "查询用户信息")
    @GetMapping("/{userId}")
    public R<SysUserVO> get(@PathVariable("userId") Long userId) {
        return R.ok(sysUserService.getUserById(userId));
    }

    @Operation(summary = "新增用户信息")
    @PostMapping
    @SaCheckPermission("sys_user_add")
    public R<Boolean> add(@Validated @RequestBody AddUserParam param) {
        return R.ok(sysUserService.saveUser(param));
    }

    @Operation(summary = "修改用户信息")
    @PutMapping
    @SaCheckPermission("sys_user_edit")
    public R<Boolean> update(@RequestBody UpdateUserParam param) {
        return R.ok(sysUserService.updateUser(param));
    }

    @Operation(summary = "删除用户信息")
    @DeleteMapping("/{userId}")
    @SaCheckPermission("sys_user_del")
    public R<Boolean> delete(@PathVariable("userId") Long userId) {
        return R.ok(sysUserService.removeUser(userId));
    }

    @Operation(summary = "获取当前用户信息")
    @GetMapping("/info")
    public R<SysUserVO> info() {
        return R.ok(sysUserService.getUserById(LoginHelper.getUserId()));
    }

    @Operation(summary = "获取当前用户菜单列表")
    @GetMapping("/menu")
    public R<List<Tree<Long>>> menu(String type, Long parentId) {
        // 获取符合条件的菜单
        List<Long> roleIds =
                LoginHelper.getLoginUser().getRoles().stream().map(RoleBody::getRoleId).collect(Collectors.toList());
        List<SysMenuEntity> all = sysMenuService.findMenuByRoleIds(roleIds);
        return R.ok(sysMenuService.filterMenu(all, type, parentId));
    }

    @Operation(summary = "根据当前用户权限查询用户列表")
    @GetMapping("/permission")
    public R<List<SysUserBaseVO>> permission(@RequestParam(value = "deptId", required = false) Long deptId) {
        List<SysUserBaseVO> res = sysUserService.listByPermissions(deptId);
        return R.ok(res);
    }

    @Operation(summary = "根据当前用户权限查询用户列表-名片")
    @GetMapping("/userPost/permission")
    public R<List<SysUserFromCardVO>> userPost() {
        List<SysUserFromCardVO> res = sysUserService.listFromCardByPermissions();
        return R.ok(res);
    }

    @Operation(summary = "根据用户手机号获取用户id")
    @GetMapping("/userId")
    public R<Long> getUserIdByPhone(@RequestParam(value = "phone") String phone) {
        SysUserEntity user = sysUserService.getUserByPhone(phone);
        if (user.getStatus() != 0) {
            return R.ok();
        }
        return R.ok(Optional.ofNullable(user).map(SysUserEntity::getUserId).orElse(null));
    }

    @Operation(summary = "用户状态统计")
    @GetMapping("/status/count")
    public R<SysUserNumVO> countByStatus() {
        SysUserNumVO res = sysUserService.countByStatus();
        return R.ok(res);
    }

}
