package cn.shihh.nimble.system.pojo.param;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import java.util.List;

/**
 * 用户表(SysUser)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:18
 */
@Data
@Schema(description = "用户信息")
public class AddUserParam {

    @Schema(description = "状态 0在职 1离职 2锁定")
    private Integer status;
    
    @Schema(description = "用户名")
    private String username;

    @Schema(description = "用户编码，如果未传入则自动生成")
    private String userCode;

    @Schema(description = "手机号")
    @Pattern(regexp = "^1[3-9][0-9]{9}$", message = "手机号格式有误")
    private String phone;

    @Schema(description = "密码")
    private String password;
    
    @Schema(description = "头像")
    private String avatar;
    
    @Schema(description = "拓展字段:昵称")
    private String nickname;
    
    @Schema(description = "拓展字段:姓名")
    private String name;
    
    @Schema(description = "拓展字段:邮箱")
    private String email;
    
    @Schema(description = "部门ID")
    private Long deptId;

    @Schema(description = "岗位列表")
    private List<Long> post;

    @Schema(description = "角色列表")
    private List<Long> role;
}

