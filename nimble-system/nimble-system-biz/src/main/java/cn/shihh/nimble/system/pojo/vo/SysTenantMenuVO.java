package cn.shihh.nimble.system.pojo.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * (SysTenantMenu)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:18
 */
@Data
@Schema(description = "租户菜单")
public class SysTenantMenuVO {
    
    @Schema(description = "租户菜单id")
    private Long id;
    
    @Schema(description = "租户菜单名称")
    private String name;
    
    @Schema(description = "绑定菜单列表 ',' 分割")
    private String menuIds;
    
    @Schema(description = "租户菜单,9:冻结,0:正常")
    private String status;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}

