package cn.shihh.nimble.system.controller.inner;

import cn.shihh.nimble.system.api.enums.TenantConfigEnum;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.client.RemoteConfigClient;
import cn.shihh.nimble.system.api.pojo.vo.ConfigVO;
import cn.shihh.nimble.system.service.SysTenantConfigService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shihh
 * @since 2023/5/30
 */
@RestController
@RequiredArgsConstructor
public class InnerTenantConfigController implements RemoteConfigClient {

    private final SysTenantConfigService tenantConfigService;

    @Override
    public R<ConfigVO> tenantConfig(TenantConfigEnum conf) {
        return R.ok(tenantConfigService.configGet(conf));
    }

    @Override
    public R<List<ConfigVO>> tenantConfig(List<TenantConfigEnum> configs) {
        return R.ok(tenantConfigService.getConfListByEnums(configs));
    }

    @Override
    public R<Boolean> updateTenantConfig(TenantConfigEnum conf, String value) {
        return R.ok(tenantConfigService.configSetting(conf, value));
    }
}
