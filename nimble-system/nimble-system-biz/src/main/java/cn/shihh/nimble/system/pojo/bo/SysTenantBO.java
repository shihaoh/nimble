package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 租户表(SysTenant)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:17
 */
@Data
@Schema(description = "租户表")
public class SysTenantBO {
    
    @Schema(description = "租户id")
    private Long id;
    
    @Schema(description = "${column.comment}")
    private String name;
    
    @Schema(description = "${column.comment}")
    private String code;
    
    @Schema(description = "${column.comment}")
    private String tenantDomain;
    
    @Schema(description = "开始时间")
    private LocalDateTime startTime;
    
    @Schema(description = "结束时间")
    private LocalDateTime endTime;
    
    @Schema(description = "${column.comment}")
    private String status;
    
    @Schema(description = "${column.comment}")
    private String delFlag;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "修改人")
    private String updateBy;
    
    @Schema(description = "创建")
    private LocalDateTime createTime;
    
    @Schema(description = "更新时间")
    private LocalDateTime updateTime;
    
    @Schema(description = "${column.comment}")
    private Long menuId;

}

