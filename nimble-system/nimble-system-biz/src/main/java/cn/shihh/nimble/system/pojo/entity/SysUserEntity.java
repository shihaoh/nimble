package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.TenantBaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 用户表(SysUser)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_user", autoResultMap = true)
public class SysUserEntity extends TenantBaseEntity {
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long userId;

    /**
     * 状态 0在职 1离职 2锁定
     */
    @TableField("status")
    private Integer status;

    /**
     * 
     */
    @TableField("username")
    private String username;

    /**
     *
     */
    @TableField("user_code")
    private String userCode;

    /**
     * 
     */
    @TableField("password")
    private String password;

    /**
     * 
     */
    @TableField("phone")
    private String phone;

    /**
     * 
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 形象照
     */
    @TableField("portrait")
    private String portrait;

    /**
     * 微信二维码
     */
    @TableField("wechat_qrcode")
    private String wechatQrcode;

    /**
     * 拓展字段:昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 拓展字段:姓名
     */
    @TableField("name")
    private String name;

    /**
     * 拓展字段:邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 部门ID
     */
    @TableField("dept_id")
    private Long deptId;

    /**
     *
     */
    @TableField("lock_flag")
    private String lockFlag;

}

