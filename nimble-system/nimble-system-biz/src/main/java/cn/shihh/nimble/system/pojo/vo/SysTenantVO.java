package cn.shihh.nimble.system.pojo.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 租户表(SysTenant)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:17
 */
@Data
@Schema(description = "租户信息")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysTenantVO {
    
    @Schema(description = "租户id")
    private Long id;
    
    @Schema(description = "租户名称")
    private String name;
    
    @Schema(description = "租户编码")
    private String code;
    
    @Schema(description = "租户域名")
    private String tenantDomain;
    
    @Schema(description = "开始时间")
    private LocalDateTime startTime;
    
    @Schema(description = "结束时间")
    private LocalDateTime endTime;
    
    @Schema(description = "租户状态")
    private String status;

    @Schema(description = "菜单id")
    private Long menuId;

}

