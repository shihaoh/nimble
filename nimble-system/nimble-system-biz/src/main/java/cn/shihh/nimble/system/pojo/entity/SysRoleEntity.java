package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.TenantBaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 系统角色表(SysRole)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_role", autoResultMap = true)
public class SysRoleEntity extends TenantBaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long roleId;

    /**
     * 启用
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 
     */
    @TableField("role_name")
    private String roleName;

    /**
     * 
     */
    @TableField("role_code")
    private String roleCode;

    /**
     * 
     */
    @TableField("role_desc")
    private String roleDesc;

    /**
     * 
     */
    @TableField("ds_type")
    private Integer dsType;

    /**
     * 
     */
    @TableField("ds_scope")
    private String dsScope;

}

