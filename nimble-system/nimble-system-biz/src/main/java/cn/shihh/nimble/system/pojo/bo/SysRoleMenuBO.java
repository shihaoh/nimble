package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 角色菜单表(SysRoleMenu)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:17
 */
@Data
@Schema(description = "角色菜单表")
public class SysRoleMenuBO {
    
    @Schema(description = "角色ID")
    private Long roleId;
    
    @Schema(description = "菜单ID")
    private Long menuId;

}

