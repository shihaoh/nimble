package cn.shihh.nimble.system.pojo.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 应用表(SysApplication)表实体类
 *
 * @author makejava
 * @since 2023-05-15 15:45:43
 */
@Data
@Schema(description = "应用表")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysApplicationVO {
    
    @Schema(description = "应用ID")
    private Long appId;
    
    @Schema(description = "应用名称")
    private String name;
    
    @Schema(description = "应用代码")
    private String code;
    
    @Schema(description = "应用入口")
    private String entry;
    
    @Schema(description = "应用图标")
    private String icon;
    
    @Schema(description = "应用首页")
    private String homepage;
    
    @Schema(description = "备注")
    private String remark;
    
    @Schema(description = "状态")
    private Boolean status;
    
    @Schema(description = "排序值")
    private Integer sortOrder;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
    
    @Schema(description = "租户ID")
    private Integer tenantId;

}

