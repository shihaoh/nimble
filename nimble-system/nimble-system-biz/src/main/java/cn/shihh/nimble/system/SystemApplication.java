package cn.shihh.nimble.system;

import cn.shihh.nimble.common.feign.annotation.EnableCustomerFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shihh
 * @since 2023/3/15
 */
@SpringBootApplication
@EnableCustomerFeign
public class SystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class, args);
    }

}
