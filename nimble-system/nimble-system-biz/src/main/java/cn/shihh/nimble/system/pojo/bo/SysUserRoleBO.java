package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户角色表(SysUserRole)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:19
 */
@Data
@Schema(description = "用户角色表")
public class SysUserRoleBO {
    
    @Schema(description = "用户ID")
    private Long userId;
    
    @Schema(description = "角色ID")
    private Long roleId;

}

