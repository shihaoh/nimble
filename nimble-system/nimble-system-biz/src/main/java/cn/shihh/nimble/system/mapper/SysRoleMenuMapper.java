package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysRoleMenuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色菜单表(SysRoleMenu)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenuEntity> {
    List<SysRoleMenuEntity> getByAppIdAndRoleId(@Param("appCode") String appCode,
                                                @Param("roleId") Long roleId);
}

