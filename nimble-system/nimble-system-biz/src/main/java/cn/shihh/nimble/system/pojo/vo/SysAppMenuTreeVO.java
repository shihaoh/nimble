package cn.shihh.nimble.system.pojo.vo;


import cn.hutool.core.lang.tree.Tree;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * 应用表(SysApplication)表实体类
 *
 * @author makejava
 * @since 2023-05-15 15:45:43
 */
@Data
@Schema(description = "应用菜单树")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysAppMenuTreeVO {
    
    @Schema(description = "应用ID")
    private Long appId;

    @Schema(description = "应用编码")
    private String code;
    
    @Schema(description = "应用名称")
    private String name;

    @Schema(description = "应用菜单树")
    private List<Tree<Long>> menuTree;

}

