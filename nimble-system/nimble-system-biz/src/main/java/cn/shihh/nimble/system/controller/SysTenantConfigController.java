package cn.shihh.nimble.system.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.shihh.nimble.common.mybatis.tenant.TenantBroker;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.pojo.vo.ConfigVO;
import cn.shihh.nimble.system.pojo.param.TenantConfigSetParam;
import cn.shihh.nimble.system.service.SysTenantConfigService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author shihh
 * @since 2023/5/30
 */
@Tag(name = "租户配置管理")
@RestController
@RequestMapping("/sys/tenant/config")
@RequiredArgsConstructor
public class SysTenantConfigController {

    private final SysTenantConfigService tenantConfigService;

    @Operation(summary = "获取租户配置")
    @GetMapping("/{key}")
    @SaIgnore
    public R<ConfigVO> getTenantConfig(@PathVariable("key") String key) {
        return R.ok(tenantConfigService.configGet(key));
    }

    @Operation(summary = "修改租户配置")
    @PutMapping
    public R<Boolean> updateTenantConfig(@RequestBody TenantConfigSetParam param) {
        return R.ok(tenantConfigService.configSet(param));
    }

    @Operation(summary = "根据配置分类获取租户配置")
    @GetMapping("/type/{type}")
    @SaIgnore
    public R<Map<String, String>> getTenantConfigByType(@PathVariable("type") String type) {
        return R.ok(tenantConfigService.configGetByType(type));
    }

    @Operation(summary = "获取租户配置-其他租户")
    @GetMapping("/{tenantId}/{key}")
    public R<ConfigVO> getOtherTenantConfig(@PathVariable("tenantId") Long tenantId,
                                            @PathVariable("key") String key) {
        return R.ok(TenantBroker.applyAs(tenantId, id -> tenantConfigService.configGet(key)));
    }

    @Operation(summary = "修改租户配置-其他租户")
    @PutMapping("{tenantId}")
    public R<Boolean> updateOtherTenantConfig(@PathVariable("tenantId") Long tenantId,
                                              @RequestBody TenantConfigSetParam param) {
        return R.ok(TenantBroker.applyAs(tenantId, id -> tenantConfigService.configSet(param)));
    }

    @Operation(summary = "根据配置分类获取租户配置-其他租户")
    @GetMapping("/{tenantId}/type/{type}")
    public R<Map<String, String>> getOtherTenantConfigByType(@PathVariable("tenantId") Long tenantId,
                                                             @PathVariable("type") String type) {
        return R.ok(TenantBroker.applyAs(tenantId, id -> tenantConfigService.configGetByType(type)));
    }

}
