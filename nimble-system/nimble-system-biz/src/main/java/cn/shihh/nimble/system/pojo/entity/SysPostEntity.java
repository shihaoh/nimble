package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.TenantBaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 岗位信息表(SysPost)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 16:26:24
 */
@Data
@TableName(value = "sys_post", autoResultMap = true)
public class SysPostEntity extends TenantBaseEntity {
    /**
     * 岗位ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long postId;

    /**
     * 岗位类型 0律师1行政2财务3其他
     */
    private Integer postType;

    /**
     * 岗位编码
     */
    @TableField("post_code")
    private String postCode;

    /**
     * 岗位名称
     */
    @TableField("post_name")
    private String postName;

    /**
     * 岗位排序
     */
    @TableField("post_sort")
    private Integer postSort;

    /**
     * 岗位描述
     */
    @TableField("remark")
    private String remark;

}

