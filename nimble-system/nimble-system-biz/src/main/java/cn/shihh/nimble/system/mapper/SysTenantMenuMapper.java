package cn.shihh.nimble.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shihh.nimble.system.pojo.entity.SysTenantMenuEntity;

/**
 * (SysTenantMenu)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysTenantMenuMapper extends BaseMapper<SysTenantMenuEntity> {
}

