package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 部门管理(SysDept)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:12
 */
@Data
@Schema(description = "部门管理")
public class SysDeptBO {
    
    @Schema(description = "${column.comment}")
    private Long deptId;
    
    @Schema(description = "${column.comment}")
    private String name;
    
    @Schema(description = "排序")
    private Integer sortOrder;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "修改人")
    private String updateBy;
    
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
    
    @Schema(description = "修改时间")
    private LocalDateTime updateTime;
    
    @Schema(description = "${column.comment}")
    private String delFlag;
    
    @Schema(description = "${column.comment}")
    private Long parentId;
    
    @Schema(description = "${column.comment}")
    private Long tenantId;

}

