package cn.shihh.nimble.system.pojo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.List;

/**
 * @author shihh
 * @since 2023/3/30
 */
@Data
@Schema(description = "更新角色参数")
public class UpdateRoleAndMenuParam {

    @Schema(description = "操作主键")
    private Long id;

    @Schema(description = "角色名称")
    @NotBlank(message = "角色名称不能为空")
    private String roleName;

    @Schema(description = "应用编码")
    private String appCode;

    @Schema(description = "角色编码")
    private String roleCode;

    @Schema(description = "角色描述")
    private String roleDesc;

    @Schema(description = "菜单编号列表")
    private List<Long> menuIds;

    @Schema(description = "是否启用")
    private String enabled;

}
