package cn.shihh.nimble.system.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.shihh.nimble.common.mybatis.page.PageUtil;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.pojo.vo.SysPostVO;
import cn.shihh.nimble.system.pojo.entity.SysPostEntity;
import cn.shihh.nimble.system.pojo.param.AddPostParam;
import cn.shihh.nimble.system.service.SysPostService;
import cn.shihh.nimble.common.mybatis.page.PageParamVO;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author shihh
 * @since 2023/3/20
 */
@Tag(name = "岗位管理模块")
@RestController
@RequestMapping("/sys/post")
@RequiredArgsConstructor
public class SysPostController {

    private final SysPostService sysPostService;

    @Operation(summary = "获取岗位列表")
    @GetMapping
    public R<List<SysPostVO>> listPosts() {
        List<SysPostEntity> list = sysPostService.list();
        return R.ok(BeanUtil.copyToList(list, SysPostVO.class));
    }

    @Operation(summary = "获取岗位分页列表")
    @GetMapping("page")
    public R<PageVO<SysPostVO>> page(PageParamVO<SysPostEntity> param) {
        IPage<SysPostEntity> page = param.toPage();
        IPage<SysPostEntity> res = sysPostService.page(page);
        return R.ok(PageUtil.pageToVO(res, SysPostVO.class));
    }

    @Operation(summary = "查询岗位信息")
    @GetMapping("{postId}")
    public R<SysPostVO> get(@PathVariable Long postId) {
        SysPostEntity entity = sysPostService.getById(postId);
        return R.ok(BeanUtil.copyProperties(entity, SysPostVO.class));
    }

    @Operation(summary = "新增岗位")
    @PostMapping
    public R<Boolean> save(@RequestBody AddPostParam param) {
        return R.ok(sysPostService.savePost(param));
    }

    @Operation(summary = "修改岗位")
    @PutMapping("{postId}")
    public R<Boolean> update(@PathVariable Long postId, @RequestBody AddPostParam param) {
        return R.ok(sysPostService.updatePost(postId, param));
    }

    @Operation(summary = "删除岗位")
    @DeleteMapping("{postId}")
    public R<Boolean> delete(@PathVariable Long postId) {
        return R.ok(sysPostService.deletePost(postId));
    }

}
