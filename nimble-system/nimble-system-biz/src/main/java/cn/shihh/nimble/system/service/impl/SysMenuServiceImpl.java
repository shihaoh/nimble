package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.shihh.nimble.common.constant.CacheConstants;
import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.enums.MenuTypeEnum;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.common.mybatis.tenant.TenantBroker;
import cn.shihh.nimble.system.mapper.SysMenuMapper;
import cn.shihh.nimble.system.mapper.SysRoleMenuMapper;
import cn.shihh.nimble.system.pojo.entity.SysMenuEntity;
import cn.shihh.nimble.system.pojo.entity.SysRoleMenuEntity;
import cn.shihh.nimble.system.pojo.param.AddMenuParam;
import cn.shihh.nimble.system.service.SysMenuService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 菜单权限表(SysMenu)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenuEntity> implements SysMenuService {

    private final SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    @Cacheable(value = CacheConstants.MENU_DETAILS, key = "#roleId", unless = "#result.isEmpty()")
    public List<Long> findMenuIdByRoleId(Long roleId) {
        List<SysMenuEntity> list = baseMapper.listMenusByRoleId(roleId);
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        return list.stream().map(SysMenuEntity::getMenuId).collect(Collectors.toList());
    }

    @Override
    public List<Long> findMenuIdByRoleIdAndAppCode(Long roleId, String appCode) {
        List<SysMenuEntity> list = baseMapper.getMenuByRoleIdAndAppCode(roleId, appCode);
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        return list.stream().map(SysMenuEntity::getMenuId).collect(Collectors.toList());
    }

    @Override
    public List<SysMenuEntity> findMenuByRoleIds(List<Long> roleIds) {
        return baseMapper.listMenusByRoleIds(roleIds);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = CacheConstants.MENU_DETAILS, allEntries = true)
    public void removeMenuByIds(Collection<Long> ids) {
        LambdaQueryWrapper<SysMenuEntity> qr = new LambdaQueryWrapper<>();
        qr.in(SysMenuEntity::getParentId, ids);
        qr.notIn(SysMenuEntity::getMenuId, ids);
        List<SysMenuEntity> list = list(qr);
        if (CollUtil.isNotEmpty(list)) {
            throw new ServiceException("菜单存在下级节点 删除失败");
        }
        baseMapper.delete(Wrappers.<SysMenuEntity>query().lambda().in(SysMenuEntity::getMenuId, ids));
        sysRoleMenuMapper.delete(Wrappers.<SysRoleMenuEntity>query().lambda().in(SysRoleMenuEntity::getMenuId, ids));
    }

    @Override
    @CacheEvict(value = CacheConstants.MENU_DETAILS, allEntries = true)
    public Boolean updateMenuById(SysMenuEntity sysMenu) {
        return this.updateById(sysMenu);
    }

    @Override
    public List<Tree<Long>> treeMenu(List<SysMenuEntity> menus) {
        return treeMenu(menus, null);
    }

    @Override
    public List<Tree<Long>> treeMenu(List<SysMenuEntity> menus, Long parentId) {
        Long parent = parentId == null ? CommonConstants.MENU_TREE_ROOT_ID : parentId;
        List<TreeNode<Long>> collect = menus.stream()
                .map(getNodeFunction()).collect(Collectors.toList());
        return TreeUtil.build(collect, parent);
    }

    /**
     * 查询菜单
     * @param all 全部菜单
     * @param type 类型
     * @param parentId 父节点ID
     * @return
     */
    @Override
    public List<Tree<Long>> filterMenu(Collection<SysMenuEntity> all, String type, Long parentId) {
        List<TreeNode<Long>> collect = all.stream().filter(menuTypePredicate(type)).map(getNodeFunction())
                .collect(Collectors.toList());
        Long parent = ObjectUtil.defaultIfNull(parentId, CommonConstants.MENU_TREE_ROOT_ID);
        return TreeUtil.build(collect, parent);
    }

    @NotNull
    private Function<SysMenuEntity, TreeNode<Long>> getNodeFunction() {
        return menu -> {
            TreeNode<Long> node = new TreeNode<>();
            node.setId(menu.getMenuId());
            node.setName(menu.getName());
            node.setParentId(menu.getParentId());
            node.setWeight(menu.getSortOrder());
            // 扩展属性
            Map<String, Object> extra = new HashMap<>();
            extra.put("icon", menu.getIcon());
            extra.put("path", menu.getPath());
            extra.put("menuType", menu.getMenuType());
            extra.put("permission", menu.getPermission());
            extra.put("label", menu.getName());
            extra.put("sortOrder", menu.getSortOrder());
            extra.put("keepAlive", menu.getKeepAlive());
            extra.put("visible", menu.getVisible());
            extra.put("originMenuId", menu.getOriginMenuId());
            node.setExtra(extra);
            return node;
        };
    }

    /**
     * menu 类型断言
     * @param type 类型
     * @return Predicate
     */
    private Predicate<SysMenuEntity> menuTypePredicate(String type) {
        return vo -> {
            if (MenuTypeEnum.TOP_MENU.getDescription().equals(type)) {
                return MenuTypeEnum.TOP_MENU.getType().equals(vo.getMenuType());
            }
            // 其他查询 左侧 + 顶部
            return !MenuTypeEnum.BUTTON.getType().equals(vo.getMenuType());
        };
    }

    @Override
    public Long updateMenu(Long menuId, AddMenuParam param) {
        SysMenuEntity entity = getById(menuId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("菜单不存在");
        }
        Long appId = entity.getAppId();
        BeanUtil.copyProperties(param, entity);
        updateMenuById(entity);
        return appId;
    }

    @Override
    public List<SysMenuEntity> getTenantMenuByOriginMenuIds(Collection<Long> originMenuIds) {
        if (CollUtil.isEmpty(originMenuIds)) {
            return Collections.emptyList();
        }
        LambdaQueryWrapper<SysMenuEntity> qr = new LambdaQueryWrapper<>();
        qr.in(SysMenuEntity::getOriginMenuId, originMenuIds);
        return list(qr);
    }

    @Override
    public List<SysMenuEntity> getMenuByAppCode(String appCode) {
        return baseMapper.getMenuByAppCode(appCode);
    }

    @Override
    public Boolean deleteMenuByTenantId(Long tenantId) {
        return TenantBroker.applyAs(tenantId, id -> {
            LambdaQueryWrapper<SysMenuEntity> qr = new LambdaQueryWrapper<>();
            qr.eq(SysMenuEntity::getTenantId, id);
            return remove(qr);
        });
    }
}

