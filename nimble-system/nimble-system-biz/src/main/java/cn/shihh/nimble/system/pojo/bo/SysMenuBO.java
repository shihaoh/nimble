package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 菜单权限表(SysMenu)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:15
 */
@Data
@Schema(description = "菜单权限表")
public class SysMenuBO {
    
    @Schema(description = "菜单ID")
    private Long menuId;
    
    @Schema(description = "${column.comment}")
    private String name;
    
    @Schema(description = "${column.comment}")
    private String permission;
    
    @Schema(description = "${column.comment}")
    private String path;
    
    @Schema(description = "父菜单ID")
    private Long parentId;
    
    @Schema(description = "${column.comment}")
    private String icon;
    
    @Schema(description = "是否显示")
    private String visible;
    
    @Schema(description = "排序值")
    private Integer sortOrder;
    
    @Schema(description = "${column.comment}")
    private String keepAlive;
    
    @Schema(description = "${column.comment}")
    private String menuType;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
    
    @Schema(description = "修改人")
    private String updateBy;
    
    @Schema(description = "更新时间")
    private LocalDateTime updateTime;
    
    @Schema(description = "${column.comment}")
    private String delFlag;
    
    @Schema(description = "租户ID")
    private Integer tenantId;

}

