package cn.shihh.nimble.system.controller.inner;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.client.RemoteRoleClient;
import cn.shihh.nimble.system.api.pojo.vo.SysRoleListVO;
import cn.shihh.nimble.system.pojo.entity.SysRoleEntity;
import cn.shihh.nimble.system.service.SysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

/**
 * @author shihh
 * @since 2023/6/12
 */
@RestController
@RequiredArgsConstructor
public class InnerRoleController implements RemoteRoleClient {

    private final SysRoleService sysRoleService;

    @Override
    public R<List<SysRoleListVO>> listRoleByIds(List<Long> roleIds) {
        if (CollUtil.isEmpty(roleIds)) {
            return R.ok(Collections.emptyList());
        }
        List<SysRoleEntity> roles = sysRoleService.listByIds(roleIds);
        if (CollUtil.isNotEmpty(roles)) {
            return R.ok(BeanUtil.copyToList(roles, SysRoleListVO.class));
        }
        return R.ok(Collections.emptyList());
    }
}
