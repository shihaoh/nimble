package cn.shihh.nimble.system.pojo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/5/15
 */
@Data
@Schema(description = "添加应用参数")
public class AddAppParam {

    @Schema(description = "应用名称")
    private String name;

    @Schema(description = "应用代码")
    private String code;

    @Schema(description = "应用入口")
    private String entry;

    @Schema(description = "应用图标")
    private String icon;

    @Schema(description = "应用首页")
    private String homepage;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "状态")
    private Boolean status;

    @Schema(description = "排序值")
    private Integer sortOrder;

    @Schema(description = "租户ID 默认为1")
    private Integer tenantId = 1;

}
