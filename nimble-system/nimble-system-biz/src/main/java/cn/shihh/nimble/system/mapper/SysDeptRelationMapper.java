package cn.shihh.nimble.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shihh.nimble.system.pojo.entity.SysDeptRelationEntity;

/**
 * 部门关系表(SysDeptRelation)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysDeptRelationMapper extends BaseMapper<SysDeptRelationEntity> {

    /**
     * 删除部门 > 删除所有关联此部门子节点的闭包关系
     * @param id 部门ID
     */
    void deleteDeptRelationsByDeptId(Long id);

    /**
     * 删除节点数据
     * @param deptRelation 关系节点
     */
    void deleteDeptRelations(SysDeptRelationEntity deptRelation);

    /**
     * 新增节点数据
     * @param deptRelation 关系节点
     */
    void insertDeptRelations(SysDeptRelationEntity deptRelation);
}

