package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户与岗位关联表(SysUserPost)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:19
 */
@Data
@Schema(description = "用户与岗位关联表")
public class SysUserPostBO {
    
    @Schema(description = "用户ID")
    private Long userId;
    
    @Schema(description = "岗位ID")
    private Long postId;

}

