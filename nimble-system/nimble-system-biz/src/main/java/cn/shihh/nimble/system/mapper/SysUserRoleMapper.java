package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.pojo.bo.SysRoleUserCountBO;
import cn.shihh.nimble.system.pojo.entity.SysUserRoleEntity;
import cn.shihh.nimble.common.body.RoleBody;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户角色表(SysUserRole)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity> {
    List<RoleBody> listRoleByUserId(@Param("userId") Long userId);

    List<SysRoleUserCountBO> countsByRoleIds(@Param("roleIds") List<Long> roleIds);

    List<SysUserBaseVO> getUsersByRoleId(Long roleId);
}

