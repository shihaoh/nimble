package cn.shihh.nimble.system.controller.inner;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.shihh.nimble.system.api.client.RemoteDataScopeClient;
import cn.shihh.nimble.system.service.SysDeptRelationService;
import cn.shihh.nimble.system.pojo.entity.SysDeptRelationEntity;
import cn.shihh.nimble.system.pojo.entity.SysRoleEntity;
import cn.shihh.nimble.system.service.SysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shihh
 * @since 2023/5/22
 */
@RestController
@RequiredArgsConstructor
public class InnerDataScopeController implements RemoteDataScopeClient {

    private final SysDeptRelationService relationService;
    private final SysRoleService roleService;

    @Override
    public String getRoleCustom(Long roleId) {
        SysRoleEntity role = roleService.getById(roleId);
        if (role == null) {
            return "-1";
        }
        return role.getDsScope();
    }

    @Override
    public String getDeptAndChild(Long deptId) {
        List<SysDeptRelationEntity> list = relationService.lambdaQuery()
                .eq(SysDeptRelationEntity::getAncestor, deptId).list();
        if (CollUtil.isNotEmpty(list)) {
            return CollUtil.join(list, ",", d -> Convert.toStr(d.getDescendant()));
        }
        return "-1";
    }
}
