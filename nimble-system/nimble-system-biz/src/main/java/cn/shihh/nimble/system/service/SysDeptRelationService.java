package cn.shihh.nimble.system.service;

import cn.shihh.nimble.system.pojo.entity.SysDeptEntity;
import cn.shihh.nimble.system.pojo.entity.SysDeptRelationEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 部门关系表(SysDeptRelation)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysDeptRelationService extends IService<SysDeptRelationEntity> {

    /**
     * 新建部门关系
     * @param sysDept 部门
     */
    void insertDeptRelation(SysDeptEntity sysDept);

    /**
     * 通过ID删除部门关系
     * @param id
     */
    void deleteAllDeptRelation(Long id);

    /**
     * 更新部门关系
     * @param relation
     */
    void updateDeptRelation(SysDeptRelationEntity relation);

}

