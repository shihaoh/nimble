package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.TenantBaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 应用表(SysApplication)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-05-15 15:45:42
 */
@Data
@TableName(value = "sys_application", autoResultMap = true)
public class SysApplicationEntity extends TenantBaseEntity {
    /**
     * 应用ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long appId;

    /**
     * 应用名称
     */
    @TableField("name")
    private String name;

    /**
     * 应用代码
     */
    @TableField("code")
    private String code;

    /**
     * 应用入口
     */
    @TableField("entry")
    private String entry;

    /**
     * 应用图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 应用首页
     */
    @TableField("homepage")
    private String homepage;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 状态
     */
    @TableField("status")
    private Boolean status;

    /**
     * 原始应用ID
     */
    @TableField("origin_app_id")
    private Long originAppId;

    /**
     * 排序值
     */
    @TableField("sort_order")
    private Integer sortOrder;

}

