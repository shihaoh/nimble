package cn.shihh.nimble.system.pojo.param;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 菜单权限表(SysMenu)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:15
 */
@Data
@Schema(description = "添加菜单参数")
public class AddMenuParam {
    
    @Schema(description = "名称")
    private String name;
    
    @Schema(description = "权限标识")
    private String permission;
    
    @Schema(description = "路径")
    private String path;
    
    @Schema(description = "父菜单ID")
    private Long parentId;
    
    @Schema(description = "图标")
    private String icon;
    
    @Schema(description = "是否显示")
    private String visible;
    
    @Schema(description = "排序值")
    private Integer sortOrder;
    
    @Schema(description = "路由缓冲")
    private String keepAlive;
    
    @Schema(description = "菜单类型,0:菜单 1:按钮")
    private String menuType;

}

