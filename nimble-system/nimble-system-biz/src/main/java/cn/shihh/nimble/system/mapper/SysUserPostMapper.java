package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysUserPostEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户与岗位关联表(SysUserPost)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 16:26:29
 */
public interface SysUserPostMapper extends BaseMapper<SysUserPostEntity> {
}

