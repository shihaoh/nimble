package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysMenuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单权限表(SysMenu)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {


    /**
     * 通过角色编号查询菜单
     * @param roleId 角色ID
     * @return
     */
    List<SysMenuEntity> listMenusByRoleId(Long roleId);


    /**
     * 通过角色编号查询菜单
     * @param roleIds 角色ID
     * @return
     */
    List<SysMenuEntity> listMenusByRoleIds(@Param("roleIds") List<Long> roleIds);

    /**
     * 通过应用编号查询菜单
     * @param appCode
     * @return
     */
    List<SysMenuEntity> getMenuByAppCode(@Param("appCode") String appCode);

    /**
     * 通过应用编号查询菜单
     * @param appCode
     * @return
     */
    List<SysMenuEntity> getMenuByRoleIdAndAppCode(@Param("roleId") Long roleId,@Param("appCode") String appCode);
}

