package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.common.mybatis.tenant.TenantBroker;
import cn.shihh.nimble.system.api.enums.TenantConfigEnum;
import cn.shihh.nimble.system.api.pojo.vo.ConfigVO;
import cn.shihh.nimble.system.mapper.SysRoleMapper;
import cn.shihh.nimble.system.pojo.entity.SysRoleEntity;
import cn.shihh.nimble.system.pojo.entity.SysRoleMenuEntity;
import cn.shihh.nimble.system.pojo.param.AddRoleParam;
import cn.shihh.nimble.system.pojo.param.UpdateRoleAndMenuParam;
import cn.shihh.nimble.system.pojo.param.UpdateRoleParam;
import cn.shihh.nimble.system.service.SysRoleMenuService;
import cn.shihh.nimble.system.service.SysRoleService;
import cn.shihh.nimble.system.service.SysTenantConfigService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * 系统角色表(SysRole)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Service
@RequiredArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleEntity> implements SysRoleService {

    private final SysRoleMenuService sysRoleMenuService;
    private final SysTenantConfigService sysTenantConfigService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeRoleById(Long id) {
        SysRoleEntity role = getById(id);
        if (ObjectUtil.isNull(role)) {
            throw new ServiceException("角色不存在");
        }
        ConfigVO defaultRoleCode = TenantBroker.applyAs(CommonConstants.TENANT_ID_DEFAULT, tenantId -> sysTenantConfigService.configGet(TenantConfigEnum.TENANT_DEFAULT_ROLECODE));
        if (ObjectUtil.equal(defaultRoleCode.getConfVal(), role.getRoleCode())) {
            throw new ServiceException("默认超管角色不能删除");
        }
        sysRoleMenuService.remove(Wrappers.<SysRoleMenuEntity>update().lambda().eq(SysRoleMenuEntity::getRoleId, id));
        return this.removeById(id);
    }

    @Override
    public Boolean updateRoleMenus(Long roleId, List<Long> menuIds) {
        return sysRoleMenuService.saveRoleMenus(roleId, null, menuIds);
    }

    @Override
    public Boolean saveRole(AddRoleParam param) {
        if (existsRoleCode(param.getRoleCode())) {
            throw new ServiceException("角色编码已存在");
        }
        SysRoleEntity entity = BeanUtil.toBean(param, SysRoleEntity.class);
        save(entity);
        Long roleId = entity.getRoleId();
        return sysRoleMenuService.saveRoleMenus(roleId, null, param.getMenuIds());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateRole(UpdateRoleParam param) {
        SysRoleEntity entity = getById(param.getId());
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("角色不存在");
        }
        checkRoleCode(entity.getRoleCode(), param.getRoleCode());
        BeanUtil.copyProperties(param, entity);
        return updateById(entity);
    }

    @Override
    public Boolean updateRoleAndMenu(UpdateRoleAndMenuParam param) {
        SysRoleEntity entity = getById(param.getId());
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("角色不存在");
        }
        checkRoleCode(entity.getRoleCode(), param.getRoleCode());
        BeanUtil.copyProperties(param, entity);
        updateById(entity);
        Long roleId = entity.getRoleId();
        return sysRoleMenuService.saveRoleMenus(roleId, param.getAppCode(), param.getMenuIds());
    }

    private void checkRoleCode(String entityRoleCode, String paramRoleCode) {
        if (StrUtil.isNotBlank(paramRoleCode) && ObjectUtil.notEqual(entityRoleCode, paramRoleCode)) {
            ConfigVO defaultRoleCode = TenantBroker.applyAs(CommonConstants.TENANT_ID_DEFAULT, tenantId -> sysTenantConfigService.configGet(TenantConfigEnum.TENANT_DEFAULT_ROLECODE));
            if (ObjectUtil.equal(defaultRoleCode.getConfVal(), entityRoleCode)) {
                throw new ServiceException("默认超管角色编码不能修改");
            } else if (existsRoleCode(paramRoleCode)) {
                throw new ServiceException("角色编码已存在");
            }
        }
    }

    @Override
    public Boolean enabled(Long roleId) {
        SysRoleEntity entity = getById(roleId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("角色不存在");
        }
        LambdaUpdateWrapper<SysRoleEntity> up = new LambdaUpdateWrapper<>();
        up.eq(SysRoleEntity::getRoleId, roleId);
        up.set(SysRoleEntity::getEnabled, BooleanUtil.isFalse(entity.getEnabled()));
        return update(up);
    }

    @Override
    public SysRoleEntity getDefaultRoleByTenantId(Long tenantId) {
        ConfigVO defaultRoleCode = TenantBroker.applyAs(CommonConstants.TENANT_ID_DEFAULT, id -> sysTenantConfigService.configGet(TenantConfigEnum.TENANT_DEFAULT_ROLECODE));
        return TenantBroker.applyAs(tenantId, id -> getOne(Wrappers.<SysRoleEntity>lambdaQuery().eq(SysRoleEntity::getRoleCode, defaultRoleCode.getConfVal())));
    }

    @Override
    public Boolean existsRoleCode(String roleCode) {
        LambdaQueryWrapper<SysRoleEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysRoleEntity::getRoleCode, roleCode);
        return baseMapper.exists(qr);
    }

    @Override
    public Boolean saveDefaultRoleMenus(Long tenantId, Collection<Long> menuIds) {
        SysRoleEntity defaultRole = getDefaultRoleByTenantId(tenantId);
        if (ObjectUtil.isNull(defaultRole)) {
            throw new ServiceException("默认角色不存在");
        }
        Long roleId = defaultRole.getRoleId();
        return sysRoleMenuService.saveRoleMenus(roleId, null, menuIds);
    }
}

