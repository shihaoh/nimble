package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.mybatis.tenant.TenantBroker;
import cn.shihh.nimble.system.pojo.entity.SysApplicationEntity;
import cn.shihh.nimble.system.pojo.entity.SysMenuEntity;
import cn.shihh.nimble.system.pojo.vo.SysAppMenuTreeVO;
import cn.shihh.nimble.system.service.SysApplicationService;
import cn.shihh.nimble.common.constant.CacheConstants;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.common.mybatis.tenant.TenantContextHolder;
import cn.shihh.nimble.common.redis.util.RedisUtil;
import cn.shihh.nimble.system.mapper.SysApplicationMapper;
import cn.shihh.nimble.system.pojo.param.AddAppParam;
import cn.shihh.nimble.system.pojo.param.AddMenuParam;
import cn.shihh.nimble.system.pojo.param.AddTenantApplicationParam;
import cn.shihh.nimble.system.pojo.vo.SysApplicationEntryVO;
import cn.shihh.nimble.system.pojo.vo.SysApplicationVO;
import cn.shihh.nimble.system.pojo.vo.SysMenuListVO;
import cn.shihh.nimble.system.service.SysMenuService;
import cn.shihh.nimble.system.service.SysRoleService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 应用表(SysApplication)表服务实现类
 *
 * @author makejava
 * @since 2023-05-15 14:47:28
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SysApplicationServiceImpl extends ServiceImpl<SysApplicationMapper, SysApplicationEntity> implements SysApplicationService {

    private final SysMenuService sysMenuService;
    private final SysRoleService sysRoleService;

    @Override
    public SysApplicationEntity getByCode(String code) {
        LambdaQueryWrapper<SysApplicationEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysApplicationEntity::getCode, code);
        return getOne(qr);
    }

    @Override
    public Boolean addApplication(AddAppParam param) {
        SysApplicationEntity byCode = getByCode(param.getCode());
        if (ObjectUtil.isNotNull(byCode)) {
            throw new ServiceException("应用编码已存在");
        }
        SysApplicationEntity entity = BeanUtil.toBean(param, SysApplicationEntity.class);
        return save(entity);
    }

    @Override
    public Boolean saveApplicationMenu(String appCode, AddMenuParam param) {
        SysApplicationEntity application = getByCode(appCode);
        if (ObjectUtil.isNull(application)) {
            throw new ServiceException("应用不存在");
        }
        SysMenuEntity menu = BeanUtil.toBean(param, SysMenuEntity.class);
        menu.setAppId(application.getAppId());
        return sysMenuService.save(menu);
    }

    @Override
    public Boolean deleteApplicationMenu(List<Long> menuIds) {
        if (CollUtil.isEmpty(menuIds)) {
            return false;
        }
        return sysMenuService.removeByIds(menuIds);
    }

    @Override
    @CacheEvict(value = CacheConstants.MENU_APPLICATION_TREE, key = "#appCode")
    public void deleteMenuApplicationCache(String appCode) {
        log.info("删除应用菜单缓存 {}", appCode);
    }

    @Override
    @Cacheable(value = CacheConstants.MENU_APPLICATION_TREE, key = "#appCode", unless = "#result.isEmpty()")
    public List<Tree<Long>> getApplicationMenuTree(String appCode) {
        List<SysMenuEntity> list = sysMenuService.getMenuByAppCode(appCode);
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        return sysMenuService.treeMenu(list);
    }

    @Override
    public Boolean updateApplication(Long appId, AddAppParam param) {
        SysApplicationEntity entity = getById(appId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("应用不存在");
        }
        if (StrUtil.isNotBlank(param.getCode()) && ObjectUtil.notEqual(entity.getCode(), param.getCode())) {
            SysApplicationEntity byCode = getByCode(param.getCode());
            if (ObjectUtil.isNotNull(byCode)) {
                throw new ServiceException("应用编码已存在");
            }
        }
        BeanUtil.copyProperties(param, entity);
        return updateById(entity);
    }

    @Override
    public Boolean deleteApplication(Long appId) {
        SysApplicationEntity entity = getById(appId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("应用不存在");
        }
        return removeById(appId);
    }

    @Override
    public Boolean deleteApplicationByTenantId(Long tenantId) {
        return TenantBroker.applyAs(tenantId, id -> {
            LambdaQueryWrapper<SysApplicationEntity> qr = new LambdaQueryWrapper<>();
            qr.eq(SysApplicationEntity::getTenantId, id);
            return remove(qr);
        });
    }

    @Override
    public SysApplicationVO getApplication(Long appId) {
        SysApplicationEntity entity = getById(appId);
        return BeanUtil.toBean(entity, SysApplicationVO.class);
    }

    // 保存菜单线程变量
    private static final ThreadLocal<Set<Long>> SAVE_MENU_ID_THREAD_LOCAL = new ThreadLocal<>();

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean addTenantApplication(AddTenantApplicationParam param) {
        if (CollUtil.isEmpty(param.getMenuList())) {
            deleteApplicationByTenantId(param.getTenantId());
            sysRoleService.saveDefaultRoleMenus(param.getTenantId(), Collections.emptyList());
            return sysMenuService.deleteMenuByTenantId(param.getTenantId());
        }

        Map<Long, SysMenuListVO> saveMenuMap = param.getMenuList().stream().collect(Collectors.toMap(SysMenuListVO::getId, Function.identity()));
        Set<Long> saveMenuIds = saveMenuMap.keySet();
        List<SysMenuEntity> menus = sysMenuService.listByIds(saveMenuIds);
        if (CollUtil.isEmpty(menus)) {
            throw new ServiceException("菜单不存在");
        }

        // 租户已保存的菜单
        List<SysMenuEntity> tenantMenus = TenantBroker.applyAs(param::getTenantId, id -> sysMenuService.list());
        Map<Long, Long> tenantMenuByOriginMenuId = tenantMenus.stream().collect(Collectors.toMap(SysMenuEntity::getOriginMenuId, SysMenuEntity::getMenuId));

        List<Long> saveAppIds = menus.stream().map(SysMenuEntity::getAppId).distinct().collect(Collectors.toList());
        List<SysApplicationEntity> apps = listByIds(saveAppIds);
        Map<Long, SysApplicationEntity> appMapByAppid = apps.stream().collect(Collectors.toMap(SysApplicationEntity::getAppId, Function.identity()));
        // 租户已保存的应用
        List<SysApplicationEntity> tenantApps = TenantBroker.applyAs(param::getTenantId, id -> getTenantApplicationByOriginAppIds(saveAppIds));
        Map<Long, Long> tenantAppByOriginAppId = tenantApps.stream().collect(Collectors.toMap(SysApplicationEntity::getOriginAppId, SysApplicationEntity::getAppId));

        // 创建线程空间来保存已保存的菜单ID
        SAVE_MENU_ID_THREAD_LOCAL.set(Sets.newHashSet());
        // 分组创建菜单
        Map<Long, List<SysMenuEntity>> menuMapByAppid = menus.stream()
                .collect(Collectors.groupingBy(SysMenuEntity::getAppId));
        menuMapByAppid.forEach((k, v) -> {
            Long tenantAppId = tenantAppByOriginAppId.get(k);
            if (tenantAppId == null) {
                SysApplicationEntity app = appMapByAppid.get(k);
                app.setOriginAppId(app.getAppId());
                app.setAppId(null);
                app.setTenantId(param.getTenantId());
                TenantBroker.applyAs(param::getTenantId, id -> save(app));
                tenantAppId = app.getAppId();
            } else {
                tenantAppByOriginAppId.remove(k);
            }
            Long finalTenantAppId = tenantAppId;
            TenantBroker.runAs(param::getTenantId, id -> saveTenantMenu(v, finalTenantAppId, tenantMenuByOriginMenuId, saveMenuMap));
        });

        // 删除未传入的应用和菜单
        if (CollUtil.isNotEmpty(tenantAppByOriginAppId)) {
            TenantBroker.applyAs(param::getTenantId, id -> removeByIds(tenantAppByOriginAppId.values()));
        }
        if (CollUtil.isNotEmpty(tenantMenuByOriginMenuId)) {
            TenantBroker.runAs(param::getTenantId, id -> sysMenuService.removeMenuByIds(tenantMenuByOriginMenuId.values()));
        }

        // 关联到默认角色上
        sysRoleService.saveDefaultRoleMenus(param.getTenantId(), SAVE_MENU_ID_THREAD_LOCAL.get());
        SAVE_MENU_ID_THREAD_LOCAL.remove();
        return true;
    }

    /**
     * 保存新的租户菜单，维护成新的菜单
     *
     * @param menuList                 菜单列表
     * @param appId                    应用id
     * @param tenantMenuByOriginMenuId 当前租户已保存的id映射
     * @param saveMenuMap              保存菜单map
     */
    private void saveTenantMenu(List<SysMenuEntity> menuList, Long appId, Map<Long, Long> tenantMenuByOriginMenuId, Map<Long, SysMenuListVO> saveMenuMap) {
        Map<Long, List<SysMenuEntity>> menuByParentIdMap = menuList.stream().collect(Collectors.groupingBy(SysMenuEntity::getParentId));
        // 递归保存菜单，从根节点 -1 开始
        saveTenantMenu(menuByParentIdMap, appId, CommonConstants.MENU_TREE_ROOT_ID, CommonConstants.MENU_TREE_ROOT_ID, tenantMenuByOriginMenuId, saveMenuMap);
        // 根节点以外的菜单则属于新增子级菜单，需要将新增菜单与原始菜单相关的租户父级菜单绑定保存
        if (CollUtil.isNotEmpty(menuByParentIdMap)) {
            // menuByParentIdMap的key为新增菜单的父级id，可以通过新增菜单的父级id找到租户父级菜单
            Set<Long> menuIds = menuByParentIdMap.keySet();
            // 通过原始菜单id获取租户父级菜单
            List<SysMenuEntity> tenantParentMenu = sysMenuService.getTenantMenuByOriginMenuIds(menuIds);
            // 如果存在租户父级菜单，则将新增菜单与原始菜单相关的租户父级菜单绑定保存
            // 不存在租户父级菜单的新增子菜单则抛弃
            if (CollUtil.isNotEmpty(tenantParentMenu)) {
                // 根据原始菜单id对应租户现存父级菜单，key为原始菜单id，value为租户父级菜单id
                Map<Long, Long> tenantParentMenuByOriginMenuMap = tenantParentMenu.stream()
                        .collect(Collectors.toMap(SysMenuEntity::getOriginMenuId, SysMenuEntity::getMenuId));
                for (Long menuId : menuIds) {
                    if (tenantParentMenuByOriginMenuMap.containsKey(menuId)) {
                        saveTenantMenu(menuByParentIdMap, appId, menuId, tenantParentMenuByOriginMenuMap.get(menuId), tenantMenuByOriginMenuId, saveMenuMap);
                    }
                }
            }
        }
    }

    /**
     * 递归保存菜单
     *
     * @param menuByParentIdMap        菜单Map，可通过父级菜单查询到子级菜单列表
     * @param appId                    应用id
     * @param originParentId           原始父级菜单id
     * @param targetParentId           保存父级菜单id
     * @param tenantMenuByOriginMenuId 当前租户已保存的id映射
     * @param saveMenuMap              保存菜单map
     */
    private void saveTenantMenu(Map<Long, List<SysMenuEntity>> menuByParentIdMap, Long appId, Long originParentId, Long targetParentId, Map<Long, Long> tenantMenuByOriginMenuId, Map<Long, SysMenuListVO> saveMenuMap) {
        List<SysMenuEntity> menuList = menuByParentIdMap.get(originParentId);
        if (CollUtil.isEmpty(menuList)) {
            return;
        }
        menuList.forEach(menu -> {
            // 保存菜单原始menuId， 方便查询子节点使用
            Long originMenuId = menu.getMenuId();
            Long tenantMenuId = tenantMenuByOriginMenuId.get(originMenuId);
            SysMenuListVO saveMenuParam = saveMenuMap.get(originMenuId);
            if (tenantMenuId == null) {
                menu.setMenuId(null);
                menu.setOriginMenuId(originMenuId);
                menu.setAppId(appId);
                menu.setParentId(targetParentId);
                menu.setTenantId(TenantContextHolder.getTenantId());
                if (BooleanUtil.isTrue(saveMenuParam.getIsAlias())) {
                    menu.setIsAlias(Boolean.TRUE);
                    menu.setName(saveMenuParam.getName());
                }
                sysMenuService.save(menu);
                tenantMenuId = menu.getMenuId();
            } else {
                if (BooleanUtil.isTrue(saveMenuParam.getIsAlias())) {
                    // 别名修改
                    sysMenuService.lambdaUpdate()
                            .eq(SysMenuEntity::getMenuId, tenantMenuId)
                            .set(SysMenuEntity::getName, saveMenuParam.getName())
                            .set(SysMenuEntity::getIsAlias, Boolean.TRUE)
                            .update();
                }
                tenantMenuByOriginMenuId.remove(originMenuId);
            }
            SAVE_MENU_ID_THREAD_LOCAL.get().add(tenantMenuId);
            // 查找此节点的子节点，然后子节点的重新插入父节点更改为新的menuId
            saveTenantMenu(menuByParentIdMap, appId, originMenuId, tenantMenuId, tenantMenuByOriginMenuId, saveMenuMap);
        });
        // 保存完成后移除此节点，在保存查询完成后可通过map状态判断是否进入新增子节点菜单的保存逻辑
        menuByParentIdMap.remove(originParentId);
    }

    @Override
    public List<SysApplicationVO> getTenantApplication(Long tenantId) {
        LambdaQueryWrapper<SysApplicationEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysApplicationEntity::getTenantId, tenantId);
        List<SysApplicationEntity> list = list(qr);
        if (ObjectUtil.isNull(list)) {
            return Collections.emptyList();
        }
        return BeanUtil.copyToList(list, SysApplicationVO.class);
    }

    @Override
    public List<SysApplicationEntryVO> getEntryApplication() {
        String key = CacheConstants.ENTRY_APPLICATION;
        Object cache = RedisUtil.getObject(key);
        if (ObjectUtil.isNotNull(cache)) {
            return (List<SysApplicationEntryVO>) cache;
        }
        List<SysApplicationEntity> list = TenantBroker.applyAs(CommonConstants.TENANT_ID_DEFAULT, id -> {
            LambdaQueryWrapper<SysApplicationEntity> qr = new LambdaQueryWrapper<>();
            qr.eq(SysApplicationEntity::getTenantId, id);
            return list(qr);
        });
        if (ObjectUtil.isNull(list)) {
            return Collections.emptyList();
        }
        List<SysApplicationEntryVO> res = BeanUtil.copyToList(list, SysApplicationEntryVO.class);
        RedisUtil.putObject(key, res);
        return res;
    }

    @Override
    public List<SysAppMenuTreeVO> getTenantApplicationMenuTree(Long tenantId) {
        LambdaQueryWrapper<SysApplicationEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysApplicationEntity::getTenantId, tenantId);
        List<SysApplicationEntity> list = list(qr);
        if (ObjectUtil.isNull(list)) {
            return Collections.emptyList();
        }
        List<SysAppMenuTreeVO> result = BeanUtil.copyToList(list, SysAppMenuTreeVO.class);
        result.forEach(item -> item.setMenuTree(getApplicationMenuTree(item.getCode())));
        return result;
    }

    @Override
    public Map<Long, List<SysMenuListVO>> getTenantMenuList(Long tenantId) {
        List<SysMenuEntity> tenantMenus = TenantBroker.applyAs(tenantId, id -> sysMenuService.list());
        if (CollUtil.isEmpty(tenantMenus)) {
            return Collections.emptyMap();
        }
        Map<Long, List<SysMenuEntity>> tenantMenuMapByAppId = tenantMenus.stream()
                .collect(Collectors.groupingBy(SysMenuEntity::getAppId));
        List<SysApplicationEntity> tenantApps = TenantBroker.applyAs(tenantId, id -> listByIds(tenantMenuMapByAppId.keySet()));
        Map<Long, Long> tenantAppByOriginAppId = tenantApps.stream()
                .collect(Collectors.toMap(SysApplicationEntity::getAppId, SysApplicationEntity::getOriginAppId));

        Map<Long, List<SysMenuListVO>> res = new HashMap<>();
        tenantMenuMapByAppId.forEach((k, v) -> {
            Long originAppId = tenantAppByOriginAppId.get(k);
            List<SysMenuListVO> resList = new ArrayList<>();
            v.forEach(tenantMenu -> {
                SysMenuListVO vo = new SysMenuListVO();
                vo.setId(tenantMenu.getOriginMenuId());
                vo.setName(tenantMenu.getName());
                vo.setIsAlias(tenantMenu.getIsAlias());
                resList.add(vo);
            });
            res.put(originAppId, resList);
        });
        return res;
    }

    @Override
    public List<SysApplicationEntity> getTenantApplicationByOriginAppIds(Collection<Long> originAppIds) {
        if (CollUtil.isEmpty(originAppIds)) {
            return Collections.emptyList();
        }
        LambdaQueryWrapper<SysApplicationEntity> qr = new LambdaQueryWrapper<>();
        qr.in(SysApplicationEntity::getOriginAppId, originAppIds);
        return list(qr);
    }

    @Override
    public Map<Long, Set<Long>> roleAppMenu(Long roleId) {
        List<SysMenuEntity> menus = baseMapper.getApplicationMenuByRoleId(roleId);
        if (CollUtil.isEmpty(menus)) {
            return MapUtil.empty();
        }
        return menus.stream()
                .collect(Collectors.groupingBy(SysMenuEntity::getAppId,
                        Collectors.mapping(SysMenuEntity::getMenuId, Collectors.toSet())));
    }

    @Override
    public List<SysApplicationVO> getUserApplication(Long userId) {
        return baseMapper.getApplicationByUserId(userId);
    }

    @Override
    public List<Tree<Long>> getUserMenuApplication(Long userId, Long appId) {
        List<SysMenuEntity> menus = baseMapper.getApplicationMenuByUserIdAndAppId(userId, appId);
        if (CollUtil.isEmpty(menus)) {
            return Collections.emptyList();
        }
        return sysMenuService.treeMenu(menus);
    }

    @Override
    public List<Tree<Long>> getUserMenuApplicationByCode(Long userId, String code) {
        List<SysMenuEntity> menus = baseMapper.getApplicationMenuByUserIdAndAppCode(userId, code);
        if (CollUtil.isEmpty(menus)) {
            return Collections.emptyList();
        }
        return sysMenuService.treeMenu(menus);
    }
}

