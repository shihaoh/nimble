package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 租户表(SysTenant)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_tenant", autoResultMap = true)
public class SysTenantEntity extends BaseEntity {
    /**
     * 租户id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 
     */
    @TableField("name")
    private String name;

    /**
     * 
     */
    @TableField("code")
    private String code;

    /**
     * 
     */
    @TableField("tenant_domain")
    private String tenantDomain;

    /**
     * 开始时间
     */
    @TableField("start_time")
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    @TableField("end_time")
    private LocalDateTime endTime;

    /**
     * 
     */
    @TableField("status")
    private String status;

    /**
     * 
     */
    @TableField("menu_id")
    private Long menuId;

}

