package cn.shihh.nimble.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;

/**
 * 部门关系表(SysDeptRelation)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_dept_relation", autoResultMap = true)
public class SysDeptRelationEntity {
    /**
     * 祖先节点
     */
    private Long ancestor;
    /**
     * 后代节点
     */
    private Long descendant;

}

