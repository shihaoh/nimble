package cn.shihh.nimble.system.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.pojo.entity.SysDeptEntity;
import cn.shihh.nimble.system.pojo.param.AddDeptParam;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.service.SysDeptService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import jakarta.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/sys/dept")
@Tag(name = "部门管理模块")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class SysDeptController {

	private final SysDeptService sysDeptService;

	@Operation(summary = "根据ID查询部门")
	@GetMapping("/{id}")
	public R<SysDeptVO> getById(@PathVariable Long id) {
		SysDeptEntity entity = sysDeptService.getById(id);
		return R.ok(BeanUtil.copyProperties(entity, SysDeptVO.class));
	}

	@Operation(summary = "查询部门列表")
	@GetMapping("/list")
	public R<List<SysDeptVO>> list() {
		List<SysDeptEntity> list = sysDeptService.list();
		return R.ok(BeanUtil.copyToList(list, SysDeptVO.class));
	}

	@Operation(summary = "查询部门树")
	@GetMapping(value = "/tree")
	public R<List<Tree<Long>>> getTree() {
		return R.ok(sysDeptService.selectTree());
	}

	@Operation(summary = "添加部门")
	@PostMapping
	public R<Boolean> save(@Valid @RequestBody AddDeptParam param) {
		return R.ok(sysDeptService.saveDept(param));
	}

	@Operation(summary = "删除部门")
	@DeleteMapping("/{id}")
	public R<Boolean> removeById(@PathVariable Long id) {
		return R.ok(sysDeptService.removeDeptById(id));
	}

	@Operation(summary = "修改部门")
	@PutMapping("{deptId}")
	public R<Boolean> update(@PathVariable Long deptId, @Valid @RequestBody AddDeptParam param) {
		return R.ok(sysDeptService.updateDeptById(deptId, param));
	}

	@Operation(summary = "查询子级部门")
	@GetMapping(value = "/getDescendantList/{deptId}")
	public R<List<SysDeptVO>> getDescendantList(@PathVariable Long deptId) {
		return R.ok(sysDeptService.getDescendantList(deptId));
	}

}
