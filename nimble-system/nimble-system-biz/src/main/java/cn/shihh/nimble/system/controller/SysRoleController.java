package cn.shihh.nimble.system.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.shihh.nimble.common.mybatis.page.PageParamVO;
import cn.shihh.nimble.common.mybatis.page.PageUtil;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.pojo.entity.SysRoleEntity;
import cn.shihh.nimble.system.pojo.param.AddRoleParam;
import cn.shihh.nimble.system.pojo.param.RoleMenusParam;
import cn.shihh.nimble.system.pojo.param.UpdateRoleAndMenuParam;
import cn.shihh.nimble.system.pojo.param.UpdateRoleParam;
import cn.shihh.nimble.system.pojo.vo.SysRoleVO;
import cn.shihh.nimble.system.service.SysDeptService;
import cn.shihh.nimble.system.service.SysMenuService;
import cn.shihh.nimble.system.service.SysRoleService;
import cn.shihh.nimble.system.service.SysUserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author shihh
 * @since 2023/3/20
 */
@Tag(name = "角色管理模块")
@RestController
@RequestMapping("sys/role")
@RequiredArgsConstructor
public class SysRoleController {

    private final SysRoleService sysRoleService;
    private final SysDeptService sysDeptService;
    private final SysMenuService sysMenuService;
    private final SysUserRoleService sysUserRoleService;

    @Operation(summary = "获取角色列表")
    @GetMapping
    public R<List<SysRoleVO>> listRoles() {
        LambdaQueryWrapper<SysRoleEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysRoleEntity::getEnabled, true);
        List<SysRoleEntity> list = sysRoleService.list(qr);
        return R.ok(BeanUtil.copyToList(list, SysRoleVO.class));
    }

    @Operation(summary = "获取角色分页列表")
    @GetMapping("page")
    public R<PageVO<SysRoleVO>> page(PageParamVO<SysRoleEntity> param) {
        IPage<SysRoleEntity> page = param.toPage();
        IPage<SysRoleEntity> res = sysRoleService.page(page);
        PageVO<SysRoleVO> result = PageUtil.pageToVO(res, SysRoleVO.class);
        List<SysRoleVO> content = result.getContent();
        if (CollUtil.isNotEmpty(content)) {
            List<Long> roleIds = content.stream().map(SysRoleVO::getRoleId).collect(Collectors.toList());
            Map<Long, Integer> countMaps = sysUserRoleService.countsByRoleIds(roleIds);
            content.forEach(item -> {
                item.setDsScopeArray(sysDeptService.getDeptArrayByDeptIdStrs(item.getDsScope()));
                item.setUserCount(ObjectUtil.defaultIfNull(countMaps.get(item.getRoleId()), 0));
            });
        }
        return R.ok(result);
    }

    @Operation(summary = "更新角色菜单")
    @PutMapping("menu")
    public R<Boolean> updateRoleMenu(@RequestBody RoleMenusParam param) {
        return R.ok(sysRoleService.updateRoleMenus(param.getRoleId(), param.getMenuIds()));
    }

    @Operation(summary = "查询角色信息")
    @GetMapping("{roleId}")
    public R<SysRoleVO> get(@PathVariable Long roleId) {
        SysRoleEntity entity = sysRoleService.getById(roleId);
        SysRoleVO res = BeanUtil.copyProperties(entity, SysRoleVO.class);
        res.setMenuIds(sysMenuService.findMenuIdByRoleId(roleId));
        return R.ok(res);
    }

    @Operation(summary = "查询角色信息,根据应用编码")
    @GetMapping("/{appCode}/{roleId}")
    public R<SysRoleVO> getByAppCode(@PathVariable String appCode,
                                     @PathVariable Long roleId) {
        SysRoleEntity entity = sysRoleService.getById(roleId);
        SysRoleVO res = BeanUtil.copyProperties(entity, SysRoleVO.class);
        res.setMenuIds(sysMenuService.findMenuIdByRoleIdAndAppCode(roleId, appCode));
        return R.ok(res);
    }

    @Operation(summary = "新增角色")
    @PostMapping
    public R<Boolean> save(@RequestBody AddRoleParam param) {
        return R.ok(sysRoleService.saveRole(param));
    }

    @Operation(summary = "修改角色及角色菜单配置")
    @PutMapping
    public R<Boolean> updateRoleAndMenu(@RequestBody UpdateRoleAndMenuParam param) {
        return R.ok(sysRoleService.updateRoleAndMenu(param));
    }

    @Operation(summary = "修改角色")
    @PutMapping("{roleId}")
    public R<Boolean> update(@PathVariable("roleId") Long roleId, @RequestBody UpdateRoleParam param) {
        param.setId(roleId);
        return R.ok(sysRoleService.updateRole(param));
    }

    @Operation(summary = "删除角色")
    @DeleteMapping("{roleId}")
    public R<Boolean> delete(@PathVariable Long roleId) {
        return R.ok(sysRoleService.removeRoleById(roleId));
    }

    @Operation(summary = "启用/停用角色")
    @PutMapping("enabled/{roleId}")
    public R<Boolean> enabled(@PathVariable Long roleId) {
        return R.ok(sysRoleService.enabled(roleId));
    }

    @Operation(summary = "查询角色用户")
    @GetMapping("user/{roleId}")
    public R<List<SysUserBaseVO>> getRoleUser(@PathVariable Long roleId) {
        return R.ok(sysUserRoleService.getUsersByRoleId(roleId));
    }
}
