package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.shihh.nimble.system.mapper.SysDeptRelationMapper;
import cn.shihh.nimble.system.pojo.entity.SysDeptEntity;
import cn.shihh.nimble.system.pojo.entity.SysDeptRelationEntity;
import cn.shihh.nimble.system.service.SysDeptRelationService;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门关系表(SysDeptRelation)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Service
@RequiredArgsConstructor
public class SysDeptRelationServiceImpl extends ServiceImpl<SysDeptRelationMapper, SysDeptRelationEntity> implements SysDeptRelationService {

    /**
     * 维护部门关系
     * @param sysDept 部门
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertDeptRelation(SysDeptEntity sysDept) {
        // 增加部门关系表
        List<SysDeptRelationEntity> relationList = baseMapper.selectList(
                        Wrappers.<SysDeptRelationEntity>query().lambda()
                                .eq(SysDeptRelationEntity::getDescendant, sysDept.getParentId()))
                .stream().map(relation -> {
                    relation.setDescendant(sysDept.getDeptId());
                    return relation;
                }).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(relationList)) {
            relationList.forEach(ele -> baseMapper.insert(ele));
        }

        // 自己也要维护到关系表中
        SysDeptRelationEntity own = new SysDeptRelationEntity();
        own.setDescendant(sysDept.getDeptId());
        own.setAncestor(sysDept.getDeptId());
        baseMapper.insert(own);
    }

    /**
     * 通过ID删除部门关系
     * @param id
     */
    @Override
    public void deleteAllDeptRelation(Long id) {
        baseMapper.deleteDeptRelationsByDeptId(id);
    }

    /**
     * 更新部门关系
     * @param relation
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDeptRelation(SysDeptRelationEntity relation) {
        baseMapper.deleteDeptRelations(relation);
        baseMapper.insertDeptRelations(relation);
    }

}

