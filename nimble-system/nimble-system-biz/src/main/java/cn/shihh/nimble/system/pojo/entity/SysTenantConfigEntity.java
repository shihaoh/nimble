package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.TenantBaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 机构配置表(SysTenantConfig)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-21 15:51:25
 */
@Data
@TableName(value = "sys_tenant_config", autoResultMap = true)
public class SysTenantConfigEntity extends TenantBaseEntity {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 配置分类
     */
    @TableField("conf_type")
    private String confType;

    /**
     * 配置键
     */
    @TableField("conf_key")
    private String confKey;

    /**
     * 配置值
     */
    @TableField("conf_value")
    private String confValue;

    /**
     * 配置默认值
     */
    @TableField("conf_default_value")
    private String confDefaultValue;

    /**
     * 配置描述
     */
    @TableField("conf_desc")
    private String confDesc;
}

