package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 应用表(SysApplication)表实体类
 *
 * @author makejava
 * @since 2023-05-15 15:45:43
 */
@Data
@Schema(description = "应用表")
public class SysApplicationBO {
    
    @Schema(description = "应用ID")
    private Long appId;
    
    @Schema(description = "应用名称")
    private String name;
    
    @Schema(description = "应用代码")
    private String code;
    
    @Schema(description = "应用入口")
    private String entry;
    
    @Schema(description = "应用图标")
    private String icon;
    
    @Schema(description = "应用首页")
    private String homepage;
    
    @Schema(description = "备注")
    private String remark;
    
    @Schema(description = "状态")
    private Boolean status;
    
    @Schema(description = "排序值")
    private Integer sortOrder;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "创建时间")
    private LocalDateTime createTime;
    
    @Schema(description = "修改人")
    private String updateBy;
    
    @Schema(description = "更新时间")
    private LocalDateTime updateTime;
    
    @Schema(description = "${column.comment}")
    private String delFlag;
    
    @Schema(description = "租户ID")
    private Integer tenantId;

}

