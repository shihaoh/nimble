package cn.shihh.nimble.system.pojo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/3/22
 */
@Data
@Schema(description = "添加租户菜单参数")
public class AddTenantMenuParam {

    @Schema(description = "租户菜单名称")
    private String name;

    @Schema(description = "绑定菜单列表 ',' 分割")
    private String menuIds;

    @Schema(description = "租户菜单,9:冻结,0:正常")
    private String status;
}
