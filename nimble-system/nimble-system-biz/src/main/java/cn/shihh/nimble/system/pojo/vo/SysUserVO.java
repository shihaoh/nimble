package cn.shihh.nimble.system.pojo.vo;


import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * 用户表(SysUser)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:18
 */
@Data
@Schema(description = "用户信息")
public class SysUserVO extends SysUserBaseVO {
    
    @Schema(description = "拓展字段:邮箱")
    private String email;

    @Schema(description = "部门名称")
    private String deptName;

    @Schema(description = "角色名称")
    private String roleNames;

    @Schema(description = "职位名称")
    private String postNames;

    @Schema(description = "拥有的角色列表")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Long> roleList;

    @Schema(description = "拥有的岗位列表")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Long> postList;
}

