package cn.shihh.nimble.system.pojo.entity;

import cn.shihh.nimble.common.mybatis.entity.BaseEntity;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * (SysTenantMenu)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_tenant_menu", autoResultMap = true)
public class SysTenantMenuEntity extends BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 租户菜单名称
     */
    @TableField("name")
    private String name;

    /**
     * 
     */
    @TableField("menu_ids")
    private String menuIds;

    /**
     * 租户菜单,9:冻结,0:正常
     */
    @TableField("status")
    private String status;

}

