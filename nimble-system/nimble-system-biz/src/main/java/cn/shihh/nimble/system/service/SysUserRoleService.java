package cn.shihh.nimble.system.service;

import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.pojo.entity.SysUserRoleEntity;
import cn.shihh.nimble.common.body.RoleBody;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 用户角色表(SysUserRole)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {

    /**
     * 根据用户删除关联
     * @param userId
     * @return
     */
    Boolean removeByUserId(Long userId);

    /**
     * 根据角色删除关联
     * @param roleId
     * @return
     */
    Boolean removeByRoleId(Long roleId);

    /**
     * 根据用户查询角色
     * @param userId
     * @return
     */
    List<Long> listRoleIdByUserId(Long userId);

    /**
     * 根据用户查询角色
     * @param userId
     * @return
     */
    List<RoleBody> listRoleByUserId(Long userId);

    /**
     * 保存用户角色关联
     * @param userId
     * @param roleIds
     * @return
     */
    Boolean saveUserRole(Long userId, List<Long> roleIds);

    /**
     * 根据角色查询用户数量
     * @param roleIds
     * @return
     */
    Map<Long, Integer> countsByRoleIds(List<Long> roleIds);

    /**
     * 根据角色查询用户
     * @param roleId
     * @return
     */
    List<SysUserBaseVO> getUsersByRoleId(Long roleId);
}

