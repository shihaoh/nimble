package cn.shihh.nimble.system.pojo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author shihh
 * @since 2023/3/21
 */
@Data
@Schema(description = "角色菜单参数")
public class RoleMenusParam {

    @Schema(description = "角色id")
    private Long roleId;

    @Schema(description = "菜单编号列表")
    private List<Long> menuIds;
}
