package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.system.mapper.SysDeptMapper;
import cn.shihh.nimble.system.service.SysDeptRelationService;
import cn.shihh.nimble.system.pojo.entity.SysDeptEntity;
import cn.shihh.nimble.system.pojo.entity.SysDeptRelationEntity;
import cn.shihh.nimble.system.pojo.param.AddDeptParam;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.service.SysDeptService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 部门管理(SysDept)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:52
 */
@Service
@RequiredArgsConstructor
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDeptEntity> implements SysDeptService {

    private final SysDeptRelationService sysDeptRelationService;

    /**
     * 添加信息部门
     *
     * @param dept 部门
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean saveDept(AddDeptParam dept) {
        SysDeptEntity sysDept = new SysDeptEntity();
        BeanUtils.copyProperties(dept, sysDept);
        this.save(sysDept);
        sysDeptRelationService.insertDeptRelation(sysDept);
        return Boolean.TRUE;
    }

    /**
     * 删除部门
     * @param id 部门 ID
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean removeDeptById(Long id) {
        // 级联删除部门
        List<Long> idList = sysDeptRelationService
                .list(Wrappers.<SysDeptRelationEntity>query().lambda().eq(SysDeptRelationEntity::getAncestor, id)).stream()
                .map(SysDeptRelationEntity::getDescendant).collect(Collectors.toList());

        if (CollUtil.isNotEmpty(idList)) {
            this.removeByIds(idList);
        }

        // 删除部门级联关系
        sysDeptRelationService.deleteAllDeptRelation(id);
        return Boolean.TRUE;
    }

    /**
     * 更新部门
     *
     * @param deptId
     * @param param  部门信息
     * @return 成功、失败
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean updateDeptById(Long deptId, AddDeptParam param) {
        SysDeptEntity entity = getById(deptId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("部门不存在");
        }
        BeanUtil.copyProperties(param, entity);
        // 更新部门状态
        this.updateById(entity);
        // 更新部门关系
        SysDeptRelationEntity relation = new SysDeptRelationEntity();
        relation.setAncestor(param.getParentId());
        relation.setDescendant(deptId);
        sysDeptRelationService.updateDeptRelation(relation);
        return Boolean.TRUE;
    }

    @Override
    public List<Tree<Long>> selectTree() {
        // 查询全部部门
        List<SysDeptEntity> deptAllList = list();

        List<TreeNode<Long>> collect = deptAllList.stream()
                .filter(dept -> !Objects.equals(dept.getDeptId(), dept.getParentId()))
                .sorted(Comparator.comparingInt(SysDeptEntity::getSortOrder))
                .map(dept -> {
                    TreeNode<Long> treeNode = new TreeNode();
                    treeNode.setId(dept.getDeptId());
                    treeNode.setParentId(dept.getParentId());
                    treeNode.setName(dept.getName());
                    treeNode.setWeight(dept.getSortOrder());
                    // 有权限不返回标识
                    Map<String, Object> extra = new HashMap<>(8);
                    extra.put("createTime", dept.getCreateTime());
                    treeNode.setExtra(extra);
                    return treeNode;
                }).collect(Collectors.toList());

        return TreeUtil.build(collect, 0L);
    }

    @Override
    public List<SysDeptVO> getDescendantList(Long deptId) {
        return baseMapper.getDescendantList(deptId);
    }

    @Override
    public JSONArray getDeptArrayByDeptIdStrs(String deptIdStr) {
        JSONArray res = new JSONArray();
        if (StrUtil.isBlank(deptIdStr)) {
            return res;
        }
        List<String> deptIds = StrUtil.split(deptIdStr, ',');
        List<SysDeptEntity> depts = listByIds(deptIds);
        if (CollUtil.isNotEmpty(depts)) {
            for (SysDeptEntity dept : depts) {
                JSONObject d = new JSONObject();
                d.put("label", dept.getName());
                d.put("value", dept.getDeptId());
                res.add(d);
            }
        }
        return res;
    }

}

