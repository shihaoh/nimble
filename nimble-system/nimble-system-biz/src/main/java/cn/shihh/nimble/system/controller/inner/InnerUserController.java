package cn.shihh.nimble.system.controller.inner;

import cn.shihh.nimble.common.body.LoginUser;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.client.RemoteUserClient;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.service.SysUserService;
import cn.shihh.nimble.system.api.pojo.vo.SysDeptVO;
import cn.shihh.nimble.system.api.pojo.vo.SysUserFromCardVO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

/**
 * @author shihh
 * @since 2023/3/22
 */
@RestController
@RequiredArgsConstructor
public class InnerUserController implements RemoteUserClient {

    private final SysUserService sysUserService;

    @Override
    public R<LoginUser> getUserByPhone(Long tenantId, String phone) {
        return R.ok(sysUserService.getLoginUserByPhone(phone));
    }

    @Override
    public R<List<SysUserBaseVO>> listByRoleIds(@RequestParam("roleId") Collection<Long> roleIds) {
        return R.ok(sysUserService.listByRoleIds(roleIds));
    }

    @Override
    public R<List<SysUserBaseVO>> listByKeywords(String keywords) {
        return R.ok(sysUserService.listByKeywords(keywords));
    }

    @Override
    public R<List<SysUserBaseVO>> listByPermissions(@RequestParam(value = "deptId", required = false) Long deptId) {
        return R.ok(sysUserService.listByPermissions(deptId));
    }

    @Override
    public R<List<SysUserFromCardVO>> getUserFromCardByUserIds(List<Long> userIds) {
        return R.ok(sysUserService.getUserFromCardByUserIds(userIds));
    }

    @Override
    public R<SysDeptVO> getUserDeptInfoByUserId(Long userId) {
        SysDeptVO dept = sysUserService.getUserDeptInfoByUserId(userId);
        return R.ok(dept);
    }
}
