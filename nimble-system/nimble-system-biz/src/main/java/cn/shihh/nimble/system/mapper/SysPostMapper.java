package cn.shihh.nimble.system.mapper;

import cn.shihh.nimble.system.pojo.entity.SysPostEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 岗位信息表(SysPost)表数据库访问层
 *
 * @author makejava
 * @since 2023-03-15 16:26:29
 */
public interface SysPostMapper extends BaseMapper<SysPostEntity> {
}

