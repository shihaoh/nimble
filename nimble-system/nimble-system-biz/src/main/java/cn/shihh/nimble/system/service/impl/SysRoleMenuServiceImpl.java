package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.shihh.nimble.common.constant.CacheConstants;
import cn.shihh.nimble.system.mapper.SysRoleMenuMapper;
import cn.shihh.nimble.system.pojo.entity.SysRoleMenuEntity;
import cn.shihh.nimble.system.service.SysRoleMenuService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 角色菜单表(SysRoleMenu)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Service
@RequiredArgsConstructor
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenuEntity> implements SysRoleMenuService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = CacheConstants.MENU_DETAILS, key = "#roleId")
    public Boolean saveRoleMenus(Long roleId, String appCode, Collection<Long> menuIds) {
        removeByAppIdAndRoleId(appCode, roleId);

        if (CollUtil.isEmpty(menuIds)) {
            return Boolean.TRUE;
        }
        List<SysRoleMenuEntity> roleMenuList = menuIds.stream().map(menuId -> {
            SysRoleMenuEntity roleMenu = new SysRoleMenuEntity();
            roleMenu.setRoleId(roleId);
            roleMenu.setMenuId(menuId);
            return roleMenu;
        }).collect(Collectors.toList());
        return saveBatch(roleMenuList);
    }

    @Override
    @CacheEvict(value = CacheConstants.MENU_DETAILS, key = "#roleId")
    public Boolean removeByRoleId(Long roleId) {
        LambdaQueryWrapper<SysRoleMenuEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysRoleMenuEntity::getRoleId, roleId);
        return remove(qr);
    }

    @Override
    @CacheEvict(value = CacheConstants.MENU_DETAILS, key = "#roleId")
    public Boolean removeByAppIdAndRoleId(String appCode, Long roleId) {
        if (ObjectUtil.isNull(appCode)) {
            return removeByRoleId(roleId);
        }
        List<SysRoleMenuEntity> rms = baseMapper.getByAppIdAndRoleId(appCode, roleId);
        if (CollUtil.isNotEmpty(rms)) {
            for (SysRoleMenuEntity rm : rms) {
                LambdaQueryWrapper<SysRoleMenuEntity> qr = new LambdaQueryWrapper<>();
                qr.eq(SysRoleMenuEntity::getRoleId, roleId);
                qr.eq(SysRoleMenuEntity::getMenuId, rm.getMenuId());
                remove(qr);
            }
        }
        return true;
    }
}

