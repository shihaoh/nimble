package cn.shihh.nimble.system.service;

import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.system.pojo.entity.SysTenantMenuEntity;
import cn.shihh.nimble.system.pojo.param.AddTenantMenuParam;
import cn.shihh.nimble.system.pojo.vo.SysTenantMenuVO;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * (SysTenantMenu)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysTenantMenuService extends IService<SysTenantMenuEntity> {

    /**
     * 分页查询租户菜单列表
     * @param objectPage
     * @param key
     * @param status
     * @return
     */
    PageVO<SysTenantMenuVO> pageSearch(Page<SysTenantMenuEntity> objectPage, String key, Integer status);

    /**
     * 保存租户菜单
     *
     * @param param
     * @return
     */
    Boolean saveTenantMenu(AddTenantMenuParam param);

    /**
     * 更新租户菜单
     * @param id
     * @param param
     * @return
     */
    Boolean updateTenantMenu(Long id, AddTenantMenuParam param);
}

