package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.common.constant.CacheConstants;
import cn.shihh.nimble.common.constant.CommonConstants;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.common.mybatis.enums.DataScopeTypeEnum;
import cn.shihh.nimble.common.mybatis.page.PageUtil;
import cn.shihh.nimble.common.mybatis.page.PageVO;
import cn.shihh.nimble.common.mybatis.tenant.TenantBroker;
import cn.shihh.nimble.system.api.enums.TenantConfigEnum;
import cn.shihh.nimble.system.api.pojo.vo.ConfigVO;
import cn.shihh.nimble.system.mapper.SysTenantMapper;
import cn.shihh.nimble.system.pojo.entity.*;
import cn.shihh.nimble.system.pojo.param.AddTenantParam;
import cn.shihh.nimble.system.pojo.vo.SysTenantVO;
import cn.shihh.nimble.system.service.*;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 租户表(SysTenant)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SysTenantServiceImpl extends ServiceImpl<SysTenantMapper, SysTenantEntity> implements SysTenantService {

    private static final PasswordEncoder ENCODER = new BCryptPasswordEncoder();

    private final SysDeptRelationService deptRelationService;

    private final SysUserRoleService userRoleService;

    private final SysUserService userService;

    private final SysRoleService roleService;

    private final SysDeptService deptService;

    private final SysTenantConfigService sysTenantConfigService;

    @Override
    public SysTenantVO getTenantByDomain(String domain) {
        LambdaQueryWrapper<SysTenantEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysTenantEntity::getTenantDomain, domain);
        SysTenantEntity entity = baseMapper.selectOne(qr);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("该域名未注册");
        }
        SysTenantVO res = new SysTenantVO();
        res.setName(entity.getName());
        res.setTenantDomain(entity.getTenantDomain());
        res.setId(entity.getId());
        return res;
    }

    @Override
    public Boolean existsByDomain(String domain) {
        LambdaQueryWrapper<SysTenantEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysTenantEntity::getTenantDomain, domain);
        return count(qr) > 0;
    }

    @Override
    public Boolean existsByCode(String code) {
        LambdaQueryWrapper<SysTenantEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysTenantEntity::getCode, code);
        return count(qr) > 0;
    }

    /**
     * 获取正常状态租户
     * <p>
     * 1. 状态正常 2. 开始时间小于等于当前时间 3. 结束时间大于等于当前时间
     *
     * @return
     */
    @Override
    @Cacheable(value = CacheConstants.TENANT_DETAILS)
    public List<SysTenantEntity> getNormalTenant() {
        return lambdaQuery()
                .eq(SysTenantEntity::getStatus, CommonConstants.STATUS_NORMAL)
                .le(SysTenantEntity::getStartTime, LocalDateTime.now())
                .ge(SysTenantEntity::getEndTime, LocalDateTime.now())
                .list();
    }

    /**
     * 保存租户
     * <p>
     * 1. 保存租户
     * 2. 初始化权限相关表
     * - sys_user
     * - sys_role
     * - sys_menu
     * - sys_user_role
     * - sys_role_menu
     * - sys_client_details
     * - sys_public_params
     *
     * @param param 租户实体
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = CacheConstants.TENANT_DETAILS)
    public Boolean saveTenant(AddTenantParam param) {
        if (existsByDomain(param.getTenantDomain())) {
            throw new ServiceException("该域名已存在");
        }
        if (existsByCode(param.getCode())) {
            throw new ServiceException("该租户编码已存在");
        }
        SysTenantEntity entity = BeanUtil.toBean(param, SysTenantEntity.class);
        this.save(entity);

        ConfigVO defaultDept = sysTenantConfigService.configGet(TenantConfigEnum.TENANT_DEFAULT_DEPTNAME);
        ConfigVO defaultRoleCode = sysTenantConfigService.configGet(TenantConfigEnum.TENANT_DEFAULT_ROLECODE);
        ConfigVO defaultRoleName = sysTenantConfigService.configGet(TenantConfigEnum.TENANT_DEFAULT_ROLENAME);
        ConfigVO defaultUserName = sysTenantConfigService.configGet(TenantConfigEnum.TENANT_DEFAULT_USERNAME);

        TenantBroker.runAs(entity.getId(), id -> {
            // 插入部门
            SysDeptEntity dept = new SysDeptEntity();
            dept.setName(defaultDept.getConfVal());
            dept.setParentId(0L);
            deptService.save(dept);
            // 维护部门关系
            deptRelationService.insertDeptRelation(dept);

            // 构造初始化用户
            SysUserEntity user = new SysUserEntity();
            user.setPhone(param.getAdminPhone());
            user.setPassword(ENCODER.encode(StrUtil.subSufByLength(param.getAdminPhone(), 6)));
            user.setDeptId(dept.getDeptId());
            user.setUsername(defaultUserName.getConfVal());
            user.setName(defaultRoleName.getConfVal());
            userService.save(user);

            // 构造新角色 管理员角色
            SysRoleEntity role = new SysRoleEntity();
            role.setRoleCode(defaultRoleCode.getConfVal());
            role.setRoleName(defaultRoleName.getConfVal());
            role.setDsType(DataScopeTypeEnum.ALL.getCode());
            roleService.save(role);
            // 用户角色关系
            SysUserRoleEntity userRole = new SysUserRoleEntity();
            userRole.setUserId(user.getUserId());
            userRole.setRoleId(role.getRoleId());
            userRoleService.save(userRole);

            // TODO 字典
        });
        return Boolean.TRUE;
    }

    @Override
    public Boolean updateTenant(Long tenantId, AddTenantParam param) {
        SysTenantEntity entity = getById(tenantId);
        if (ObjectUtil.isNull(entity)) {
            throw new ServiceException("租户不存在");
        }
        // code 和 domain 重复性校验
        LambdaQueryWrapper<SysTenantEntity> qr = new LambdaQueryWrapper<>();
        qr.ne(SysTenantEntity::getId, entity.getId());
        qr.and(q -> q.eq(SysTenantEntity::getCode, param.getCode()).or()
                .eq(SysTenantEntity::getTenantDomain, param.getTenantDomain()));
        boolean exists = baseMapper.exists(qr);
        if (exists) {
            throw new ServiceException("该租户编码或域名已存在");
        }
        BeanUtil.copyProperties(param, entity);
        return updateById(entity);
    }

    @Override
    public PageVO<SysTenantVO> pageSearch(Page<SysTenantEntity> objectPage, String domain, String tenantName,
                                          Integer status) {
        LambdaQueryWrapper<SysTenantEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(StrUtil.isNotBlank(domain), SysTenantEntity::getTenantDomain, domain);
        qr.like(StrUtil.isNotBlank(tenantName), SysTenantEntity::getName, tenantName);
        qr.eq(ObjectUtil.isNotNull(status), SysTenantEntity::getStatus, status);
        Page<SysTenantEntity> respage = page(objectPage, qr);
        return PageUtil.pageToVO(respage, SysTenantVO.class);
    }

}

