package cn.shihh.nimble.system.service;

import cn.shihh.nimble.system.pojo.entity.SysPostEntity;
import cn.shihh.nimble.system.pojo.param.AddPostParam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 岗位信息表(SysPost)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 16:26:28
 */
public interface SysPostService extends IService<SysPostEntity> {

    /**
     * 添加岗位
     * @param param
     * @return
     */
    Boolean savePost(AddPostParam param);

    /**
     * 修改岗位
     * @param param
     * @return
     */
    Boolean updatePost(Long postId, AddPostParam param);

    /**
     * 删除岗位
     * @param postId
     * @return
     */
    Boolean deletePost(Long postId);

}

