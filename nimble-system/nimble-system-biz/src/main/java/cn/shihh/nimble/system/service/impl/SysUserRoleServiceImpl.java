package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.shihh.nimble.system.api.pojo.vo.SysUserBaseVO;
import cn.shihh.nimble.system.pojo.entity.SysUserRoleEntity;
import cn.shihh.nimble.common.body.RoleBody;
import cn.shihh.nimble.system.mapper.SysUserRoleMapper;
import cn.shihh.nimble.system.pojo.bo.SysRoleUserCountBO;
import cn.shihh.nimble.system.service.SysUserRoleService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 用户角色表(SysUserRole)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Service
@RequiredArgsConstructor
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRoleEntity> implements SysUserRoleService {
    @Override
    public Boolean removeByUserId(Long userId) {
        LambdaQueryWrapper<SysUserRoleEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysUserRoleEntity::getUserId, userId);
        return remove(qr);
    }

    @Override
    public Boolean removeByRoleId(Long roleId) {
        LambdaQueryWrapper<SysUserRoleEntity> qr = new LambdaQueryWrapper<>();
        qr.eq(SysUserRoleEntity::getRoleId, roleId);
        return remove(qr);
    }

    @Override
    public List<Long> listRoleIdByUserId(Long userId) {
        List<SysUserRoleEntity> list =
                lambdaQuery().select(SysUserRoleEntity::getRoleId).eq(SysUserRoleEntity::getUserId, userId).list();
        return list.stream().map(SysUserRoleEntity::getRoleId).collect(Collectors.toList());
    }

    @Override
    public List<RoleBody> listRoleByUserId(Long userId) {
        return baseMapper.listRoleByUserId(userId);
    }

    @Override
    public Boolean saveUserRole(Long userId, List<Long> roleIds) {
        removeByUserId(userId);
        if (CollUtil.isEmpty(roleIds)) {
            return Boolean.TRUE;
        }
        List<SysUserRoleEntity> list = new ArrayList<>();
        for (Long roleId : roleIds) {
            SysUserRoleEntity entity = new SysUserRoleEntity();
            entity.setRoleId(roleId);
            entity.setUserId(userId);
            list.add(entity);
        }
        return saveBatch(list);
    }

    @Override
    public Map<Long, Integer> countsByRoleIds(List<Long> roleIds) {
        if (CollUtil.isNotEmpty(roleIds)) {
            List<SysRoleUserCountBO> counts = baseMapper.countsByRoleIds(roleIds);
            return counts.stream().collect(Collectors.toMap(SysRoleUserCountBO::getRoleId, SysRoleUserCountBO::getUserCount));
        }
        return Maps.newHashMap();
    }

    @Override
    public List<SysUserBaseVO> getUsersByRoleId(Long roleId) {
        return baseMapper.getUsersByRoleId(roleId);
    }
}

