package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * (SysTenantMenu)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:18
 */
@Data
@Schema(description = "")
public class SysTenantMenuBO {
    
    @Schema(description = "${column.comment}")
    private Long id;
    
    @Schema(description = "租户菜单名称")
    private String name;
    
    @Schema(description = "${column.comment}")
    private String menuIds;
    
    @Schema(description = "租户菜单,9:冻结,0:正常")
    private String status;
    
    @Schema(description = "${column.comment}")
    private String delFlag;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "修改人")
    private String updateBy;
    
    @Schema(description = "创建")
    private LocalDateTime createTime;
    
    @Schema(description = "更新时间")
    private LocalDateTime updateTime;

}

