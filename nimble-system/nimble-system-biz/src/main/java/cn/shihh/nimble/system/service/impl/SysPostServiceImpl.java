package cn.shihh.nimble.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.shihh.nimble.common.exception.ServiceException;
import cn.shihh.nimble.system.mapper.SysPostMapper;
import cn.shihh.nimble.system.pojo.entity.SysPostEntity;
import cn.shihh.nimble.system.pojo.param.AddPostParam;
import cn.shihh.nimble.system.service.SysPostService;
import cn.shihh.nimble.system.service.SysUserPostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 岗位信息表(SysPost)表服务实现类
 *
 * @author makejava
 * @since 2023-03-15 16:26:28
 */
@Service
@RequiredArgsConstructor
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPostEntity> implements SysPostService {

    private final SysUserPostService sysUserPostService;

    @Override
    public Boolean savePost(AddPostParam param) {
        SysPostEntity entity = BeanUtil.copyProperties(param, SysPostEntity.class);
        return save(entity);
    }

    @Override
    public Boolean updatePost(Long postId, AddPostParam param) {
        SysPostEntity post = getById(postId);
        if (ObjectUtil.isNull(post)) {
            throw new ServiceException("岗位不存在");
        }
        SysPostEntity entity = BeanUtil.copyProperties(param, SysPostEntity.class);
        entity.setPostId(postId);
        return updateById(entity);
    }

    @Override
    public Boolean deletePost(Long postId) {
        SysPostEntity post = getById(postId);
        if (ObjectUtil.isNull(post)) {
            throw new ServiceException("岗位不存在");
        }
        sysUserPostService.removeByPostId(postId);
        return removeById(postId);
    }
}

