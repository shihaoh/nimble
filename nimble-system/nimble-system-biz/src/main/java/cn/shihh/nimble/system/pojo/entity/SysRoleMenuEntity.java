package cn.shihh.nimble.system.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 角色菜单表(SysRoleMenu)${!tableInfo.obj.comment}
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
@Data
@TableName(value = "sys_role_menu", autoResultMap = true)
public class SysRoleMenuEntity {
    /**
     * 角色ID
     */
    private Long roleId;
    /**
     * 菜单ID
     */
    private Long menuId;

}

