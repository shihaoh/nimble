package cn.shihh.nimble.system.service;

import cn.shihh.nimble.system.pojo.entity.SysUserPostEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户与岗位关联表(SysUserPost)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 16:26:29
 */
public interface SysUserPostService extends IService<SysUserPostEntity> {

    /**
     * 根据岗位删除关联
     * @param postId
     * @return
     */
    Boolean removeByPostId(Long postId);

    /**
     * 根据用户删除关联
     * @param userId
     * @return
     */
    Boolean removeByUserId(Long userId);

    /**
     * 根据用户查询岗位
     * @param userId
     * @return
     */
    List<Long> listPostByUserId(Long userId);

    /**
     * 保存用户岗位关联
     * @param userId
     * @param postIds
     * @return
     */
    Boolean saveUserPost(Long userId, List<Long> postIds);
}

