package cn.shihh.nimble.system.service;

import cn.shihh.nimble.system.pojo.entity.SysRoleMenuEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;

/**
 * 角色菜单表(SysRoleMenu)表服务接口
 *
 * @author makejava
 * @since 2023-03-15 14:36:57
 */
public interface SysRoleMenuService extends IService<SysRoleMenuEntity> {

    /**
     * 更新角色菜单
     *
     * @param roleId  角色ID
     * @param appCode 应用编码
     * @param menuIds 菜单ID拼成的字符串，每个id之间根据逗号分隔
     * @return
     */
    Boolean saveRoleMenus(Long roleId, String appCode, Collection<Long> menuIds);

    /**
     * 通过角色ID删除角色菜单
     * @param roleId 角色ID
     * @return
     */
    Boolean removeByRoleId(Long roleId);

    /**
     * 通过角色ID删除角色菜单
     *
     * @param appCode 应用编码
     * @param roleId  角色ID
     * @return
     */
    Boolean removeByAppIdAndRoleId(String appCode, Long roleId);

}

