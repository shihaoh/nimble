package cn.shihh.nimble.system.pojo.bo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 系统角色表(SysRole)表实体类
 *
 * @author makejava
 * @since 2023-03-15 16:30:16
 */
@Data
@Schema(description = "系统角色表")
public class SysRoleBO {
    
    @Schema(description = "${column.comment}")
    private Long roleId;
    
    @Schema(description = "${column.comment}")
    private String roleName;
    
    @Schema(description = "${column.comment}")
    private String roleCode;
    
    @Schema(description = "${column.comment}")
    private String roleDesc;
    
    @Schema(description = "${column.comment}")
    private Integer dsType;
    
    @Schema(description = "${column.comment}")
    private String dsScope;
    
    @Schema(description = "创建人")
    private String createBy;
    
    @Schema(description = "修改人")
    private String updateBy;
    
    @Schema(description = "${column.comment}")
    private LocalDateTime createTime;
    
    @Schema(description = "${column.comment}")
    private LocalDateTime updateTime;
    
    @Schema(description = "${column.comment}")
    private String delFlag;
    
    @Schema(description = "${column.comment}")
    private Long tenantId;

}

