package cn.shihh.nimble.system.pojo.bo;

import lombok.Data;

/**
 * @author shihh
 * @since 2023/3/30
 */
@Data
public class SysRoleUserCountBO {

    private Long roleId;
    private Integer userCount;

}
