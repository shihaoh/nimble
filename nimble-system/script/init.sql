DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`
(
    `dept_id`     bigint      NOT NULL,
    `name`        varchar(50)          DEFAULT NULL,
    `sort_order`  int         NOT NULL DEFAULT '0' COMMENT '排序',
    `parent_id`   bigint      NOT NULL DEFAULT 0,
    `dept_conf`   varchar(512)         DEFAULT NULL COMMENT '部门配置',
    `create_by`   varchar(64) NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`   bigint      NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`   bigint      NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`   varchar(64) NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    char(1)     NOT NULL DEFAULT '0',
    `tenant_id`   bigint      NOT NULL DEFAULT '1' COMMENT '所属租户',
    PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='部门管理';

-- ----------------------------
-- Table structure for sys_dept_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_relation`;
CREATE TABLE `sys_dept_relation`
(
    `ancestor`   bigint NOT NULL COMMENT '祖先节点',
    `descendant` bigint NOT NULL COMMENT '后代节点',
    PRIMARY KEY (`ancestor`, `descendant`) USING BTREE,
    KEY `idx1` (`ancestor`) USING BTREE,
    KEY `idx2` (`descendant`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='部门关系表';



DROP TABLE IF EXISTS `sys_application`;
CREATE TABLE `sys_application`
(
    `app_id`      bigint       not null comment '应用ID',
    `name`        varchar(32)  not null comment '应用名称',
    `code`        varchar(32)  not null comment '应用代码',
    `entry`       varchar(32)  not null comment '应用入口',
    `icon`        varchar(255) not null comment '应用图标',
    `homepage`    varchar(32)  not null comment '应用首页',
    `remark`      varchar(200)          DEFAULT '' COMMENT '备注',
    `status`      bit(1)                DEFAULT b'1' COMMENT '状态',
    `sort_order`  int                   DEFAULT '1' COMMENT '排序值',
    `create_by`   varchar(64)  NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`   bigint       NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`   bigint       NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`   varchar(64)  NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    char(1)      NOT NULL DEFAULT '0',
    `tenant_id`   bigint       NOT NULL DEFAULT '1' COMMENT '所属租户',
    PRIMARY KEY (`app_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='应用表';


-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `menu_id`        bigint      NOT NULL COMMENT '菜单ID',
    `parent_id`      bigint               DEFAULT NULL COMMENT '父菜单ID',
    `app_id`         bigint      NOT NULL COMMENT '应用ID',
    `name`           varchar(32)          DEFAULT NULL,
    `is_alias`       bit                  default b'0' not null comment '是否别名',
    `permission`     varchar(32)          DEFAULT NULL,
    `path`           varchar(128)         DEFAULT NULL,
    `icon`           varchar(255)         DEFAULT NULL,
    `visible`        char(1)              DEFAULT '1' COMMENT '是否显示',
    `sort_order`     int                  DEFAULT '1' COMMENT '排序值',
    `keep_alive`     char(1)              DEFAULT '0',
    `menu_type`      char(1)              DEFAULT '0',
    `origin_menu_id` bigint      NOT NULL DEFAULT -1 COMMENT '原始菜单ID',
    `create_by`      varchar(64) NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`      bigint      NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`      bigint      NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`      varchar(64) NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time`    datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`    datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`       char(1)     NOT NULL DEFAULT '0',
    `tenant_id`      bigint      NOT NULL DEFAULT '1' COMMENT '所属租户',
    PRIMARY KEY (`menu_id`) USING BTREE,
    KEY `sys_menu_parentid` (parent_id) USING BTREE,
    KEY `sys_menu_appid` (app_id) USING BTREE,
    KEY `sys_menu_tenantid` (tenant_id) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;

COMMIT;


-- ----------------------------
-- Table structure for tenant_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_config`;
CREATE TABLE `sys_tenant_config`
(
    `id`                 bigint       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `conf_type`          varchar(40)  NOT NULL DEFAULT '' COMMENT '配置分类',
    `conf_key`           varchar(255) NOT NULL DEFAULT '' COMMENT '配置键',
    `conf_value`         text COMMENT '配置值',
    `conf_default_value` text COMMENT '配置默认值',
    `conf_desc`          text COMMENT '配置描述',
    `create_by`          varchar(64)  NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`          bigint       NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`          bigint       NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`          varchar(64)  NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time`        datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`           char(1)      NOT NULL DEFAULT '0',
    `tenant_id`          bigint       NOT NULL DEFAULT '1' COMMENT '所属租户',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uni_tenant_config__index` (`conf_key`, `tenant_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4 COMMENT ='机构配置表';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `role_id`     bigint      NOT NULL,
    `enabled`     bit         NOT NULL DEFAULT b'1' COMMENT '启用',
    `role_name`   varchar(64)          DEFAULT NULL,
    `role_code`   varchar(64)          DEFAULT NULL,
    `role_desc`   varchar(255)         DEFAULT NULL,
    `ds_type`     char(1)              DEFAULT '2',
    `ds_scope`    varchar(255)         DEFAULT NULL,
    `create_by`   varchar(64) NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`   bigint      NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`   bigint      NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`   varchar(64) NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    char(1)     NOT NULL DEFAULT '0',
    `tenant_id`   bigint      NOT NULL DEFAULT '1' COMMENT '所属租户',
    PRIMARY KEY (`role_id`) USING BTREE,
    KEY `role_idx1_role_code` (`role_code`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='系统角色表';

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`
(
    `role_id` bigint NOT NULL COMMENT '角色ID',
    `menu_id` bigint NOT NULL COMMENT '菜单ID',
    PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='角色菜单表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`
(
    `id`            bigint(0)    NOT NULL COMMENT '租户id',
    `name`          varchar(255) NULL     DEFAULT NULL,
    `code`          varchar(64)  NULL     DEFAULT NULL,
    `tenant_domain` varchar(255) NULL     DEFAULT NULL,
    `start_time`    datetime(0)  NULL     DEFAULT NULL COMMENT '开始时间',
    `end_time`      datetime(0)  NULL     DEFAULT NULL COMMENT '结束时间',
    `status`        char(1)      NULL     DEFAULT '0',
    `del_flag`      char(1)      NOT NULL DEFAULT '0',
    `create_by`     varchar(64)  NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`     bigint       NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`     bigint       NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`     varchar(64)  NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time`   datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `menu_id`       bigint(0)    NULL     DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `code` (`code`) USING BTREE,
    UNIQUE KEY `tenant_domain` (`tenant_domain`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '租户表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
BEGIN;
INSERT INTO `sys_tenant`
VALUES (1, '平台后台', '1', '', '2019-05-15 00:00:00', '2029-05-15 00:00:00', '0', '0', 'admin', 0, 0, 'admin',
        '2019-05-15 15:44:57', '2022-12-09 15:27:50', null);
COMMIT;

-- ----------------------------
-- Table structure for sys_tenant_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_menu`;
CREATE TABLE `sys_tenant_menu`
(
    `id`          bigint(0)    NOT NULL,
    `name`        varchar(255) NOT NULL COMMENT '租户菜单名称',
    `menu_ids`    text,
    `status`      char(1)      NOT NULL DEFAULT '0' COMMENT '租户菜单,9:冻结,0:正常',
    `create_by`   varchar(64)  NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`   bigint       NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`   bigint       NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`   varchar(64)  NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    char(1)      NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `user_id`       bigint       NOT NULL COMMENT '主键ID',
    `status`        tinyint      NOT NULL COMMENT '状态',
    `user_code`     varchar(64)  NOT NULL DEFAULT '' COMMENT '用户编号',
    `username`      varchar(64)  NOT NULL DEFAULT '' COMMENT '用户名',
    `password`      varchar(255) NOT NULL COMMENT '密码',
    `phone`         varchar(20)  NOT NULL COMMENT '手机号',
    `avatar`        varchar(255)          DEFAULT '' COMMENT '头像',
    `portrait`      varchar(255)          DEFAULT '' COMMENT '形象照',
    `wechat_qrcode` varchar(255)          DEFAULT '' COMMENT '微信二维码',
    `nickname`      varchar(64)           DEFAULT '' COMMENT '拓展字段:昵称',
    `name`          varchar(64)           DEFAULT '' COMMENT '拓展字段:姓名',
    `email`         varchar(128)          DEFAULT '' COMMENT '拓展字段:邮箱',
    `dept_id`       bigint       NOT NULL COMMENT '部门ID',
    `lock_flag`     char(1)               DEFAULT '0',
    `create_by`     varchar(64)  NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`     bigint       NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`     bigint       NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`     varchar(64)  NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time`   datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`      char(1)      NOT NULL DEFAULT '0',
    `tenant_id`     bigint       NOT NULL DEFAULT '1' COMMENT '所属租户',
    PRIMARY KEY (`user_id`) USING BTREE,
    KEY `user_idx1_username` (`username`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='用户表';

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `user_id` bigint NOT NULL COMMENT '用户ID',
    `role_id` bigint NOT NULL COMMENT '角色ID',
    PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT ='用户角色表';

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`
(
    `post_id`     bigint       NOT NULL COMMENT '岗位ID',
    `post_type`   tinyint      NOT NULL default 0 COMMENT '岗位分类: 0律师1行政2财务3其他',
    `post_code`   varchar(64)  NOT NULL COMMENT '岗位编码',
    `post_name`   varchar(50)  NOT NULL COMMENT '岗位名称',
    `post_sort`   int(0)       NOT NULL DEFAULT 0 COMMENT '岗位排序',
    `remark`      varchar(500) NULL     DEFAULT NULL COMMENT '岗位描述',
    `create_by`   varchar(64)  NOT NULL DEFAULT ' ' COMMENT '创建人',
    `create_at`   bigint       NOT NULL DEFAULT '0' COMMENT '创建人id',
    `update_at`   bigint       NOT NULL DEFAULT '0' COMMENT '修改人id',
    `update_by`   varchar(64)  NOT NULL DEFAULT ' ' COMMENT '修改人',
    `create_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `del_flag`    char(1)      NOT NULL DEFAULT '0',
    `tenant_id`   bigint       NOT NULL DEFAULT '1' COMMENT '所属租户',
    PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表';


-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`
(
    `user_id` bigint(0) NOT NULL COMMENT '用户ID',
    `post_id` bigint(0) NOT NULL COMMENT '岗位ID',
    PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT = '用户与岗位关联表';

