package cn.shihh.nimble.auth.annotation;

import cn.shihh.nimble.auth.config.AuthDependScanConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({AuthDependScanConfig.class})
public @interface EnableAuthServer {
}
