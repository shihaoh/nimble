package cn.shihh.nimble.auth.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author shihh
 * @since 2023/3/23
 */
@Configuration
@ComponentScan("cn.shihh.nimble.auth")
public class AuthDependScanConfig {
}
