package cn.shihh.nimble.auth.service;

import cn.shihh.nimble.auth.form.LoginBody;

/**
 * @author shihh
 * @since 2023/3/29
 */
public interface LoginService {
    /**
     * 登录
     *
     * @param loginBody 登录信息
     * @return token
     */
    String login(LoginBody loginBody);
}
