package cn.shihh.nimble.auth;

import cn.shihh.nimble.common.feign.annotation.EnableCustomerFeign;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shihh
 * @since 2023/3/22
 */
@SpringBootApplication
@EnableCustomerFeign
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
}
