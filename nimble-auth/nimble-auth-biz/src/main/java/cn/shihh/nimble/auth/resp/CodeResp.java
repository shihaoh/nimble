package cn.shihh.nimble.auth.resp;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/3/29
 */
@Data
@Schema(description = "验证码响应体")
@AllArgsConstructor
public class CodeResp {

    @Schema(description = "验证码ID")
    private String uuid;

    @Schema(description = "验证码图片")
    private String codePic;

}
