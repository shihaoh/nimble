package cn.shihh.nimble.auth.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.shihh.nimble.auth.resp.CodeResp;
import cn.shihh.nimble.auth.service.ValidateService;
import cn.shihh.nimble.common.constant.CacheConstants;
import cn.shihh.nimble.common.exception.CaptchaException;
import cn.shihh.nimble.common.redis.util.RedisUtil;
import com.wf.captcha.SpecCaptcha;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.UUID;

/**
 * @author shihh
 * @since 2023/3/29
 */
@Slf4j
@Service
public class ValidateServiceImpl implements ValidateService {
    @Override
    public CodeResp getCode() {
        String uuid = UUID.randomUUID().toString().replace("-", "");
        // 宽120，高40，4位字符
        SpecCaptcha captcha = new SpecCaptcha(120, 40, 4);
        // 获取字符内容
        String text = captcha.text();
        // 获取base64编码的图片
        String base64 = captcha.toBase64();
        // 缓存验证码 1分钟有效期
        RedisUtil.putString(CacheConstants.CAPTCHA_PREFIX + uuid, text, Duration.ofMinutes(1));
        log.debug("验证码：{}，uuid：{}", text, uuid);
        return new CodeResp(uuid, base64);
    }

    @Override
    @SneakyThrows
    public void check(String uuid, String code) {
        String key = CacheConstants.CAPTCHA_PREFIX + uuid;
        String rdsCode = RedisUtil.getString(key);
        RedisUtil.delete(key);
        if (code == null || !StrUtil.equalsIgnoreCase(code, rdsCode)) {
            throw new CaptchaException("验证码错误或已失效，请重新获取验证码");
        }
    }
}
