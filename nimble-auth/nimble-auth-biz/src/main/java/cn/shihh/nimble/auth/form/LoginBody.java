package cn.shihh.nimble.auth.form;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

/**
 * @author shihh
 * @since 2023/3/22
 */
@Data
@Schema(description = "登录表单")
public class LoginBody {

    @Schema(description = "租户ID")
    @NotNull(message = "租户ID不能为空")
    private Long tenantId;

    @Schema(description = "手机号")
    @Pattern(regexp = "^1[3-9][0-9]{9}$", message = "手机号格式有误")
    private String phone;

    @Schema(description = "密码")
    @NotEmpty(message = "密码不能为空")
    private String password;

    @Schema(description = "验证码id")
    @NotEmpty(message = "验证码不能为空")
    private String uuid;

    @Schema(description = "验证码")
    @NotEmpty(message = "验证码不能为空")
    private String code;
}
