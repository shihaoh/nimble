package cn.shihh.nimble.auth.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.shihh.nimble.auth.service.ValidateService;
import cn.shihh.nimble.auth.resp.CodeResp;
import cn.shihh.nimble.common.util.R;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shihh
 * @since 2023/3/29
 */
@Tag(name = "Auth", description = "认证相关接口")
@Slf4j
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@SaIgnore
public class AuthController {

    private final ValidateService validateService;

    @Operation(summary = "验证码")
    @RequestMapping("code")
    public R<CodeResp> code() {
        return R.ok(validateService.getCode());
    }

}
