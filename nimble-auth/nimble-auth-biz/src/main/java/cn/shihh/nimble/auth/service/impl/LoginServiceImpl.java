package cn.shihh.nimble.auth.service.impl;

import cn.dev33.satoken.exception.SaTokenException;
import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.shihh.nimble.auth.form.LoginBody;
import cn.shihh.nimble.auth.service.LoginService;
import cn.shihh.nimble.auth.service.ValidateService;
import cn.shihh.nimble.common.body.LoginUser;
import cn.shihh.nimble.common.satoken.utils.LoginHelper;
import cn.shihh.nimble.common.util.ClientEx;
import cn.shihh.nimble.common.util.R;
import cn.shihh.nimble.system.api.client.RemoteUserClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shihh
 * @since 2023/3/22
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final ValidateService validateService;
    private final RemoteUserClient remoteUserClient;

    @Override
    public String login(LoginBody loginBody) {
        // 校验验证码
        validateService.check(loginBody.getUuid(), loginBody.getCode());
        // 获取登录用户信息
        R<LoginUser> r = remoteUserClient.getUserByPhone(loginBody.getTenantId(), loginBody.getPhone());
        if (ObjectUtil.equal(r.getMsg(), "用户不存在")) {
            throw new SaTokenException("用户名或密码错误");
        }
        LoginUser loginUser = ClientEx.execute(() -> r);
        // 密码校验
        if (!BCrypt.checkpw(loginBody.getPassword(), loginUser.getPassword())) {
            throw new SaTokenException("用户名或密码错误");
        }

        // 登录
        LoginHelper.login(loginUser);
        log.info("登录成功：{}", LoginHelper.getLoginId());
        return StpUtil.getTokenValue();
    }

}
