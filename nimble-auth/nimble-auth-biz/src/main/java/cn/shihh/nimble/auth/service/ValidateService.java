package cn.shihh.nimble.auth.service;

import cn.shihh.nimble.auth.resp.CodeResp;

/**
 * 验证码业务类
 */
public interface ValidateService {

    /**
     * 获取图形验证码
     *
     * @return R
     */
    CodeResp getCode();

    /**
     * 校验验证码
     *
     * @param uuid 验证码ID
     * @param code 验证码
     */
    void check(String uuid, String code);
}
