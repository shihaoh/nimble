package cn.shihh.nimble.auth.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.StpUtil;
import cn.shihh.nimble.auth.form.LoginBody;
import cn.shihh.nimble.auth.service.LoginService;
import cn.shihh.nimble.common.util.R;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shihh
 * @since 2023/3/22
 */
@Tag(name = "Token", description = "Token相关接口")
@RestController
@RequestMapping("/auth/token")
@RequiredArgsConstructor
public class TokenController {

    private final LoginService loginService;

    @Operation(summary = "登录")
    @PostMapping("login")
    @SaIgnore
    public R<String> login(@Validated @RequestBody LoginBody loginBody) {
        return R.ok(loginService.login(loginBody));
    }

    @Operation(summary = "登出")
    @PostMapping("logout")
    public R<String> logout() {
        StpUtil.logout();
        return R.ok();
    }
}
