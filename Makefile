
VERSION ?= 1.0.0

REPO ?= ip

BUILD_IMAGE = nimble

SVC ?= bg

APP = null
PT = null

ifeq (sys, $(SVC))
APP = nimble-system-server
PT = nimble-system
else
ifeq (auth, $(SVC))
APP = nimble-auth-server
PT = nimble-auth
else
else
APP = $(SVC)
endif
endif

TIMESTAMP := $(shell date +%Y%m%d%H%M)

deploy: build set-image

set-image:
	kubectl -n nimble-module set image deployment $(APP) $(APP)=$(BUILD_IMAGE)/$(APP):$(TIMESTAMP)

build: mvn-build docker-build

docker-build:
	docker buildx build --rm --platform=linux/amd64 --push -f $(PT)/Dockerfile $(PT) -t $(BUILD_IMAGE)/$(APP):$(TIMESTAMP)

mvn-build:
	cd $(PT) && mvn clean package


