## 构建说明

> 可根据 `Makefile` 所定义的构建命令进行构建
> 
> 例如：`make build SVC=sys` 构建system服务镜像

## 部署说明

> 根据自己实际连接的数据库和redis修改配置文件secret-nimble-x-conf.yaml
> 
> 确认无误后执行`kubectl apply -f . -n nimble-x` 命名空间自己定义

## 本地开发说明

> 自己定义一个application-{active}.yaml文件，对照`cm-nimble-x.yaml`将自己的配置写入所需运行的服务resources文件夹下
> 
> 也可以使用java注入环境变量的方式注入对应参数启动

