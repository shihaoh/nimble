package cn.shihh.nimble.log.annotation;

import cn.shihh.nimble.log.config.LogDependScanConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author shihh
 * @since 2023/6/22
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({LogDependScanConfig.class})
public @interface EnableLogServer {

}
