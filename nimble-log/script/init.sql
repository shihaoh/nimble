DROP TABLE IF EXISTS `t_log_req`;
CREATE TABLE `t_log_req`
(
    `id`            bigint(20) NOT NULL COMMENT '日志ID',
    `trace_id`      varchar(32)         DEFAULT NOT NULL COMMENT '链路id',
    `app_name`      varchar(32)         DEFAULT NOT NULL COMMENT '应用名称',
    `method`        varchar(20)         DEFAULT NULL COMMENT '提交类型',
    `url`           varchar(1000)       DEFAULT NULL COMMENT '提交url',
    `get_data`      json                DEFAULT NULL COMMENT 'get数据',
    `post_data`     json                DEFAULT NULL COMMENT 'post数据',
    `header_data`   json                DEFAULT NULL COMMENT 'header数据',
    `ip`            varchar(64)         DEFAULT NULL COMMENT 'IP地址',
    `res_code`      int(11)             DEFAULT '0' COMMENT '响应code',
    `res_msg`       varchar(1000)       DEFAULT NULL COMMENT '响应信息',
    `res_data`      json                DEFAULT NULL COMMENT '响应对象',
    `user_agent`    varchar(512)        DEFAULT NULL COMMENT 'UA信息',
    `take_up_time`  bigint(20)          DEFAULT '0' COMMENT '请求耗时',
    `request_time`  timestamp  NOT NULL COMMENT '请求时间',
    `response_time` timestamp  NOT NULL COMMENT '响应时间',
    `tenant_id`     bigint(20) UNSIGNED DEFAULT '0' COMMENT '租户ID',
    `user_id`       bigint(20)          DEFAULT '0' COMMENT '用户ID',
    `request_by`    varchar(32)         DEFAULT '' COMMENT '请求用户',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='请求日志';



