package cn.shihh.nimble.log.api.client;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author shihh
 * @since 2023/3/27
 */
@FeignClient(value = "nimble-log", url = "${nimble.feign.url.log:nimble-log}")
public interface RemoteLogClient {
}
