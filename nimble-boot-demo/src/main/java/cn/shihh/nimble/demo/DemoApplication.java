package cn.shihh.nimble.demo;

import cn.shihh.nimble.auth.annotation.EnableAuthServer;
import cn.shihh.nimble.common.feign.annotation.EnableCustomerFeign;
import cn.shihh.nimble.log.annotation.EnableLogServer;
import cn.shihh.nimble.system.annotation.EnableSystemServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shihh
 * @since 2023/6/22
 */
@SpringBootApplication
@EnableCustomerFeign
@EnableSystemServer
@EnableAuthServer
@EnableLogServer
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
